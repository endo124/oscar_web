<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $categories = [
            [
                "id" => 1,
                "name" => "Oscar Signature Specialties",
                "slug" => "signature",
                "parent" => 0,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
                    "title" => "signature",
                    "alt" => ""
                ])
            ],
            [
                "id" => 11,
                "name" => "Dairy",
                "slug" => "dairy",
                "parent" => 1,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
                    "title" => "signature",
                    "alt" => ""
                ])
            ],
            [
                "id" => 12,
                "name" => "Cocoaccino",
                "slug" => "cocoaccino",
                "parent" => 1,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
                    "title" => "signature",
                    "alt" => ""
                ])
            ],
            [
                "id" => 13,
                "name" => "Marjaayoun",
                "slug" => "marjaayoun",
                "parent" => 1,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
                    "title" => "signature",
                    "alt" => ""
                ])
            ],
            [
                "id" => 14,
                "name" => "Make Your Own Sandwich",
                "slug" => "Make Your Own sandwich",
                "parent" => 1,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
                    "title" => "signature",
                    "alt" => ""
                ])
            ],
            [
                "id" => 15,
                "name" => "Fresh Juices",
                "slug" => "Fresh juices",
                "parent" => 1,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
                    "title" => "signature",
                    "alt" => ""
                ])
            ],
            [
                "id" => 16,
                "name" => "Salads/Dips",
                "slug" => "Salads/dips",
                "parent" => 1,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
                    "title" => "signature",
                    "alt" => ""
                ])
            ],
            [
                "id" => 2,
                "name" => "Oscar Bakery",
                "slug" => "bakery",
                "parent" => 0,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/2 Oscar Bakery.jpg",
                    "title" => "Oscar Bakery",
                    "alt" => ""
                ])
            ],
            [
                "id" => 3,
                "name" => "Fresh Food",
                "slug" => "fresh",
                "parent" => 0,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/3 Fresh Food.jpg",
                    "title" => "Fresh Food",
                    "alt" => ""
                ])
            ],

            [
                "id" => 4,
                "name" => "Deli",
                "slug" => "men",
                "parent" => 0,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Deli.jpg",
                    "title" => "man",
                    "alt" => ""
                ])
            ],
            [
                "id" => 5,
                "name" => "Frozen",
                "slug" => "frozen",
                "parent" => 0,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/5 Frozen.jpg",
                    "title" => "Frozen",
                    "alt" => ""
                ])
            ],
            [
                "id" => 6,
                "name" => "Groceries",
                "slug" => "groceries",
                "parent" => 0,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Groceries.jpg",
                    "title" => "Groceries",
                    "alt" => ""
                ])
            ],
            [
                "id" => 7,
                "name" => "Detergents and Cleaning Supplies",
                "slug" => "detergents",
                "parent" => 0,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Detergents and Cleaning Supplies.jpg",
                    "title" => "Detergents",
                    "alt" => ""
                ])
            ],
            [
                "id" => 8,
                "name" => "Health & Beauty",
                "slug" => "health",
                "parent" => 0,
                "description" => "",
                "display" => "default",
                "image" =>json_encode( [
                    "src" => "http://oscar.momentum-sol.com/storage/media/Health & Beauty.jpg",
                    "title" => "Health",
                    "alt" => ""
                ])
            ],
            [
                "id" => 9,
                "name" => "Oscar Home",
                "slug" => "men",
                "parent" => 0,
                "description" => "",
                "display" => "default",
                "image" => json_encode([
                    "src" => "http://oscar.momentum-sol.com/storage/media/Oscar-Home.jpg",
                    "title" => "man",
                    "alt" => ""
                ])
            ]
        ];
        \DB::table('categories')->insert($categories);
	}
}
