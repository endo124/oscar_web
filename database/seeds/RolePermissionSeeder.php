<?php

use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		\DB::table('role_has_permissions')->truncate();
		\App\Models\Permission::truncate();
		\App\Models\Role::truncate();
		$role = \App\Models\Role::create([
			'name' => 'admin',
			'guard_name' => 'web'
		]);
		$permissions = collect([
			'users.index',
			'users.show',
			'users.create',
			'users.edit',
			'users.delete',
			'roles.index',
			'roles.show',
			'roles.create',
			'roles.edit',
			'roles.delete',
			'permissions.index',
			'permissions.show',
			'permissions.create',
			'permissions.edit',
			'permissions.delete',
			'products.index',
			'products.show',
			'products.create',
			'products.edit',
			'products.delete',
			'categories.index',
			'categories.show',
			'categories.create',
			'categories.edit',
			'categories.delete',
			'restaurants.index',
			'restaurants.show',
			'restaurants.create',
			'restaurants.edit',
			'restaurants.delete',
			'branches.index',
			'branches.show',
			'branches.create',
			'branches.edit',
			'branches.delete',
			'kitchens.index',
			'kitchens.show',
			'kitchens.create',
			'kitchens.edit',
			'kitchens.delete',
		]);
		$permissions->map(function ($permission) use ($role) {
			\App\Models\Permission::create([
				'name' => $permission,
				'guard_name' => 'web'
			]);
			$role->givePermissionTo($permission);
		});
		\DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}
}
