<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTranslationToTables extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('page_contents', function (Blueprint $table) {
			$table->integer('m_value')->unsigned()->nullable();
		});
		Schema::table('promotions', function (Blueprint $table) {
			$table->integer('m_title')->unsigned()->nullable();
			$table->integer('m_description')->unsigned()->nullable();
			$table->integer('m_image')->unsigned()->nullable();
			$table->integer('m_url')->unsigned()->nullable();
		});
		Schema::table('restaurants', function (Blueprint $table) {
			$table->integer('m_title')->unsigned()->nullable();
			$table->integer('m_description')->unsigned()->nullable();
			$table->integer('m_image')->unsigned()->nullable();
			$table->integer('m_menu')->unsigned()->nullable();
		});
		Schema::table('categories', function (Blueprint $table) {
			$table->integer('m_title')->unsigned()->nullable();
			$table->integer('m_image')->unsigned()->nullable();
		});
		Schema::table('on_boardings', function (Blueprint $table) {
			$table->integer('m_question')->unsigned()->nullable();
			$table->integer('m_answers')->unsigned()->nullable();
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('page_contents', function (Blueprint $table) {
			$table->dropColumn('m_value');
		});
		Schema::table('promotions', function (Blueprint $table) {
			$table->dropColumn('m_title');
			$table->dropColumn('m_description');
			$table->dropColumn('m_image');
			$table->dropColumn('m_url');
		});
		Schema::table('restaurants', function (Blueprint $table) {
			$table->dropColumn('m_title');
			$table->dropColumn('m_description');
			$table->dropColumn('m_image');
			$table->dropColumn('m_menu');
		});
		Schema::table('categories', function (Blueprint $table) {
			$table->dropColumn('m_title');
			$table->dropColumn('m_image');
		});
		Schema::table('on_boardings', function (Blueprint $table) {
			$table->dropColumn('m_question');
			$table->dropColumn('m_answers');
		});
	}
}
