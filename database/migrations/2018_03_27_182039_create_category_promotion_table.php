<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryPromotionTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_promotion', function (Blueprint $table) {
			$table->integer('category_id')->unsigned()->default(0);
			$table->integer('promotion_id')->unsigned()->default(0);
			$table->foreign('category_id')->references('id')->on('categories');
			$table->foreign('promotion_id')->references('id')->on('promotions');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('category_promotion');
	}
}
