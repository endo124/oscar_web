<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOnBoardingValuesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('on_boarding_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id')->unsigned()->default(0);
            $table->text('value');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('question_id')->references('id')->on('on_boardings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('on_boarding_values');
    }
}
