<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('id');
            $table->string('name');
            $table->string('description');
            $table->string('name_ar');
            $table->string('description_ar');
            $table->string('on_sale');
            $table->string('regular_price');
            $table->string('hot_price');
            $table->string('discountFrom');
            $table->string('discountto');
            $table->string('discountprice');
            $table->string('barcode');
            $table->string('PriceUnit');
            $table->string('in_stock');
            $table->unsignedInteger('category_id');
            $table->string('image');
            $table->string('01');
            $table->string('02');
            $table->string('03');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
