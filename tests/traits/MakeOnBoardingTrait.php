<?php

use Faker\Factory as Faker;
use App\Models\OnBoarding;
use App\Repositories\OnBoardingRepository;

trait MakeOnBoardingTrait
{
    /**
     * Create fake instance of OnBoarding and save it in database
     *
     * @param array $onBoardingFields
     * @return OnBoarding
     */
    public function makeOnBoarding($onBoardingFields = [])
    {
        /** @var OnBoardingRepository $onBoardingRepo */
        $onBoardingRepo = App::make(OnBoardingRepository::class);
        $theme = $this->fakeOnBoardingData($onBoardingFields);
        return $onBoardingRepo->create($theme);
    }

    /**
     * Get fake instance of OnBoarding
     *
     * @param array $onBoardingFields
     * @return OnBoarding
     */
    public function fakeOnBoarding($onBoardingFields = [])
    {
        return new OnBoarding($this->fakeOnBoardingData($onBoardingFields));
    }

    /**
     * Get fake data of OnBoarding
     *
     * @param array $postFields
     * @return array
     */
    public function fakeOnBoardingData($onBoardingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order' => $fake->randomDigitNotNull,
            'question' => $fake->word,
            'answers' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $onBoardingFields);
    }
}
