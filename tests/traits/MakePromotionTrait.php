<?php

use Faker\Factory as Faker;
use App\Models\Promotion;
use App\Repositories\PromotionRepository;

trait MakePromotionTrait
{
    /**
     * Create fake instance of Promotion and save it in database
     *
     * @param array $promotionFields
     * @return Promotion
     */
    public function makePromotion($promotionFields = [])
    {
        /** @var PromotionRepository $promotionRepo */
        $promotionRepo = App::make(PromotionRepository::class);
        $theme = $this->fakePromotionData($promotionFields);
        return $promotionRepo->create($theme);
    }

    /**
     * Get fake instance of Promotion
     *
     * @param array $promotionFields
     * @return Promotion
     */
    public function fakePromotion($promotionFields = [])
    {
        return new Promotion($this->fakePromotionData($promotionFields));
    }

    /**
     * Get fake data of Promotion
     *
     * @param array $postFields
     * @return array
     */
    public function fakePromotionData($promotionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'type' => $fake->word,
            'description' => $fake->text,
            'image' => $fake->word,
            'url' => $fake->text,
            'price' => $fake->text,
            'sale' => $fake->text,
            'start_date' => $fake->word,
            'end_date' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $promotionFields);
    }
}
