<?php

use Faker\Factory as Faker;
use App\Models\ContactForm;
use App\Repositories\ContactFormRepository;

trait MakeContactFormTrait
{
    /**
     * Create fake instance of ContactForm and save it in database
     *
     * @param array $contactFormFields
     * @return ContactForm
     */
    public function makeContactForm($contactFormFields = [])
    {
        /** @var ContactFormRepository $contactFormRepo */
        $contactFormRepo = App::make(ContactFormRepository::class);
        $theme = $this->fakeContactFormData($contactFormFields);
        return $contactFormRepo->create($theme);
    }

    /**
     * Get fake instance of ContactForm
     *
     * @param array $contactFormFields
     * @return ContactForm
     */
    public function fakeContactForm($contactFormFields = [])
    {
        return new ContactForm($this->fakeContactFormData($contactFormFields));
    }

    /**
     * Get fake data of ContactForm
     *
     * @param array $postFields
     * @return array
     */
    public function fakeContactFormData($contactFormFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'type' => $fake->word,
            'values' => $fake->text,
            'language' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $contactFormFields);
    }
}
