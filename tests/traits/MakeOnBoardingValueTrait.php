<?php

use Faker\Factory as Faker;
use App\Models\OnBoardingValue;
use App\Repositories\OnBoardingValueRepository;

trait MakeOnBoardingValueTrait
{
    /**
     * Create fake instance of OnBoardingValue and save it in database
     *
     * @param array $onBoardingValueFields
     * @return OnBoardingValue
     */
    public function makeOnBoardingValue($onBoardingValueFields = [])
    {
        /** @var OnBoardingValueRepository $onBoardingValueRepo */
        $onBoardingValueRepo = App::make(OnBoardingValueRepository::class);
        $theme = $this->fakeOnBoardingValueData($onBoardingValueFields);
        return $onBoardingValueRepo->create($theme);
    }

    /**
     * Get fake instance of OnBoardingValue
     *
     * @param array $onBoardingValueFields
     * @return OnBoardingValue
     */
    public function fakeOnBoardingValue($onBoardingValueFields = [])
    {
        return new OnBoardingValue($this->fakeOnBoardingValueData($onBoardingValueFields));
    }

    /**
     * Get fake data of OnBoardingValue
     *
     * @param array $postFields
     * @return array
     */
    public function fakeOnBoardingValueData($onBoardingValueFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'question_id' => $fake->randomDigitNotNull,
            'value' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $onBoardingValueFields);
    }
}
