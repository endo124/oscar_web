<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PageTypeApiTest extends TestCase
{
    use MakePageTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePageType()
    {
        $pageType = $this->fakePageTypeData();
        $this->json('POST', '/api/v1/pageTypes', $pageType);

        $this->assertApiResponse($pageType);
    }

    /**
     * @test
     */
    public function testReadPageType()
    {
        $pageType = $this->makePageType();
        $this->json('GET', '/api/v1/pageTypes/'.$pageType->id);

        $this->assertApiResponse($pageType->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePageType()
    {
        $pageType = $this->makePageType();
        $editedPageType = $this->fakePageTypeData();

        $this->json('PUT', '/api/v1/pageTypes/'.$pageType->id, $editedPageType);

        $this->assertApiResponse($editedPageType);
    }

    /**
     * @test
     */
    public function testDeletePageType()
    {
        $pageType = $this->makePageType();
        $this->json('DELETE', '/api/v1/pageTypes/'.$pageType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/pageTypes/'.$pageType->id);

        $this->assertResponseStatus(404);
    }
}
