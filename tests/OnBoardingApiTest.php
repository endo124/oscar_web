<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OnBoardingApiTest extends TestCase
{
    use MakeOnBoardingTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateOnBoarding()
    {
        $onBoarding = $this->fakeOnBoardingData();
        $this->json('POST', '/api/v1/onBoardings', $onBoarding);

        $this->assertApiResponse($onBoarding);
    }

    /**
     * @test
     */
    public function testReadOnBoarding()
    {
        $onBoarding = $this->makeOnBoarding();
        $this->json('GET', '/api/v1/onBoardings/'.$onBoarding->id);

        $this->assertApiResponse($onBoarding->toArray());
    }

    /**
     * @test
     */
    public function testUpdateOnBoarding()
    {
        $onBoarding = $this->makeOnBoarding();
        $editedOnBoarding = $this->fakeOnBoardingData();

        $this->json('PUT', '/api/v1/onBoardings/'.$onBoarding->id, $editedOnBoarding);

        $this->assertApiResponse($editedOnBoarding);
    }

    /**
     * @test
     */
    public function testDeleteOnBoarding()
    {
        $onBoarding = $this->makeOnBoarding();
        $this->json('DELETE', '/api/v1/onBoardings/'.$onBoarding->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/onBoardings/'.$onBoarding->id);

        $this->assertResponseStatus(404);
    }
}
