<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OnBoardingValueApiTest extends TestCase
{
    use MakeOnBoardingValueTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateOnBoardingValue()
    {
        $onBoardingValue = $this->fakeOnBoardingValueData();
        $this->json('POST', '/api/v1/onBoardingValues', $onBoardingValue);

        $this->assertApiResponse($onBoardingValue);
    }

    /**
     * @test
     */
    public function testReadOnBoardingValue()
    {
        $onBoardingValue = $this->makeOnBoardingValue();
        $this->json('GET', '/api/v1/onBoardingValues/'.$onBoardingValue->id);

        $this->assertApiResponse($onBoardingValue->toArray());
    }

    /**
     * @test
     */
    public function testUpdateOnBoardingValue()
    {
        $onBoardingValue = $this->makeOnBoardingValue();
        $editedOnBoardingValue = $this->fakeOnBoardingValueData();

        $this->json('PUT', '/api/v1/onBoardingValues/'.$onBoardingValue->id, $editedOnBoardingValue);

        $this->assertApiResponse($editedOnBoardingValue);
    }

    /**
     * @test
     */
    public function testDeleteOnBoardingValue()
    {
        $onBoardingValue = $this->makeOnBoardingValue();
        $this->json('DELETE', '/api/v1/onBoardingValues/'.$onBoardingValue->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/onBoardingValues/'.$onBoardingValue->id);

        $this->assertResponseStatus(404);
    }
}
