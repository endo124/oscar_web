<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PageContentApiTest extends TestCase
{
    use MakePageContentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePageContent()
    {
        $pageContent = $this->fakePageContentData();
        $this->json('POST', '/api/v1/pageContents', $pageContent);

        $this->assertApiResponse($pageContent);
    }

    /**
     * @test
     */
    public function testReadPageContent()
    {
        $pageContent = $this->makePageContent();
        $this->json('GET', '/api/v1/pageContents/'.$pageContent->id);

        $this->assertApiResponse($pageContent->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePageContent()
    {
        $pageContent = $this->makePageContent();
        $editedPageContent = $this->fakePageContentData();

        $this->json('PUT', '/api/v1/pageContents/'.$pageContent->id, $editedPageContent);

        $this->assertApiResponse($editedPageContent);
    }

    /**
     * @test
     */
    public function testDeletePageContent()
    {
        $pageContent = $this->makePageContent();
        $this->json('DELETE', '/api/v1/pageContents/'.$pageContent->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/pageContents/'.$pageContent->id);

        $this->assertResponseStatus(404);
    }
}
