<?php

use App\Models\Kitchen;
use App\Repositories\KitchenRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KitchenRepositoryTest extends TestCase
{
    use MakeKitchenTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var KitchenRepository
     */
    protected $kitchenRepo;

    public function setUp()
    {
        parent::setUp();
        $this->kitchenRepo = App::make(KitchenRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateKitchen()
    {
        $kitchen = $this->fakeKitchenData();
        $createdKitchen = $this->kitchenRepo->create($kitchen);
        $createdKitchen = $createdKitchen->toArray();
        $this->assertArrayHasKey('id', $createdKitchen);
        $this->assertNotNull($createdKitchen['id'], 'Created Kitchen must have id specified');
        $this->assertNotNull(Kitchen::find($createdKitchen['id']), 'Kitchen with given id must be in DB');
        $this->assertModelData($kitchen, $createdKitchen);
    }

    /**
     * @test read
     */
    public function testReadKitchen()
    {
        $kitchen = $this->makeKitchen();
        $dbKitchen = $this->kitchenRepo->find($kitchen->id);
        $dbKitchen = $dbKitchen->toArray();
        $this->assertModelData($kitchen->toArray(), $dbKitchen);
    }

    /**
     * @test update
     */
    public function testUpdateKitchen()
    {
        $kitchen = $this->makeKitchen();
        $fakeKitchen = $this->fakeKitchenData();
        $updatedKitchen = $this->kitchenRepo->update($fakeKitchen, $kitchen->id);
        $this->assertModelData($fakeKitchen, $updatedKitchen->toArray());
        $dbKitchen = $this->kitchenRepo->find($kitchen->id);
        $this->assertModelData($fakeKitchen, $dbKitchen->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteKitchen()
    {
        $kitchen = $this->makeKitchen();
        $resp = $this->kitchenRepo->delete($kitchen->id);
        $this->assertTrue($resp);
        $this->assertNull(Kitchen::find($kitchen->id), 'Kitchen should not exist in DB');
    }
}
