<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PageItemApiTest extends TestCase
{
    use MakePageItemTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePageItem()
    {
        $pageItem = $this->fakePageItemData();
        $this->json('POST', '/api/v1/pageItems', $pageItem);

        $this->assertApiResponse($pageItem);
    }

    /**
     * @test
     */
    public function testReadPageItem()
    {
        $pageItem = $this->makePageItem();
        $this->json('GET', '/api/v1/pageItems/'.$pageItem->id);

        $this->assertApiResponse($pageItem->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePageItem()
    {
        $pageItem = $this->makePageItem();
        $editedPageItem = $this->fakePageItemData();

        $this->json('PUT', '/api/v1/pageItems/'.$pageItem->id, $editedPageItem);

        $this->assertApiResponse($editedPageItem);
    }

    /**
     * @test
     */
    public function testDeletePageItem()
    {
        $pageItem = $this->makePageItem();
        $this->json('DELETE', '/api/v1/pageItems/'.$pageItem->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/pageItems/'.$pageItem->id);

        $this->assertResponseStatus(404);
    }
}
