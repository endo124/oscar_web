<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KitchenApiTest extends TestCase
{
    use MakeKitchenTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateKitchen()
    {
        $kitchen = $this->fakeKitchenData();
        $this->json('POST', '/api/v1/kitchens', $kitchen);

        $this->assertApiResponse($kitchen);
    }

    /**
     * @test
     */
    public function testReadKitchen()
    {
        $kitchen = $this->makeKitchen();
        $this->json('GET', '/api/v1/kitchens/'.$kitchen->id);

        $this->assertApiResponse($kitchen->toArray());
    }

    /**
     * @test
     */
    public function testUpdateKitchen()
    {
        $kitchen = $this->makeKitchen();
        $editedKitchen = $this->fakeKitchenData();

        $this->json('PUT', '/api/v1/kitchens/'.$kitchen->id, $editedKitchen);

        $this->assertApiResponse($editedKitchen);
    }

    /**
     * @test
     */
    public function testDeleteKitchen()
    {
        $kitchen = $this->makeKitchen();
        $this->json('DELETE', '/api/v1/kitchens/'.$kitchen->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/kitchens/'.$kitchen->id);

        $this->assertResponseStatus(404);
    }
}
