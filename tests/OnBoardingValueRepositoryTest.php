<?php

use App\Models\OnBoardingValue;
use App\Repositories\OnBoardingValueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OnBoardingValueRepositoryTest extends TestCase
{
    use MakeOnBoardingValueTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var OnBoardingValueRepository
     */
    protected $onBoardingValueRepo;

    public function setUp()
    {
        parent::setUp();
        $this->onBoardingValueRepo = App::make(OnBoardingValueRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateOnBoardingValue()
    {
        $onBoardingValue = $this->fakeOnBoardingValueData();
        $createdOnBoardingValue = $this->onBoardingValueRepo->create($onBoardingValue);
        $createdOnBoardingValue = $createdOnBoardingValue->toArray();
        $this->assertArrayHasKey('id', $createdOnBoardingValue);
        $this->assertNotNull($createdOnBoardingValue['id'], 'Created OnBoardingValue must have id specified');
        $this->assertNotNull(OnBoardingValue::find($createdOnBoardingValue['id']), 'OnBoardingValue with given id must be in DB');
        $this->assertModelData($onBoardingValue, $createdOnBoardingValue);
    }

    /**
     * @test read
     */
    public function testReadOnBoardingValue()
    {
        $onBoardingValue = $this->makeOnBoardingValue();
        $dbOnBoardingValue = $this->onBoardingValueRepo->find($onBoardingValue->id);
        $dbOnBoardingValue = $dbOnBoardingValue->toArray();
        $this->assertModelData($onBoardingValue->toArray(), $dbOnBoardingValue);
    }

    /**
     * @test update
     */
    public function testUpdateOnBoardingValue()
    {
        $onBoardingValue = $this->makeOnBoardingValue();
        $fakeOnBoardingValue = $this->fakeOnBoardingValueData();
        $updatedOnBoardingValue = $this->onBoardingValueRepo->update($fakeOnBoardingValue, $onBoardingValue->id);
        $this->assertModelData($fakeOnBoardingValue, $updatedOnBoardingValue->toArray());
        $dbOnBoardingValue = $this->onBoardingValueRepo->find($onBoardingValue->id);
        $this->assertModelData($fakeOnBoardingValue, $dbOnBoardingValue->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteOnBoardingValue()
    {
        $onBoardingValue = $this->makeOnBoardingValue();
        $resp = $this->onBoardingValueRepo->delete($onBoardingValue->id);
        $this->assertTrue($resp);
        $this->assertNull(OnBoardingValue::find($onBoardingValue->id), 'OnBoardingValue should not exist in DB');
    }
}
