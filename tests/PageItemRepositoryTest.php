<?php

use App\Models\PageItem;
use App\Repositories\PageItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PageItemRepositoryTest extends TestCase
{
    use MakePageItemTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PageItemRepository
     */
    protected $pageItemRepo;

    public function setUp()
    {
        parent::setUp();
        $this->pageItemRepo = App::make(PageItemRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePageItem()
    {
        $pageItem = $this->fakePageItemData();
        $createdPageItem = $this->pageItemRepo->create($pageItem);
        $createdPageItem = $createdPageItem->toArray();
        $this->assertArrayHasKey('id', $createdPageItem);
        $this->assertNotNull($createdPageItem['id'], 'Created PageItem must have id specified');
        $this->assertNotNull(PageItem::find($createdPageItem['id']), 'PageItem with given id must be in DB');
        $this->assertModelData($pageItem, $createdPageItem);
    }

    /**
     * @test read
     */
    public function testReadPageItem()
    {
        $pageItem = $this->makePageItem();
        $dbPageItem = $this->pageItemRepo->find($pageItem->id);
        $dbPageItem = $dbPageItem->toArray();
        $this->assertModelData($pageItem->toArray(), $dbPageItem);
    }

    /**
     * @test update
     */
    public function testUpdatePageItem()
    {
        $pageItem = $this->makePageItem();
        $fakePageItem = $this->fakePageItemData();
        $updatedPageItem = $this->pageItemRepo->update($fakePageItem, $pageItem->id);
        $this->assertModelData($fakePageItem, $updatedPageItem->toArray());
        $dbPageItem = $this->pageItemRepo->find($pageItem->id);
        $this->assertModelData($fakePageItem, $dbPageItem->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePageItem()
    {
        $pageItem = $this->makePageItem();
        $resp = $this->pageItemRepo->delete($pageItem->id);
        $this->assertTrue($resp);
        $this->assertNull(PageItem::find($pageItem->id), 'PageItem should not exist in DB');
    }
}
