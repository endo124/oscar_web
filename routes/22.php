<?php

return [
	[
			"id" => 9230,
			"name" => "Flared 70'S Jean",
			"slug" => "flared-70s-jean",
			"permalink" => "https://mstore.io/product/flared-70s-jean/",
			"date_created" => "2015-08-01T00:48:33",
			"date_created_gmt" => "2015-07-31T17:48:33",
			"date_modified" => "2015-08-01T00:48:33",
			"date_modified_gmt" => "2015-07-31T17:48:33",
			"type" => "variable",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Stretch denim</li>\n<li>Concealed zip fly</li>\n<li>Classic five pocket styling</li>\n<li>Flared fit - cut with a straight leg that flares at the ankle</li>\n<li>Machine wash</li>\n<li>86% Cotton, 12% Polyester, 2% Elastane</li>\n<li>Our model wears a UK 8/EU 34/US XS and is 175cm/5'9” tall</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"9230\"><span class=\"woocommerce-Price-amount amount\">40.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => true,
			"purchasable" => true,
			"total_sales" => 10,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 20,
							"date_created" => "2015-08-01T00:44:26",
							"date_created_gmt" => "2015-07-31T17:44:26",
							"date_modified" => "2015-08-01T00:44:26",
							"date_modified_gmt" => "2015-07-31T17:44:26",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image1xxl.jpg",
							"name" => "image1xxl",
							"alt" => "",
							"position" => 0
					]
			],
			"attributes" => [
					[
							"id" => 1,
							"name" => "size",
							"position" => 0,
							"visible" => false,
							"variation" => true,
							"options" => [
									"L",
									"M",
									"S",
									"XL"
							]
					]
			],
			"default_attributes" => [],
			"variations" => [
					9240,
					9241,
					9242,
					9243
			],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 1243,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1248,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1272,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 1273,
							"key" => "_min_variation_price",
							"value" => "40"
					],
					[
							"id" => 1274,
							"key" => "_max_variation_price",
							"value" => "40"
					],
					[
							"id" => 1275,
							"key" => "_min_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 1276,
							"key" => "_max_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 1277,
							"key" => "_min_variation_regular_price",
							"value" => "40"
					],
					[
							"id" => 1278,
							"key" => "_max_variation_regular_price",
							"value" => "40"
					],
					[
							"id" => 1279,
							"key" => "_min_regular_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 1280,
							"key" => "_max_regular_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 1281,
							"key" => "_min_variation_sale_price",
							"value" => ""
					],
					[
							"id" => 1282,
							"key" => "_max_variation_sale_price",
							"value" => ""
					],
					[
							"id" => 1283,
							"key" => "_min_sale_price_variation_id",
							"value" => ""
					],
					[
							"id" => 1284,
							"key" => "_max_sale_price_variation_id",
							"value" => ""
					],
					[
							"id" => 1286,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/9230"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 10630,
			"name" => "Flared 70'S Jean",
			"slug" => "flared-70s-jean-2",
			"permalink" => "https://mstore.io/product/flared-70s-jean-2/",
			"date_created" => "2015-08-01T00:48:33",
			"date_created_gmt" => "2015-07-31T17:48:33",
			"date_modified" => "2015-08-01T00:48:33",
			"date_modified_gmt" => "2015-07-31T17:48:33",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Stretch denim</li>\n<li>Concealed zip fly</li>\n<li>Classic five pocket styling</li>\n<li>Flared fit - cut with a straight leg that flares at the ankle</li>\n<li>Machine wash</li>\n<li>86% Cotton, 12% Polyester, 2% Elastane</li>\n<li>Our model wears a UK 8/EU 34/US XS and is 175cm/5'9” tall</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"10630\"><span class=\"woocommerce-Price-amount amount\">40.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 0,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 20,
							"date_created" => "2015-08-01T00:44:26",
							"date_created_gmt" => "2015-07-31T17:44:26",
							"date_modified" => "2015-08-01T00:44:26",
							"date_modified_gmt" => "2015-07-31T17:44:26",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image1xxl.jpg",
							"name" => "image1xxl",
							"alt" => "",
							"position" => 0
					]
			],
			"attributes" => [
					[
							"id" => 1,
							"name" => "size",
							"position" => 0,
							"visible" => false,
							"variation" => true,
							"options" => [
									"L",
									"M",
									"S",
									"XL"
							]
					]
			],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 31233,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31234,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31239,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31263,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 31264,
							"key" => "_min_variation_price",
							"value" => "40"
					],
					[
							"id" => 31265,
							"key" => "_max_variation_price",
							"value" => "40"
					],
					[
							"id" => 31266,
							"key" => "_min_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 31267,
							"key" => "_max_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 31268,
							"key" => "_min_variation_regular_price",
							"value" => "40"
					],
					[
							"id" => 31269,
							"key" => "_max_variation_regular_price",
							"value" => "40"
					],
					[
							"id" => 31270,
							"key" => "_min_regular_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 31271,
							"key" => "_max_regular_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 31272,
							"key" => "_min_variation_sale_price",
							"value" => ""
					],
					[
							"id" => 31273,
							"key" => "_max_variation_sale_price",
							"value" => ""
					],
					[
							"id" => 31274,
							"key" => "_min_sale_price_variation_id",
							"value" => ""
					],
					[
							"id" => 31275,
							"key" => "_max_sale_price_variation_id",
							"value" => ""
					],
					[
							"id" => 31277,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 36507,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36512,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36536,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 36537,
							"key" => "_min_variation_price",
							"value" => "40"
					],
					[
							"id" => 36538,
							"key" => "_max_variation_price",
							"value" => "40"
					],
					[
							"id" => 36539,
							"key" => "_min_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 36540,
							"key" => "_max_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 36541,
							"key" => "_min_variation_regular_price",
							"value" => "40"
					],
					[
							"id" => 36542,
							"key" => "_max_variation_regular_price",
							"value" => "40"
					],
					[
							"id" => 36543,
							"key" => "_min_regular_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 36544,
							"key" => "_max_regular_price_variation_id",
							"value" => "15"
					],
					[
							"id" => 36545,
							"key" => "_min_variation_sale_price",
							"value" => ""
					],
					[
							"id" => 36546,
							"key" => "_max_variation_sale_price",
							"value" => ""
					],
					[
							"id" => 36547,
							"key" => "_min_sale_price_variation_id",
							"value" => ""
					],
					[
							"id" => 36548,
							"key" => "_max_sale_price_variation_id",
							"value" => ""
					],
					[
							"id" => 36550,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/10630"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 9231,
			"name" => "Skinny Mid Rise Jeans",
			"slug" => "skinny-mid-rise-jeans",
			"permalink" => "https://mstore.io/product/skinny-mid-rise-jeans/",
			"date_created" => "2015-08-01T00:56:49",
			"date_created_gmt" => "2015-07-31T17:56:49",
			"date_modified" => "2015-08-01T00:56:49",
			"date_modified_gmt" => "2015-07-31T17:56:49",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Mid-weight power stretch denim</li>\n<li>Classic five pocket styling</li>\n<li>Streamlined fit to the leg opening</li>\n<li>Mid rise</li>\n<li>Zip fly</li>\n<li>Super skinny fit - cut closest to the body</li>\n<li>Machine wash</li>\n<li>98% Cotton, 2% Elastane</li>\n<li>Our model wears a UK 8/EU 36/US 4 and is 175cm/5'9\" tall</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"9231\"><span class=\"woocommerce-Price-amount amount\">32.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 3,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "4.00",
			"rating_count" => 1,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 9115,
							"date_created" => "2015-08-01T00:56:27",
							"date_created_gmt" => "2015-07-31T17:56:27",
							"date_modified" => "2015-08-01T00:56:27",
							"date_modified_gmt" => "2015-07-31T17:56:27",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image1xxl-1.jpg",
							"name" => "image1xxl (1)",
							"alt" => "",
							"position" => 0
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 1287,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1292,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1316,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 1317,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 31281,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31286,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31310,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 31311,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 36554,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36559,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36583,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 36584,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/9231"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 9232,
			"name" => "Zip Detail Tapered Jeans",
			"slug" => "zip-detail-tapered-jeans",
			"permalink" => "https://mstore.io/product/zip-detail-tapered-jeans/",
			"date_created" => "2015-08-01T00:58:42",
			"date_created_gmt" => "2015-07-31T17:58:42",
			"date_modified" => "2015-08-01T00:58:42",
			"date_modified_gmt" => "2015-07-31T17:58:42",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Non-stretch denim</li>\n<li>Button fly</li>\n<li>Four pocket styling</li>\n<li>Contrast stitching</li>\n<li>Zipped cuffs</li>\n<li>Tapered fit - cut loosely around the thigh and tapered from the knee to the ankle</li>\n<li>Machine wash</li>\n<li>100% Cotton</li>\n<li>Our model wears a UK 8/EU 36/US 4 and is 175cm/5'9\" tall</li>\n<li>Colour may transfer during washing and wear</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"9232\"><span class=\"woocommerce-Price-amount amount\">50.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 1,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "4.00",
			"rating_count" => 1,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 9120,
							"date_created" => "2015-08-01T00:57:59",
							"date_created_gmt" => "2015-07-31T17:57:59",
							"date_modified" => "2015-08-01T00:57:59",
							"date_modified_gmt" => "2015-07-31T17:57:59",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image1xxl-2.jpg",
							"name" => "image1xxl (2)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 22,
							"date_created" => "2015-08-01T00:44:36",
							"date_created_gmt" => "2015-07-31T17:44:36",
							"date_modified" => "2015-08-01T00:44:36",
							"date_modified_gmt" => "2015-07-31T17:44:36",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image2xxl.jpg",
							"name" => "image2xxl",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 24,
							"date_created" => "2015-08-01T00:44:38",
							"date_created_gmt" => "2015-07-31T17:44:38",
							"date_modified" => "2015-08-01T00:44:38",
							"date_modified_gmt" => "2015-07-31T17:44:38",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image3xxl.jpg",
							"name" => "image3xxl",
							"alt" => "",
							"position" => 3
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 1318,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1323,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1347,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 1348,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 31315,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31320,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31344,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 31345,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 36588,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36593,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36617,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 36618,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/9232"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 9233,
			"name" => "Black with Thigh Rip and Busted Knees",
			"slug" => "black-with-thigh-rip-and-busted-knees",
			"permalink" => "https://mstore.io/product/black-with-thigh-rip-and-busted-knees/",
			"date_created" => "2015-08-01T01:04:18",
			"date_created_gmt" => "2015-07-31T18:04:18",
			"date_modified" => "2015-08-01T01:04:18",
			"date_modified_gmt" => "2015-07-31T18:04:18",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Made from super soft high-stretch denim</li>\n<li>Stretch added for comfort and fit</li>\n<li>High-waisted cut</li>\n<li>Ultra skinny fit through the leg</li>\n<li>Ripped detailing</li>\n<li>Contoured bottom lifting back pockets</li>\n<li>Button and concealed zip-fly fastening</li>\n<li>Skinny fit - cut closely to body</li>\n<li>Machine wash</li>\n<li>67% Cotton, 19% Polyester, 11% Viscose, 3% Elastane</li>\n<li>Our model wears a UK 8/EU 36/US 4 and is cm/\" tall</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"9233\"><span class=\"woocommerce-Price-amount amount\">35.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 4,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 9129,
							"date_created" => "2015-08-01T01:03:30",
							"date_created_gmt" => "2015-07-31T18:03:30",
							"date_modified" => "2015-08-01T01:03:30",
							"date_modified_gmt" => "2015-07-31T18:03:30",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image1xxl-3.jpg",
							"name" => "image1xxl (3)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 27,
							"date_created" => "2016-06-11T17:25:50",
							"date_created_gmt" => "2016-06-11T10:25:50",
							"date_modified" => "2016-06-11T17:25:50",
							"date_modified_gmt" => "2016-06-11T10:25:50",
							"src" => "https://mstore.io/wp-content/uploads/2016/06/eraser1.png",
							"name" => "eraser1.png",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 28,
							"date_created" => "2016-06-11T17:25:51",
							"date_created_gmt" => "2016-06-11T10:25:51",
							"date_modified" => "2016-06-11T17:25:51",
							"date_modified_gmt" => "2016-06-11T10:25:51",
							"src" => "https://mstore.io/wp-content/uploads/2016/06/glasses.png",
							"name" => "glasses.png",
							"alt" => "",
							"position" => 2
					],
					[
							"id" => 29,
							"date_created" => "2016-06-11T17:25:51",
							"date_created_gmt" => "2016-06-11T10:25:51",
							"date_modified" => "2016-06-11T17:25:51",
							"date_modified_gmt" => "2016-06-11T10:25:51",
							"src" => "https://mstore.io/wp-content/uploads/2016/06/mouse.png",
							"name" => "mouse.png",
							"alt" => "",
							"position" => 3
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 1349,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1354,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1378,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 1380,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 31349,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31354,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31378,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 31380,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 36622,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36627,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36651,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 36653,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/9233"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 30,
			"name" => "Rivington High Waist Denim Jeggings",
			"slug" => "rivington-high-waist-denim-jeggings",
			"permalink" => "https://mstore.io/product/rivington-high-waist-denim-jeggings/",
			"date_created" => "2015-08-01T01:06:48",
			"date_created_gmt" => "2015-07-31T18:06:48",
			"date_modified" => "2015-08-01T01:06:48",
			"date_modified_gmt" => "2015-07-31T18:06:48",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Smooth lightweight fabric</li>\n<li>High performing stretch denim</li>\n<li>Super high rise</li>\n<li>Super skinny fit down to the leg opening</li>\n<li>Machine wash</li>\n<li>61% Cotton, 37% Polyester, 2% Elastane</li>\n<li>Our model wears a UK 8/EU 36/US 4 and is 175cm/5'9” tall</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"30\"><span class=\"woocommerce-Price-amount amount\">30.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 0,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [
					329,
					36,
					9233,
					81,
					198
			],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [
					[
							"id" => 29,
							"name" => "Jeans",
							"slug" => "jeans"
					],
					[
							"id" => 29,
							"name" => "Jeans",
							"slug" => "jeans"
					],
					[
							"id" => 29,
							"name" => "Jeans",
							"slug" => "jeans"
					],
					[
							"id" => 29,
							"name" => "Jeans",
							"slug" => "jeans"
					],
					[
							"id" => 29,
							"name" => "Jeans",
							"slug" => "jeans"
					],
					[
							"id" => 29,
							"name" => "Jeans",
							"slug" => "jeans"
					],
					[
							"id" => 29,
							"name" => "Jeans",
							"slug" => "jeans"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					]
			],
			"tags" => [
					[
							"id" => 13,
							"name" => "ripped",
							"slug" => "ripped"
					],
					[
							"id" => 13,
							"name" => "ripped",
							"slug" => "ripped"
					],
					[
							"id" => 13,
							"name" => "ripped",
							"slug" => "ripped"
					]
			],
			"images" => [
					[
							"id" => 31,
							"date_created" => "2015-08-01T01:05:58",
							"date_created_gmt" => "2015-07-31T18:05:58",
							"date_modified" => "2015-08-01T01:05:58",
							"date_modified_gmt" => "2015-07-31T18:05:58",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image1xxl-4.jpg",
							"name" => "image1xxl (4)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 32,
							"date_created" => "2015-08-01T01:06:15",
							"date_created_gmt" => "2015-07-31T18:06:15",
							"date_modified" => "2015-08-01T01:06:15",
							"date_modified_gmt" => "2015-07-31T18:06:15",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image2xxl-4.jpg",
							"name" => "image2xxl (4)",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 33,
							"date_created" => "2015-08-01T01:06:17",
							"date_created_gmt" => "2015-07-31T18:06:17",
							"date_modified" => "2015-08-01T01:06:17",
							"date_modified_gmt" => "2015-07-31T18:06:17",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image3xxl-3.jpg",
							"name" => "image3xxl (3)",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 1381,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1386,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1410,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 1412,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 30992,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 30997,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31021,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 31023,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 36266,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36271,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36295,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 36297,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 168885,
							"key" => "_wpml_media_duplicate",
							"value" => "1"
					],
					[
							"id" => 168886,
							"key" => "_wpml_media_featured",
							"value" => "1"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/30"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 36,
			"name" => "Customised Vintage Fit Ripped Jeans",
			"slug" => "customised-vintage-fit-ripped-jeans",
			"permalink" => "https://mstore.io/product/customised-vintage-fit-ripped-jeans/",
			"date_created" => "2015-08-01T01:09:00",
			"date_created_gmt" => "2015-07-31T18:09:00",
			"date_modified" => "2015-08-01T01:09:00",
			"date_modified_gmt" => "2015-07-31T18:09:00",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Non-stretch denim</li>\n<li>Button fly</li>\n<li>Classic five pocket styling</li>\n<li>Ripped detail</li>\n<li>Regular fit - true to size</li>\n<li>100% Cotton</li>\n<li>Machine wash</li>\n<li>100% Cotton</li>\n<li>Our model wears a UK 8/EU 36/US 4 and is 178 cm/5'10” tall</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"36\"><span class=\"woocommerce-Price-amount amount\">95.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 1,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "4.00",
			"rating_count" => 1,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 9205,
							"date_created" => "2015-08-01T01:07:49",
							"date_created_gmt" => "2015-07-31T18:07:49",
							"date_modified" => "2015-08-01T01:07:49",
							"date_modified_gmt" => "2015-07-31T18:07:49",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image1xxl-5.jpg",
							"name" => "image1xxl (5)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 38,
							"date_created" => "2015-08-01T01:08:41",
							"date_created_gmt" => "2015-07-31T18:08:41",
							"date_modified" => "2015-08-01T01:08:41",
							"date_modified_gmt" => "2015-07-31T18:08:41",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image2xxl-5.jpg",
							"name" => "image2xxl (5)",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 39,
							"date_created" => "2016-06-11T17:28:18",
							"date_created_gmt" => "2016-06-11T10:28:18",
							"date_modified" => "2016-06-11T17:28:18",
							"date_modified_gmt" => "2016-06-11T10:28:18",
							"src" => "https://mstore.io/wp-content/uploads/2016/06/ipad_big-1.png",
							"name" => "ipad_big-1.png",
							"alt" => "",
							"position" => 2
					],
					[
							"id" => 40,
							"date_created" => "2015-08-01T01:08:46",
							"date_created_gmt" => "2015-07-31T18:08:46",
							"date_modified" => "2015-08-01T01:08:46",
							"date_modified_gmt" => "2015-07-31T18:08:46",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image4xxl-4.jpg",
							"name" => "image4xxl (4)",
							"alt" => "",
							"position" => 3
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 1413,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1418,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1442,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 1444,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 31027,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31032,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31056,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 31058,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 36301,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36306,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36330,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 36332,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 168968,
							"key" => "_wpml_media_duplicate",
							"value" => "1"
					],
					[
							"id" => 168969,
							"key" => "_wpml_media_featured",
							"value" => "1"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/36"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 16506,
			"name" => "Customised Vintage Fit Ripped Jeans",
			"slug" => "customised-vintage-fit-ripped-jeans-4",
			"permalink" => "https://mstore.io/product/customised-vintage-fit-ripped-jeans-4/",
			"date_created" => "2015-08-01T01:09:00",
			"date_created_gmt" => "2015-07-31T18:09:00",
			"date_modified" => "2015-08-01T01:09:00",
			"date_modified_gmt" => "2015-07-31T18:09:00",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Non-stretch denim</li>\n<li>Button fly</li>\n<li>Classic five pocket styling</li>\n<li>Ripped detail</li>\n<li>Regular fit - true to size</li>\n<li>100% Cotton</li>\n<li>Machine wash</li>\n<li>100% Cotton</li>\n<li>Our model wears a UK 8/EU 36/US 4 and is 178 cm/5'10” tall</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"16506\"></span>",
			"on_sale" => false,
			"purchasable" => false,
			"total_sales" => "0",
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 0,
							"date_created" => "2019-08-21T03:47:02",
							"date_created_gmt" => "2019-08-20T13:47:02",
							"date_modified" => "2019-08-21T03:47:02",
							"date_modified_gmt" => "2019-08-20T13:47:02",
							"src" => "https://mstore.io/wp-content/plugins/woocommerce/assets/images/placeholder.png",
							"name" => "Placeholder",
							"alt" => "Placeholder",
							"position" => 0
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 253676,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 253677,
							"key" => "_wp_attachment_metadata",
							"value" => ""
					],
					[
							"id" => 253678,
							"key" => "wpml_media_processed",
							"value" => "1"
					],
					[
							"id" => 253679,
							"key" => "_wp_attached_file",
							"value" => ""
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/16506"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 9234,
			"name" => "Tailored Waistcoat Co-ord",
			"slug" => "tailored-waistcoat-co-ord",
			"permalink" => "https://mstore.io/product/tailored-waistcoat-co-ord/",
			"date_created" => "2015-08-01T01:11:48",
			"date_created_gmt" => "2015-07-31T18:11:48",
			"date_modified" => "2019-07-10T18:01:33",
			"date_modified_gmt" => "2019-07-10T11:01:33",
			"type" => "simple",
			"status" => "publish",
			"featured" => true,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Lightweight lined crepe</li>\n<li>V-neckline</li>\n<li>Button placket</li>\n<li>Faux front pockets</li>\n<li>Silk-feel panel to back with adjustable cinch</li>\n<li>Regular fit - true to size</li>\n<li>Machine wash</li>\n<li>95% Polyester, 5% Elastane</li>\n<li>Our model wears a UK 8/EU 36/US 4</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"9234\"><span class=\"woocommerce-Price-amount amount\">35.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 0,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [
					53,
					9236,
					9235
			],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [
					[
							"id" => 25,
							"name" => "Blazers",
							"slug" => "blazers"
					],
					[
							"id" => 25,
							"name" => "Blazers",
							"slug" => "blazers"
					]
			],
			"tags" => [],
			"images" => [
					[
							"id" => 42,
							"date_created" => "2015-08-01T01:11:29",
							"date_created_gmt" => "2015-07-31T18:11:29",
							"date_modified" => "2015-08-01T01:11:29",
							"date_modified_gmt" => "2015-07-31T18:11:29",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image1xxl-6.jpg",
							"name" => "image1xxl (6)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 43,
							"date_created" => "2015-08-01T01:11:36",
							"date_created_gmt" => "2015-07-31T18:11:36",
							"date_modified" => "2015-08-01T01:11:36",
							"date_modified_gmt" => "2015-07-31T18:11:36",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image2xxl1.jpg",
							"name" => "image2xxl",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 44,
							"date_created" => "2016-06-11T17:28:39",
							"date_created_gmt" => "2016-06-11T10:28:39",
							"date_modified" => "2016-06-11T17:28:39",
							"date_modified_gmt" => "2016-06-11T10:28:39",
							"src" => "https://mstore.io/wp-content/uploads/2016/06/chair.png",
							"name" => "chair.png",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 1445,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1450,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1474,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 1475,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 31384,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31389,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31413,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 31414,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 36657,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36662,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36686,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 36687,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/9234"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 9235,
			"name" => "Ponte Blazer",
			"slug" => "ponte-blazer",
			"permalink" => "https://mstore.io/product/ponte-blazer/",
			"date_created" => "2015-08-01T01:12:57",
			"date_created_gmt" => "2015-07-31T18:12:57",
			"date_modified" => "2015-08-01T01:12:57",
			"date_modified_gmt" => "2015-07-31T18:12:57",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Soft touch ponte fabric</li>\n<li>Notched lapels</li>\n<li>Button front fastening</li>\n<li>Patch pockets</li>\n<li>Regular fit - true to size</li>\n<li>Machine wash</li>\n<li>87% Polyester, 10% Viscose, 3% Elastane</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"9235\"><span class=\"woocommerce-Price-amount amount\">35.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 4,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 46,
							"date_created" => "2015-08-01T01:12:36",
							"date_created_gmt" => "2015-07-31T18:12:36",
							"date_modified" => "2015-08-01T01:12:36",
							"date_modified_gmt" => "2015-07-31T18:12:36",
							"src" => "https://mstore.io/wp-content/uploads/2015/07/image1xxl1.jpg",
							"name" => "image1xxl",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 47,
							"date_created" => "2016-06-11T17:28:41",
							"date_created_gmt" => "2016-06-11T10:28:41",
							"date_modified" => "2016-06-11T17:28:41",
							"date_modified_gmt" => "2016-06-11T10:28:41",
							"src" => "https://mstore.io/wp-content/uploads/2016/06/glasses-1.png",
							"name" => "glasses.png",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 48,
							"date_created" => "2016-06-11T17:28:41",
							"date_created_gmt" => "2016-06-11T10:28:41",
							"date_modified" => "2016-06-11T17:28:41",
							"date_modified_gmt" => "2016-06-11T10:28:41",
							"src" => "https://mstore.io/wp-content/uploads/2016/06/coffee_cup.png",
							"name" => "coffee_cup.png",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 1476,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1481,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 1505,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 1506,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 31418,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31423,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 31447,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 31448,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 36691,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36696,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 36720,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 36721,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/9235"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	]
];