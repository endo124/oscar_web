<?php

use Illuminate\Http\Request;

Route::get('/products/categories', 'CategoryAPIController@index');
Route::get('/products', 'ProductAPIController@index');
Route::get('/products/{categoryId}', 'ProductAPIController@getProductsByCategory');
Route::get('/stores/locations', 'BranchAPIController@index');
Route::get('/kitchen/recipes', 'KitchenAPIController@index');
