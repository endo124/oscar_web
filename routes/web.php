<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Use App\Models\Customer;
// Route::get('/', 'HomeController@index');
// Route::get('/log_out_chat/{id}', 'LoginChatController@logout');
// Route::get('/customer/verify/{token}', 'API\APIRegisterController@verifyCustomer')->name('customer.verify');
// Route::get('/customer/reset/{token}', 'API\APIForgotPasswordController@showForgetForm');
// Route::post('/customer/forget', 'API\APIForgotPasswordController@forget_password_save');

// Route::get('login_chat', 'LoginChatController@index');
// Route::post('login_chat', 'LoginChatController@login');
// Route::get('chat/{id}/{customer}/{client_id}', function() {
// $customer=Customer::find( Route::current()->customer);

// $token = auth('customers')->login($customer);
// 			return view('chat.users_chat');
// 		});
// 		Route::get('users_chat/{id}', function() {
// 			$customer=Customer::find( Route::current()->id);
	
// 			$token = auth('customers')->login($customer);
// 						return view('chat.users_chat');
// 					});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::post('/sendPN', 'HomeController@sendPN');
// Route::post('/upload', 'MediaController@upload');
// Route::get('import', 'PromotionController@import');
// Route::post('import', 'PromotionController@import');
// Route::get('cache_clear', 'ProductController@cache');


// Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
// 	Route::resource('pages', 'PageController');
// 	Route::resource('categories', 'CategoryController');
// 	Route::resource('pageContents', 'PageContentController');
// 	Route::resource('restaurants', 'RestaurantController');
// 	Route::resource('promotions', 'PromotionController');
// 	Route::delete('promotions_all', 'PromotionController@destroyAll')->name('promotions.destroy_all');
// 	Route::resource('onBoardings', 'OnBoardingController');
// 	Route::resource('onBoardingValues', 'OnBoardingValueController');
// });

// Route::resource('users', 'UserController');
// Route::resource('roles', 'RoleController');
// Route::resource('permissions', 'PermissionController');
// Route::resource('pageTypes', 'PageTypeController');
// Route::resource('pageItems', 'PageItemController');
// Route::get('productCategories/import', 'ProductCategoryController@import');
// Route::post('productCategories/import', 'ProductCategoryController@import');
// Route::any('/productCategories/selectCategory', 'ProductCategoryController@selectCategory')->name('productCategories.selectCategory');

// Route::any('/productCategories/syncAPI', 'ProductCategoryController@syncAPI')->name('productCategories.syncAPI');
// Route::resource('productCategories', 'ProductCategoryController');
// Route::resource('notifications', 'NotificationController');
// Route::resource('imageSlider', 'ImageSliderController');
// Route::resource('customerService', 'CustomerServiceController');
// Route::get('products/import', 'ProductController@import');
// Route::post('products/import', 'ProductController@import');
// Route::get('/downloadProductSample', 'ProductController@getDownload');
// Route::resource('products', 'ProductController');
// Route::post('products/search', 'ProductController@search');
// Route::any('/products/syncAPI', 'ProductController@syncAPI')->name('products.syncAPI');
// Route::post('/products/selectImages', 'ProductController@selectImages')->name('products.selectImages');
// Route::get('/products/selectImages/{filter}', 'ProductController@getselectImages')->name('products.selectImages');

// Route::get('qrCode', 'HomeController@qrcode')->name('qrCode.index');

// Route::resource('devices', 'DeviceController');
// Route::resource('contactForms', 'ContactFormController');
// Route::resource('media', 'MediaController');
// Route::resource('orders', 'OrderController');
// Route::post('/order/status', 'OrderController@changeStatus')->name('order.status');
// // MediaManager
// // ctf0\MediaManager\MediaRoutes::routes();

// Route::resource('branches', 'BranchController');
// Route::resource('kitchens', 'KitchenController');
// Route::resource('promoVideos', 'PromoVideoController');
// Route::post('promoVideos/{promoVideo}/activate', 'PromoVideoController@activate')->name('promoVideos.activate');

// Route::get('uploads', function() {
// 	$files = \Storage::allFiles("public/media");
// 	return view('uploads.index')->with('files', $files);
// })->name('uploads.index');

// // Auth::routes(['verify' => true]);


// Route::get('lang/{lang}', function ($lang) {
//     App::setlocale($lang);

//     return redirect()->back();
// });

Route::get('/homeWeb', function () {

    return redirect(app()->getLocale());
});
Route::group(['prefix' => '{locale}','namespace'=>'Web'  ,'where' => ['locale' => '[a-zA-Z]{2}'], 
'middleware' => 'setlocale'],function(){

	Route::get('homeWeb/','HomeController@redirect')->name('homeWeb');
	Route::any('homeWeb/search_product','HomeController@search')->name('search');
	Route::resource('homeWeb','HomeController',['except'=> ['index']]);
	Route::get('homeWeb/branch/{id}','HomeController@index')->name('homeWeb.index');
	Route::get('/show_product/{id}', 'HomeController@show_product');

	Route::post('homeWeb/wishList','HomeController@wishList');
	Route::post('homeWeb/unLike','HomeController@unLike');
	Route::get('homeWeb/all_products/{slug}','HomeController@showallproducts');
	Route::get('homeWeb/show/{slug}','HomeController@show');
	Route::post('login','LoginController@login')->name('web.login');
	Route::post('register','RegisterController@register')->name('web.register');
	Route::get('/customer/verify/{token}', 'RegisterController@verifyCustomer')->name('customer.verify');
	Route::post('logout','LoginController@logout')->name('web.logout');
	Route::get('forgot_password', 'LoginController@forgot_password')->name('web.forgot_password');
	Route::post('forgot', 'LoginController@forgot')->name('web.forgot');

	Route::get('cart', 'HomeController@cart')->name('cart');
	Route::post('homeWeb/add-to-cart', 'HomeController@addToCart');
	Route::get('checkout', 'HomeController@checkout')->name('checkout');
	Route::get('contact', 'HomeController@contact')->name('contact');
	Route::get('aboutus', 'HomeController@aboutus')->name('aboutus');
	Route::resource('profile', 'CustomerController');
	Route::get('profile/wishlist', 'CustomerController@wishlist')->name('wishlist');
	Route::post('profile/email', 'CustomerController@email');

	Route::resource('restaurant', 'RestaurantController');
	Route::resource('department', 'DepartmentController');
	Route::resource('address', 'AddressController');
	Route::get('address/{id}', 'AddressController@delete')->name('addresss.delete');
	Route::post('address/getaddress','AddressController@getAddress');


	// Route::get('/verify_user', function()  {
	// 	$url = 'http://41.33.238.100/RR_OSCAR/RR_Services.asmx/VerifyUserNew';
	// 	$body = [
	// 		'EMail' =>'marcos.moien@oscarstores.com',
	// 		'Phone' => '01275900400',
	// 	];
	
	// 	$ch = curl_init();
	// 	curl_setopt($ch, CURLOPT_URL, $url);
	// 	curl_setopt($ch, CURLOPT_POST, 1);
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($body));
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 	$server_output = curl_exec($ch);
	// 	curl_close($ch);
	
	// 	return $server_output;
	// });


	

});


