<?php

return [
  [
    "id" => 419,
    "name" => "Longline Sleeveless T-Shirt With Dropped Armhole And Print",
    "slug" => "longline-sleeveless-t-shirt-with-dropped-armhole-and-print",
    "permalink" => "https://mstore.io/product/longline-sleeveless-t-shirt-with-dropped-armhole-and-print/",
    "date_created" => "2015-08-13T22:47:07",
    "date_created_gmt" => "2015-08-13T15:47:07",
    "date_modified" => "2017-06-11T21:43:47",
    "date_modified_gmt" => "2017-06-11T14:43:47",
    "type" => "simple",
    "status" => "publish",
    "featured" => false,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Soft-touch jersey</li>\n<li>Crew neck</li>\n<li>Printed front</li>\n<li>Raw sleeve seams</li>\n<li>Dropped armholes</li>\n<li>Longline cut</li>\n<li>Cut longer than standard length</li>\n<li>Machine wash</li>\n<li>100% Cotton</li>\n</ul>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"419\"><span class=\"woocommerce-Price-amount amount\">24.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
    "on_sale" => false,
    "purchasable" => true,
    "total_sales" => 44,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => false,
    "stock_quantity" => null,
    "in_stock" => true,
    "backorders" => "no",
    "backorders_allowed" => false,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "0.00",
    "rating_count" => 0,
    "related_ids" => [
      456,
      452,
      448,
      440,
      427
    ],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ],
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ],
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ],
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ],
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ],
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ],
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ],
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ],
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ],
      [
        "id" => 18,
        "name" => "Men",
        "slug" => "men"
      ]
    ],
    "tags" => [],
    "images" => [
      [
        "id" => 420,
        "date_created" => "2015-08-13T22:46:51",
        "date_created_gmt" => "2015-08-13T15:46:51",
        "date_modified" => "2015-08-13T22:46:51",
        "date_modified_gmt" => "2015-08-13T15:46:51",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl9.jpg",
        "name" => "image1xxl",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 422,
        "date_created" => "2015-08-13T22:46:55",
        "date_created_gmt" => "2015-08-13T15:46:55",
        "date_modified" => "2015-08-13T22:46:55",
        "date_modified_gmt" => "2015-08-13T15:46:55",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl4.jpg",
        "name" => "image4xxl",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 421,
        "date_created" => "2015-08-13T22:46:53",
        "date_created_gmt" => "2015-08-13T15:46:53",
        "date_modified" => "2015-08-13T22:46:53",
        "date_modified_gmt" => "2015-08-13T15:46:53",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl8.jpg",
        "name" => "image2xxl",
        "alt" => "",
        "position" => 2
      ]
    ],
    "attributes" => [],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4312,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4317,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4342,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 4343,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34399,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34404,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34429,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 34430,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39670,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39675,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39700,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 39701,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 173031,
        "key" => "_wpml_media_duplicate",
        "value" => "1"
      ],
      [
        "id" => 173032,
        "key" => "_wpml_media_featured",
        "value" => "1"
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/419"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ],
  [
    "id" => 423,
    "name" => "Sleeveless T-Shirt With Extreme Dropped Armholes And Applique",
    "slug" => "sleeveless-t-shirt-with-extreme-dropped-armholes-and-applique",
    "permalink" => "https://mstore.io/product/sleeveless-t-shirt-with-extreme-dropped-armholes-and-applique/",
    "date_created" => "2015-08-13T15:49:20",
    "date_created_gmt" => "2015-08-13T08:49:20",
    "date_modified" => "2017-06-15T11:45:14",
    "date_modified_gmt" => "2017-06-15T04:45:14",
    "type" => "simple",
    "status" => "publish",
    "featured" => true,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Soft-touch jersey</li>\n<li>Crew neck</li>\n<li>Raw-edged sleeves</li>\n<li>Dropped armholes</li>\n<li>Applique design</li>\n<li>Regular fit - true to size</li>\n<li>Machine wash</li>\n<li>100% Cotton</li>\n</ul>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"423\"><span class=\"woocommerce-Price-amount amount\">26.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
    "on_sale" => false,
    "purchasable" => true,
    "total_sales" => 55,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => false,
    "stock_quantity" => null,
    "in_stock" => true,
    "backorders" => "no",
    "backorders_allowed" => false,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "0.00",
    "rating_count" => 0,
    "related_ids" => [
      275,
      262,
      266,
      300,
      242
    ],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [],
    "tags" => [],
    "images" => [
      [
        "id" => 424,
        "date_created" => "2015-08-13T22:47:43",
        "date_created_gmt" => "2015-08-13T15:47:43",
        "date_modified" => "2015-08-13T22:47:43",
        "date_modified_gmt" => "2015-08-13T15:47:43",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-17.jpg",
        "name" => "image1xxl (1)",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 426,
        "date_created" => "2015-08-13T22:47:48",
        "date_created_gmt" => "2015-08-13T15:47:48",
        "date_modified" => "2015-08-13T22:47:48",
        "date_modified_gmt" => "2015-08-13T15:47:48",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-14.jpg",
        "name" => "image4xxl (1)",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 425,
        "date_created" => "2015-08-13T22:47:46",
        "date_created_gmt" => "2015-08-13T15:47:46",
        "date_modified" => "2015-08-13T22:47:46",
        "date_modified_gmt" => "2015-08-13T15:47:46",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-17.jpg",
        "name" => "image2xxl (1)",
        "alt" => "",
        "position" => 2
      ]
    ],
    "attributes" => [],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4344,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4349,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4374,
        "key" => "slide_template",
        "value" => ""
      ],
      [
        "id" => 4375,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34434,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34439,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34464,
        "key" => "slide_template",
        "value" => ""
      ],
      [
        "id" => 34465,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39705,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39710,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39735,
        "key" => "slide_template",
        "value" => ""
      ],
      [
        "id" => 39736,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 173115,
        "key" => "_wpml_media_duplicate",
        "value" => "1"
      ],
      [
        "id" => 173116,
        "key" => "_wpml_media_featured",
        "value" => "1"
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/423"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ],
  [
    "id" => 427,
    "name" => "Muscle Fit Long Sleeve T-Shirt With Burn Out Rib Jersey",
    "slug" => "muscle-fit-long-sleeve-t-shirt-with-burn-out-rib-jersey",
    "permalink" => "https://mstore.io/product/muscle-fit-long-sleeve-t-shirt-with-burn-out-rib-jersey/",
    "date_created" => "2015-08-13T22:50:57",
    "date_created_gmt" => "2015-08-13T15:50:57",
    "date_modified" => "2017-06-11T21:43:47",
    "date_modified_gmt" => "2017-06-11T14:43:47",
    "type" => "simple",
    "status" => "publish",
    "featured" => false,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Ribbed jersey</li>\n<li>Scoop neck</li>\n<li>Longline cut</li>\n<li>Cut longer than standard length</li>\n<li>Machine wash</li>\n<li>Our model wears a size Medium and is 185.5cm/6'1\" tall</li>\n</ul>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"427\"><span class=\"woocommerce-Price-amount amount\">30.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
    "on_sale" => false,
    "purchasable" => true,
    "total_sales" => 69,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => false,
    "stock_quantity" => null,
    "in_stock" => true,
    "backorders" => "no",
    "backorders_allowed" => false,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "0.00",
    "rating_count" => 0,
    "related_ids" => [],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [],
    "tags" => [],
    "images" => [
      [
        "id" => 428,
        "date_created" => "2015-08-13T22:50:15",
        "date_created_gmt" => "2015-08-13T15:50:15",
        "date_modified" => "2015-08-13T22:50:15",
        "date_modified_gmt" => "2015-08-13T15:50:15",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-27.jpg",
        "name" => "image1xxl (2)",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 429,
        "date_created" => "2015-08-13T22:50:17",
        "date_created_gmt" => "2015-08-13T15:50:17",
        "date_modified" => "2015-08-13T22:50:17",
        "date_modified_gmt" => "2015-08-13T15:50:17",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-27.jpg",
        "name" => "image2xxl (2)",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 430,
        "date_created" => "2015-08-13T22:50:20",
        "date_created_gmt" => "2015-08-13T15:50:20",
        "date_modified" => "2015-08-13T22:50:20",
        "date_modified_gmt" => "2015-08-13T15:50:20",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-24.jpg",
        "name" => "image4xxl (2)",
        "alt" => "",
        "position" => 2
      ]
    ],
    "attributes" => [],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4376,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4381,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4406,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 4407,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34469,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34474,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34499,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 34500,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39740,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39745,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39770,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 39771,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 173195,
        "key" => "_wpml_media_duplicate",
        "value" => "1"
      ],
      [
        "id" => 173196,
        "key" => "_wpml_media_featured",
        "value" => "1"
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/427"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ],
  [
    "id" => 431,
    "name" => "Long Sleeve T-Shirt With V Neck In Grey Marl",
    "slug" => "long-sleeve-t-shirt-with-v-neck-in-grey-marl",
    "permalink" => "https://mstore.io/product/long-sleeve-t-shirt-with-v-neck-in-grey-marl/",
    "date_created" => "2015-08-13T22:52:09",
    "date_created_gmt" => "2015-08-13T15:52:09",
    "date_modified" => "2017-06-11T21:43:47",
    "date_modified_gmt" => "2017-06-11T14:43:47",
    "type" => "simple",
    "status" => "publish",
    "featured" => false,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Soft-touch, marl jersey</li>\n<li>V-neck</li>\n<li>Regular fit - true to size</li>\n<li>Machine wash</li>\n<li>100% Cotton</li>\n</ul>\n<p>&nbsp;</p>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"431\"><span class=\"woocommerce-Price-amount amount\">18.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
    "on_sale" => false,
    "purchasable" => true,
    "total_sales" => 44,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => false,
    "stock_quantity" => null,
    "in_stock" => true,
    "backorders" => "no",
    "backorders_allowed" => false,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "0.00",
    "rating_count" => 0,
    "related_ids" => [
      304,
      287,
      329,
      316,
      271
    ],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [],
    "tags" => [],
    "images" => [
      [
        "id" => 432,
        "date_created" => "2015-08-13T22:51:43",
        "date_created_gmt" => "2015-08-13T15:51:43",
        "date_modified" => "2015-08-13T22:51:43",
        "date_modified_gmt" => "2015-08-13T15:51:43",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-37.jpg",
        "name" => "image1xxl (3)",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 434,
        "date_created" => "2015-08-13T22:51:48",
        "date_created_gmt" => "2015-08-13T15:51:48",
        "date_modified" => "2015-08-13T22:51:48",
        "date_modified_gmt" => "2015-08-13T15:51:48",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-33.jpg",
        "name" => "image4xxl (3)",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 433,
        "date_created" => "2015-08-13T22:51:46",
        "date_created_gmt" => "2015-08-13T15:51:46",
        "date_modified" => "2015-08-13T22:51:46",
        "date_modified_gmt" => "2015-08-13T15:51:46",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-37.jpg",
        "name" => "image2xxl (3)",
        "alt" => "",
        "position" => 2
      ]
    ],
    "attributes" => [],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4408,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4413,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4438,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 4439,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34504,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34509,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34534,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 34535,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39775,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39780,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39805,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 39806,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 173275,
        "key" => "_wpml_media_duplicate",
        "value" => "1"
      ],
      [
        "id" => 173276,
        "key" => "_wpml_media_featured",
        "value" => "1"
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/431"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ],
  [
    "id" => 436,
    "name" => "3/4 Length Sleeve T-Shirt",
    "slug" => "34-length-sleeve-t-shirt",
    "permalink" => "https://mstore.io/product/34-length-sleeve-t-shirt/",
    "date_created" => "2015-08-13T22:53:21",
    "date_created_gmt" => "2015-08-13T15:53:21",
    "date_modified" => "2017-06-11T21:43:47",
    "date_modified_gmt" => "2017-06-11T14:43:47",
    "type" => "simple",
    "status" => "publish",
    "featured" => false,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Soft-touch jersey</li>\n<li>Crew neck</li>\n<li>Printed sleeves</li>\n<li>Embroidered logo</li>\n<li>Regular fit - true to size</li>\n<li>Machine wash</li>\n<li>100% Cotton</li>\n</ul>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"436\"><span class=\"woocommerce-Price-amount amount\">40.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
    "on_sale" => false,
    "purchasable" => true,
    "total_sales" => 69,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => false,
    "stock_quantity" => null,
    "in_stock" => true,
    "backorders" => "no",
    "backorders_allowed" => false,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "4.80",
    "rating_count" => 5,
    "related_ids" => [],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [],
    "tags" => [],
    "images" => [
      [
        "id" => 437,
        "date_created" => "2015-08-13T22:53:04",
        "date_created_gmt" => "2015-08-13T15:53:04",
        "date_modified" => "2015-08-13T22:53:04",
        "date_modified_gmt" => "2015-08-13T15:53:04",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-47.jpg",
        "name" => "image1xxl (4)",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 439,
        "date_created" => "2015-08-13T22:53:09",
        "date_created_gmt" => "2015-08-13T15:53:09",
        "date_modified" => "2015-08-13T22:53:09",
        "date_modified_gmt" => "2015-08-13T15:53:09",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-44.jpg",
        "name" => "image4xxl (4)",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 438,
        "date_created" => "2015-08-13T22:53:07",
        "date_created_gmt" => "2015-08-13T15:53:07",
        "date_modified" => "2015-08-13T22:53:07",
        "date_modified_gmt" => "2015-08-13T15:53:07",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-47.jpg",
        "name" => "image2xxl (4)",
        "alt" => "",
        "position" => 2
      ]
    ],
    "attributes" => [],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4440,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4445,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4470,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 4471,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34539,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34544,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34569,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 34570,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39810,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39815,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39840,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 39841,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 173355,
        "key" => "_wpml_media_duplicate",
        "value" => "1"
      ],
      [
        "id" => 173356,
        "key" => "_wpml_media_featured",
        "value" => "1"
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/436"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ],
  [
    "id" => 444,
    "name" => "Longline T-Shirt With Aztec Yoke Print In Relaxed Skater Fit",
    "slug" => "longline-t-shirt-with-aztec-yoke-print-in-relaxed-skater-fit",
    "permalink" => "https://mstore.io/product/longline-t-shirt-with-aztec-yoke-print-in-relaxed-skater-fit/",
    "date_created" => "2015-08-13T15:55:40",
    "date_created_gmt" => "2015-08-13T08:55:40",
    "date_modified" => "2019-04-17T10:03:08",
    "date_modified_gmt" => "2019-04-17T03:03:08",
    "type" => "simple",
    "status" => "publish",
    "featured" => false,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Cotton jersey</li>\n<li>Crew neck</li>\n<li>Yoke print</li>\n<li>Relaxed fit</li>\n<li>Longline cut</li>\n<li>Cut longer than standard length</li>\n<li>Machine wash</li>\n<li>100% Cotton</li>\n</ul>\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Donec elementum ligula eu sapien consequat eleifend. Donec nec dolor erat, condimentum sagittis sem. Praesent porttitor porttitor risus, dapibus rutrum ipsum gravida et. Integer lectus nisi, facilisis sit amet eleifend nec, pharetra ut augue. Integer quam nunc, consequat nec egestas ac, volutpat ac nisi. Sed consectetur dignissim dignissim. Donec pretium est sit amet ipsum fringilla feugiat. Aliquam erat volutpat. Maecenas scelerisque, orci sit amet cursus tincidunt, libero nisl eleifend tortor, vitae cursus risus mauris vitae nisi. Cras laoreet ultrices ligula eget tempus. Aenean metus</p>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "19",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"444\"><del><span class=\"woocommerce-Price-amount amount\">24.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></del> <ins><span class=\"woocommerce-Price-amount amount\">19.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></ins></span>",
    "on_sale" => true,
    "purchasable" => true,
    "total_sales" => 88,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => false,
    "stock_quantity" => null,
    "in_stock" => true,
    "backorders" => "no",
    "backorders_allowed" => false,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "0.00",
    "rating_count" => 0,
    "related_ids" => [],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [],
    "tags" => [],
    "images" => [
      [
        "id" => 14899,
        "date_created" => "2019-04-17T17:02:09",
        "date_created_gmt" => "2019-04-17T03:02:09",
        "date_modified" => "2019-04-17T17:02:45",
        "date_modified_gmt" => "2019-04-17T03:02:45",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-651.jpg",
        "name" => "image4xxl-651",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 447,
        "date_created" => "2015-08-13T22:55:27",
        "date_created_gmt" => "2015-08-13T15:55:27",
        "date_modified" => "2015-08-13T22:55:27",
        "date_modified_gmt" => "2015-08-13T15:55:27",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-65.jpg",
        "name" => "image4xxl (6)",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 446,
        "date_created" => "2015-08-13T22:55:24",
        "date_created_gmt" => "2015-08-13T15:55:24",
        "date_modified" => "2015-08-13T22:55:24",
        "date_modified_gmt" => "2015-08-13T15:55:24",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-67.jpg",
        "name" => "image2xxl (6)",
        "alt" => "",
        "position" => 2
      ]
    ],
    "attributes" => [
      [
        "id" => 2,
        "name" => "color",
        "position" => 0,
        "visible" => true,
        "variation" => false,
        "options" => [
          "Black",
          "blue",
          "Green",
          "Red"
        ]
      ],
      [
        "id" => 1,
        "name" => "size",
        "position" => 1,
        "visible" => true,
        "variation" => false,
        "options" => [
          "L",
          "M",
          "S"
        ]
      ],
      [
        "id" => 3,
        "name" => "Width",
        "position" => 2,
        "visible" => true,
        "variation" => false,
        "options" => [
          "large",
          "medium",
          "small"
        ]
      ]
    ],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4504,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4509,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4534,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 4535,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34609,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34614,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34639,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 34640,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39880,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39885,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39910,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 39911,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 204129,
        "key" => "onesignal_meta_box_present",
        "value" => "1"
      ],
      [
        "id" => 204130,
        "key" => "onesignal_send_notification",
        "value" => ""
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/444"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ],
  [
    "id" => 448,
    "name" => "T-Shirt with Varied Stripe",
    "slug" => "t-shirt-with-varied-stripe",
    "permalink" => "https://mstore.io/product/t-shirt-with-varied-stripe/",
    "date_created" => "2015-08-13T22:56:38",
    "date_created_gmt" => "2015-08-13T15:56:38",
    "date_modified" => "2017-06-11T21:43:46",
    "date_modified_gmt" => "2017-06-11T14:43:46",
    "type" => "simple",
    "status" => "publish",
    "featured" => false,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Soft-touch jersey</li>\n<li>Crew neck</li>\n<li>Chest pocket</li>\n<li>Embroidered logo</li>\n<li>Regular fit - true to size</li>\n<li>Machine wash</li>\n<li>60% Cotton, 40% Polyester</li>\n</ul>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"448\"><span class=\"woocommerce-Price-amount amount\">28.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
    "on_sale" => false,
    "purchasable" => true,
    "total_sales" => 65,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => false,
    "stock_quantity" => null,
    "in_stock" => true,
    "backorders" => "no",
    "backorders_allowed" => false,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "0.00",
    "rating_count" => 0,
    "related_ids" => [],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [],
    "tags" => [],
    "images" => [
      [
        "id" => 449,
        "date_created" => "2015-08-13T22:56:12",
        "date_created_gmt" => "2015-08-13T15:56:12",
        "date_modified" => "2015-08-13T22:56:12",
        "date_modified_gmt" => "2015-08-13T15:56:12",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-75.jpg",
        "name" => "image1xxl (7)",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 451,
        "date_created" => "2015-08-13T22:56:17",
        "date_created_gmt" => "2015-08-13T15:56:17",
        "date_modified" => "2015-08-13T22:56:17",
        "date_modified_gmt" => "2015-08-13T15:56:17",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-75.jpg",
        "name" => "image4xxl (7)",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 450,
        "date_created" => "2015-08-13T22:56:15",
        "date_created_gmt" => "2015-08-13T15:56:15",
        "date_modified" => "2015-08-13T22:56:15",
        "date_modified_gmt" => "2015-08-13T15:56:15",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-76.jpg",
        "name" => "image2xxl (7)",
        "alt" => "",
        "position" => 2
      ]
    ],
    "attributes" => [],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4536,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4541,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4566,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 4567,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34649,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34654,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34679,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 34680,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39920,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39925,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid" => [],
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39950,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 39951,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 173435,
        "key" => "_wpml_media_duplicate",
        "value" => "1"
      ],
      [
        "id" => 173436,
        "key" => "_wpml_media_featured",
        "value" => "1"
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/448"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ],
  [
    "id" => 440,
    "name" => "Hoodie in Jersey",
    "slug" => "hoodie-in-jersey",
    "permalink" => "https://mstore.io/product/hoodie-in-jersey/",
    "date_created" => "2015-08-13T22:54:08",
    "date_created_gmt" => "2015-08-13T15:54:08",
    "date_modified" => "2019-08-01T17:17:15",
    "date_modified_gmt" => "2019-08-01T10:17:15",
    "type" => "variable",
    "status" => "publish",
    "featured" => false,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Soft-touch jersey</li>\n<li>Drawstring hood</li>\n<li>Pouch pocket</li>\n<li>Fitted trims</li>\n<li>Regular fit - true to size</li>\n<li>Machine wash</li>\n<li>60% Cotton, 40% Polyester</li>\n</ul>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"440\"></span>",
    "on_sale" => false,
    "purchasable" => true,
    "total_sales" => 80,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => true,
    "stock_quantity" => 999999921,
    "in_stock" => false,
    "backorders" => "yes",
    "backorders_allowed" => true,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "0.00",
    "rating_count" => 0,
    "related_ids" => [
      242,
      250,
      262,
      287,
      283
    ],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [],
    "tags" => [],
    "images" => [
      [
        "id" => 441,
        "date_created" => "2015-08-13T22:53:56",
        "date_created_gmt" => "2015-08-13T15:53:56",
        "date_modified" => "2015-08-13T22:53:56",
        "date_modified_gmt" => "2015-08-13T15:53:56",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-56.jpg",
        "name" => "image1xxl (5)",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 443,
        "date_created" => "2015-08-13T22:54:01",
        "date_created_gmt" => "2015-08-13T15:54:01",
        "date_modified" => "2015-08-13T22:54:01",
        "date_modified_gmt" => "2015-08-13T15:54:01",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-55.jpg",
        "name" => "image4xxl (5)",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 442,
        "date_created" => "2015-08-13T22:53:58",
        "date_created_gmt" => "2015-08-13T15:53:58",
        "date_modified" => "2015-08-13T22:53:58",
        "date_modified_gmt" => "2015-08-13T15:53:58",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-57.jpg",
        "name" => "image2xxl (5)",
        "alt" => "",
        "position" => 2
      ]
    ],
    "attributes" => [
      [
        "id" => 2,
        "name" => "color",
        "position" => 0,
        "visible" => true,
        "variation" => false,
        "options" => [
          "blue",
          "Green",
          "Red",
          "Yellow"
        ]
      ],
      [
        "id" => 1,
        "name" => "size",
        "position" => 1,
        "visible" => true,
        "variation" => false,
        "options" => [
          "L",
          "M",
          "S",
          "XL"
        ]
      ]
    ],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4472,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4477,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4502,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 4503,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34574,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34579,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34604,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 34605,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39845,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39850,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39875,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 39876,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 51287,
        "key" => "onesignal_meta_box_present",
        "value" => "1"
      ],
      [
        "id" => 51288,
        "key" => "onesignal_send_notification",
        "value" => ""
      ],
      [
        "id" => 130650,
        "key" => "algolia_searchable_posts_records_count",
        "value" => "1"
      ],
      [
        "id" => 173522,
        "key" => "_wpml_media_duplicate",
        "value" => "1"
      ],
      [
        "id" => 173523,
        "key" => "_wpml_media_featured",
        "value" => "1"
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/440"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ],
  [
    "id" => 452,
    "name" => "Muscle Fit T-Shirt With Block Stripe In Stretch",
    "slug" => "muscle-fit-t-shirt-with-block-stripe-in-stretch",
    "permalink" => "https://mstore.io/product/muscle-fit-t-shirt-with-block-stripe-in-stretch/",
    "date_created" => "2015-08-13T22:57:17",
    "date_created_gmt" => "2015-08-13T15:57:17",
    "date_modified" => "2019-07-09T18:33:13",
    "date_modified_gmt" => "2019-07-09T11:33:13",
    "type" => "simple",
    "status" => "publish",
    "featured" => false,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Stretch jersey</li>\n<li>Slim cut sleeves</li>\n<li>Tight fit to the body</li>\n<li>Skinny fit – cut closely to the body</li>\n<li>Machine wash</li>\n<li>96% Cotton, 4% Elastane</li>\n</ul>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"452\"></span>",
    "on_sale" => false,
    "purchasable" => false,
    "total_sales" => 100,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => true,
    "stock_quantity" => 9999911,
    "in_stock" => false,
    "backorders" => "yes",
    "backorders_allowed" => true,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "0.00",
    "rating_count" => 0,
    "related_ids" => [],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [],
    "tags" => [],
    "images" => [
      [
        "id" => 453,
        "date_created" => "2015-08-13T22:57:02",
        "date_created_gmt" => "2015-08-13T15:57:02",
        "date_modified" => "2015-08-13T22:57:02",
        "date_modified_gmt" => "2015-08-13T15:57:02",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-85.jpg",
        "name" => "image1xxl (8)",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 455,
        "date_created" => "2015-08-13T22:57:07",
        "date_created_gmt" => "2015-08-13T15:57:07",
        "date_modified" => "2015-08-13T22:57:07",
        "date_modified_gmt" => "2015-08-13T15:57:07",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-84.jpg",
        "name" => "image4xxl (8)",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 454,
        "date_created" => "2015-08-13T22:57:05",
        "date_created_gmt" => "2015-08-13T15:57:05",
        "date_modified" => "2015-08-13T22:57:05",
        "date_modified_gmt" => "2015-08-13T15:57:05",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-85.jpg",
        "name" => "image2xxl (8)",
        "alt" => "",
        "position" => 2
      ]
    ],
    "attributes" => [
      [
        "id" => 2,
        "name" => "color",
        "position" => 0,
        "visible" => true,
        "variation" => false,
        "options" => [
          "Black",
          "blue",
          "Green",
          "Red",
          "Yellow"
        ]
      ],
      [
        "id" => 1,
        "name" => "size",
        "position" => 1,
        "visible" => true,
        "variation" => false,
        "options" => [
          "L",
          "M",
          "S",
          "XL"
        ]
      ]
    ],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4568,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4573,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4598,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 4599,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34684,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34689,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34714,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 34715,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39955,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39960,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39985,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 39986,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 51280,
        "key" => "onesignal_meta_box_present",
        "value" => "1"
      ],
      [
        "id" => 51281,
        "key" => "onesignal_send_notification",
        "value" => ""
      ],
      [
        "id" => 130649,
        "key" => "algolia_searchable_posts_records_count",
        "value" => "1"
      ],
      [
        "id" => 173606,
        "key" => "_wpml_media_duplicate",
        "value" => "1"
      ],
      [
        "id" => 173607,
        "key" => "_wpml_media_featured",
        "value" => "1"
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/452"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ],
  [
    "id" => 456,
    "name" => "T-Shirt with Next Stop Nowhere Print",
    "slug" => "t-shirt-with-next-stop-nowhere-print",
    "permalink" => "https://mstore.io/product/t-shirt-with-next-stop-nowhere-print/",
    "date_created" => "2015-08-13T22:58:54",
    "date_created_gmt" => "2015-08-13T15:58:54",
    "date_modified" => "2019-08-19T14:19:32",
    "date_modified_gmt" => "2019-08-19T07:19:32",
    "type" => "simple",
    "status" => "publish",
    "featured" => false,
    "catalog_visibility" => "visible",
    "description" => "<ul>\n<li>Soft-touch jersey</li>\n<li>Crew neck</li>\n<li>Chest print</li>\n<li>Regular fit - true to size</li>\n<li>Machine wash</li>\n<li>60% Cotton, 40% Polyester</li>\n</ul>\n",
    "short_description" => "",
    "sku" => "",
    "price" => "",
    "regular_price" => "",
    "sale_price" => "",
    "date_on_sale_from" => null,
    "date_on_sale_from_gmt" => null,
    "date_on_sale_to" => null,
    "date_on_sale_to_gmt" => null,
    "price_html" => "<span class=\"woocs_price_code\" data-product-id=\"456\"><span class=\"woocommerce-Price-amount amount\">27.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
    "on_sale" => false,
    "purchasable" => true,
    "total_sales" => 333,
    "virtual" => false,
    "downloadable" => false,
    "downloads" => [],
    "download_limit" => -1,
    "download_expiry" => -1,
    "external_url" => "",
    "button_text" => "",
    "tax_status" => "taxable",
    "tax_class" => "",
    "manage_stock" => true,
    "stock_quantity" => 99999684,
    "in_stock" => true,
    "backorders" => "yes",
    "backorders_allowed" => true,
    "backordered" => false,
    "sold_individually" => false,
    "weight" => "",
    "dimensions" => [
      "length" => "",
      "width" => "",
      "height" => ""
    ],
    "shipping_required" => true,
    "shipping_taxable" => true,
    "shipping_class" => "",
    "shipping_class_id" => 0,
    "reviews_allowed" => true,
    "average_rating" => "0.00",
    "rating_count" => 0,
    "related_ids" => [],
    "upsell_ids" => [],
    "cross_sell_ids" => [],
    "parent_id" => 0,
    "purchase_note" => "",
    "categories" => [],
    "tags" => [],
    "images" => [
      [
        "id" => 457,
        "date_created" => "2015-08-13T22:57:57",
        "date_created_gmt" => "2015-08-13T15:57:57",
        "date_modified" => "2015-08-13T22:57:57",
        "date_modified_gmt" => "2015-08-13T15:57:57",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-95.jpg",
        "name" => "image1xxl (9)",
        "alt" => "",
        "position" => 0
      ],
      [
        "id" => 459,
        "date_created" => "2015-08-13T22:58:02",
        "date_created_gmt" => "2015-08-13T15:58:02",
        "date_modified" => "2015-08-13T22:58:02",
        "date_modified_gmt" => "2015-08-13T15:58:02",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-94.jpg",
        "name" => "image4xxl (9)",
        "alt" => "",
        "position" => 1
      ],
      [
        "id" => 458,
        "date_created" => "2015-08-13T22:57:59",
        "date_created_gmt" => "2015-08-13T15:57:59",
        "date_modified" => "2015-08-13T22:57:59",
        "date_modified_gmt" => "2015-08-13T15:57:59",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-95.jpg",
        "name" => "image2xxl (9)",
        "alt" => "",
        "position" => 2
      ],
      [
        "id" => 14899,
        "date_created" => "2019-04-17T17:02:09",
        "date_created_gmt" => "2019-04-17T03:02:09",
        "date_modified" => "2019-04-17T17:02:45",
        "date_modified_gmt" => "2019-04-17T03:02:45",
        "src" => "https://mstore.io/wp-content/uploads/2015/08/image4xxl-651.jpg",
        "name" => "image4xxl-651",
        "alt" => "",
        "position" => 3
      ],
      [
        "id" => 9265,
        "date_created" => "2016-10-07T10:21:10",
        "date_created_gmt" => "2016-10-07T03:21:10",
        "date_modified" => "2016-10-07T10:21:10",
        "date_modified_gmt" => "2016-10-07T03:21:10",
        "src" => "https://mstore.io/wp-content/uploads/2016/10/man.jpg",
        "name" => "man",
        "alt" => "",
        "position" => 4
      ]
    ],
    "attributes" => [
      [
        "id" => 1,
        "name" => "size",
        "position" => 0,
        "visible" => true,
        "variation" => false,
        "options" => [
          "L",
          "M",
          "S",
          "XL"
        ]
      ],
      [
        "id" => 2,
        "name" => "color",
        "position" => 1,
        "visible" => true,
        "variation" => false,
        "options" => [
          "Black",
          "blue",
          "Green",
          "Red",
          "Yellow"
        ]
      ]
    ],
    "default_attributes" => [],
    "variations" => [],
    "grouped_products" => [],
    "menu_order" => 0,
    "meta_data" => [
      [
        "id" => 4600,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4605,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 4630,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 4631,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 34719,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34724,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 34749,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 34750,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 39990,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 39995,
        "key" => "_vc_post_settings",
        "value" => [
          "vc_grid_id" => []
        ]
      ],
      [
        "id" => 40020,
        "key" => "slide_template",
        "value" => "default"
      ],
      [
        "id" => 40021,
        "key" => "sbg_selected_sidebar_replacement",
        "value" => "0"
      ],
      [
        "id" => 51273,
        "key" => "onesignal_meta_box_present",
        "value" => "1"
      ],
      [
        "id" => 51274,
        "key" => "onesignal_send_notification",
        "value" => ""
      ],
      [
        "id" => 130648,
        "key" => "algolia_searchable_posts_records_count",
        "value" => "1"
      ],
      [
        "id" => 173693,
        "key" => "_wpml_media_duplicate",
        "value" => "1"
      ],
      [
        "id" => 173694,
        "key" => "_wpml_media_featured",
        "value" => "1"
      ]
    ],
    "better_featured_image" => null,
    "brands" => [],
    "_links" => [
      "self" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products/456"
        ]
      ],
      "collection" => [
        [
          "href" => "https://mstore.io/wp-json/wc/v2/products"
        ]
      ]
    ]
  ]
];