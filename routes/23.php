<?php

return [
	[
			"id" => 202,
			"name" => "Shadow Floral Print Shopper Bag",
			"slug" => "shadow-floral-print-shopper-bag",
			"permalink" => "https://mstore.io/product/shadow-floral-print-shopper-bag/",
			"date_created" => "2015-08-12T01:41:14",
			"date_created_gmt" => "2015-08-11T18:41:14",
			"date_modified" => "2015-08-12T01:41:14",
			"date_modified_gmt" => "2015-08-11T18:41:14",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Printed glossy outer</li>\n<li>Twin grab handles</li>\n<li>Signature logo</li>\n<li>Lined main compartment</li>\n<li>Inner zip pouch</li>\n<li>Do not wash</li>\n<li>100% Polyester</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"202\"><span class=\"woocommerce-Price-amount amount\">105.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 33,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "4.50",
			"rating_count" => 2,
			"related_ids" => [
					85,
					154,
					178,
					190,
					174
			],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					],
					[
							"id" => 22,
							"name" => "Women",
							"slug" => "women"
					]
			],
			"tags" => [],
			"images" => [
					[
							"id" => 203,
							"date_created" => "2015-08-12T01:40:43",
							"date_created_gmt" => "2015-08-11T18:40:43",
							"date_modified" => "2015-08-12T01:40:43",
							"date_modified_gmt" => "2015-08-11T18:40:43",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-81.jpg",
							"name" => "image1xxl (8)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 205,
							"date_created" => "2015-08-12T01:40:48",
							"date_created_gmt" => "2015-08-11T18:40:48",
							"date_modified" => "2015-08-12T01:40:48",
							"date_modified_gmt" => "2015-08-11T18:40:48",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image3xxl1.jpg",
							"name" => "image3xxl",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 204,
							"date_created" => "2015-08-12T01:40:46",
							"date_created_gmt" => "2015-08-11T18:40:46",
							"date_modified" => "2015-08-12T01:40:46",
							"date_modified_gmt" => "2015-08-11T18:40:46",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl2.jpg",
							"name" => "image2xxl",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 2699,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2704,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2729,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 2730,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 32624,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32629,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32654,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 32655,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 37896,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 37901,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 37926,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 37927,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/202"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 210,
			"name" => "Stripe Backpack with Contrast Tan Detail",
			"slug" => "stripe-backpack-with-contrast-tan-detail",
			"permalink" => "https://mstore.io/product/stripe-backpack-with-contrast-tan-detail/",
			"date_created" => "2015-08-12T01:50:58",
			"date_created_gmt" => "2015-08-11T18:50:58",
			"date_modified" => "2015-08-12T01:50:58",
			"date_modified_gmt" => "2015-08-11T18:50:58",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Striped canvas</li>\n<li>Contrast leather-look trims</li>\n<li>Grab handle</li>\n<li>Twin flap front pockets</li>\n<li>Flap top</li>\n<li>Zipped inner pocket</li>\n<li>Shoulder straps</li>\n<li>Do not wash</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"210\"><span class=\"woocommerce-Price-amount amount\">54.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 29,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 211,
							"date_created" => "2015-08-12T01:50:39",
							"date_created_gmt" => "2015-08-11T18:50:39",
							"date_modified" => "2015-08-12T01:50:39",
							"date_modified_gmt" => "2015-08-11T18:50:39",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-12.jpg",
							"name" => "image1xxl (1)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 213,
							"date_created" => "2015-08-12T01:50:44",
							"date_created_gmt" => "2015-08-11T18:50:44",
							"date_modified" => "2015-08-12T01:50:44",
							"date_modified_gmt" => "2015-08-11T18:50:44",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image3xxl-21.jpg",
							"name" => "image3xxl (2)",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 212,
							"date_created" => "2015-08-12T01:50:41",
							"date_created_gmt" => "2015-08-11T18:50:41",
							"date_modified" => "2015-08-12T01:50:41",
							"date_modified_gmt" => "2015-08-11T18:50:41",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-22.jpg",
							"name" => "image2xxl (2)",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 2763,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2768,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2793,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 2794,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 32699,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32704,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32729,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 32730,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 37971,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 37976,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38001,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 38002,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/210"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 214,
			"name" => "Ware Backpack in Burgundy",
			"slug" => "ware-backpack-in-burgundy",
			"permalink" => "https://mstore.io/product/ware-backpack-in-burgundy/",
			"date_created" => "2015-08-12T01:52:02",
			"date_created_gmt" => "2015-08-11T18:52:02",
			"date_modified" => "2015-08-12T01:52:02",
			"date_modified_gmt" => "2015-08-11T18:52:02",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Durable fabric</li>\n<li>Grab handle</li>\n<li>Flap top, drawstring fastening</li>\n<li>Zipped inner pocket</li>\n<li>Adjustable shoulder straps</li>\n<li>Wipe with a damp cloth</li>\n<li>100% Polyester</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "55",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"214\"><del><span class=\"woocommerce-Price-amount amount\">60.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></del> <ins><span class=\"woocommerce-Price-amount amount\">55.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></ins></span>",
			"on_sale" => true,
			"purchasable" => true,
			"total_sales" => 20,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 215,
							"date_created" => "2015-08-12T01:51:36",
							"date_created_gmt" => "2015-08-11T18:51:36",
							"date_modified" => "2015-08-12T01:51:36",
							"date_modified_gmt" => "2015-08-11T18:51:36",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-22.jpg",
							"name" => "image1xxl (2)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 216,
							"date_created" => "2015-08-12T01:51:39",
							"date_created_gmt" => "2015-08-11T18:51:39",
							"date_modified" => "2015-08-12T01:51:39",
							"date_modified_gmt" => "2015-08-11T18:51:39",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-32.jpg",
							"name" => "image2xxl (3)",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 217,
							"date_created" => "2015-08-12T01:51:41",
							"date_created_gmt" => "2015-08-11T18:51:41",
							"date_modified" => "2015-08-12T01:51:41",
							"date_modified_gmt" => "2015-08-11T18:51:41",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image3xxl-31.jpg",
							"name" => "image3xxl (3)",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 2795,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2800,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2825,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 2826,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 32734,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32739,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32764,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 32765,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 38006,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38011,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38036,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 38037,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/214"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 218,
			"name" => "Fringed Shoulder Bag",
			"slug" => "fringed-shoulder-bag",
			"permalink" => "https://mstore.io/product/fringed-shoulder-bag/",
			"date_created" => "2015-08-11T18:53:11",
			"date_created_gmt" => "2015-08-11T11:53:11",
			"date_modified" => "2017-06-16T16:45:30",
			"date_modified_gmt" => "2017-06-16T09:45:30",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Leather-look fabric</li>\n<li>Grab handle</li>\n<li>Removable shoulder strap</li>\n<li>Fringed detail</li>\n<li>Zip top and inner pocket</li>\n<li>Do not wash</li>\n<li>100% Polyurethane</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"218\"><span class=\"woocommerce-Price-amount amount\">22.99<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 29,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 219,
							"date_created" => "2015-08-12T01:52:45",
							"date_created_gmt" => "2015-08-11T18:52:45",
							"date_modified" => "2015-08-12T01:52:45",
							"date_modified_gmt" => "2015-08-11T18:52:45",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-32.jpg",
							"name" => "image1xxl (3)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 220,
							"date_created" => "2015-08-12T01:52:47",
							"date_created_gmt" => "2015-08-11T18:52:47",
							"date_modified" => "2015-08-12T01:52:47",
							"date_modified_gmt" => "2015-08-11T18:52:47",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-42.jpg",
							"name" => "image2xxl (4)",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 221,
							"date_created" => "2015-08-12T01:52:50",
							"date_created_gmt" => "2015-08-11T18:52:50",
							"date_modified" => "2015-08-12T01:52:50",
							"date_modified_gmt" => "2015-08-11T18:52:50",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image3xxl-41.jpg",
							"name" => "image3xxl (4)",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 2827,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2832,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2857,
							"key" => "slide_template",
							"value" => ""
					],
					[
							"id" => 2858,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 32769,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32774,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32799,
							"key" => "slide_template",
							"value" => ""
					],
					[
							"id" => 32800,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 38041,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38046,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38071,
							"key" => "slide_template",
							"value" => ""
					],
					[
							"id" => 38072,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 173857,
							"key" => "_wpml_media_duplicate",
							"value" => "1"
					],
					[
							"id" => 173858,
							"key" => "_wpml_media_featured",
							"value" => "1"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/218"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 222,
			"name" => "Winged Handheld Bag With Chain Zip",
			"slug" => "winged-handheld-bag-with-chain-zip",
			"permalink" => "https://mstore.io/product/winged-handheld-bag-with-chain-zip/",
			"date_created" => "2015-08-12T01:54:19",
			"date_created_gmt" => "2015-08-11T18:54:19",
			"date_modified" => "2015-08-12T01:54:19",
			"date_modified_gmt" => "2015-08-11T18:54:19",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Smooth leather-look outer</li>\n<li>Snakeskin panel detailing</li>\n<li>Gold-tone hardware</li>\n<li>Twin grab handles</li>\n<li>Zip top closure</li>\n<li>Internal zip pocket</li>\n<li>Exterior zip pocket with chain detailing</li>\n<li>Detachable shoulder strap</li>\n<li>Wipe with a damp cloth</li>\n<li>100% Polyurethane</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"222\"><span class=\"woocommerce-Price-amount amount\">48.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 27,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [
					234,
					166
			],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [
					[
							"id" => 8,
							"name" => "black",
							"slug" => "black"
					]
			],
			"images" => [
					[
							"id" => 223,
							"date_created" => "2015-08-12T01:53:59",
							"date_created_gmt" => "2015-08-11T18:53:59",
							"date_modified" => "2015-08-12T01:53:59",
							"date_modified_gmt" => "2015-08-11T18:53:59",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-42.jpg",
							"name" => "image1xxl (4)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 224,
							"date_created" => "2015-08-12T01:54:01",
							"date_created_gmt" => "2015-08-11T18:54:01",
							"date_modified" => "2015-08-12T01:54:01",
							"date_modified_gmt" => "2015-08-11T18:54:01",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-52.jpg",
							"name" => "image2xxl (5)",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 225,
							"date_created" => "2015-08-12T01:54:04",
							"date_created_gmt" => "2015-08-11T18:54:04",
							"date_modified" => "2015-08-12T01:54:04",
							"date_modified_gmt" => "2015-08-11T18:54:04",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image3xxl-52.jpg",
							"name" => "image3xxl (5)",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 2859,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2864,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2889,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 2890,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 32804,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32809,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32834,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 32835,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 38076,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38081,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38106,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 38107,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/222"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 226,
			"name" => "V Bar Mini Cross Body Bag",
			"slug" => "v-bar-mini-cross-body-bag",
			"permalink" => "https://mstore.io/product/v-bar-mini-cross-body-bag/",
			"date_created" => "2015-08-12T01:55:26",
			"date_created_gmt" => "2015-08-11T18:55:26",
			"date_modified" => "2015-08-12T01:55:26",
			"date_modified_gmt" => "2015-08-11T18:55:26",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Textured leather-look fabric</li>\n<li>Front flap press stud closure</li>\n<li>Gold-tone hardware</li>\n<li>Adjustable shoulder strap</li>\n<li>Wipe with a damp sponge</li>\n<li>100% Polyurethane</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"226\"><span class=\"woocommerce-Price-amount amount\">26.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 31,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [
					275,
					178
			],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [
					[
							"id" => 9,
							"name" => "gray",
							"slug" => "gray"
					]
			],
			"images" => [
					[
							"id" => 227,
							"date_created" => "2015-08-12T01:55:11",
							"date_created_gmt" => "2015-08-11T18:55:11",
							"date_modified" => "2015-08-12T01:55:11",
							"date_modified_gmt" => "2015-08-11T18:55:11",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-52.jpg",
							"name" => "image1xxl (5)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 229,
							"date_created" => "2015-08-12T01:55:16",
							"date_created_gmt" => "2015-08-11T18:55:16",
							"date_modified" => "2015-08-12T01:55:16",
							"date_modified_gmt" => "2015-08-11T18:55:16",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image3xxl-62.jpg",
							"name" => "image3xxl (6)",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 228,
							"date_created" => "2015-08-12T01:55:13",
							"date_created_gmt" => "2015-08-11T18:55:13",
							"date_modified" => "2015-08-12T01:55:13",
							"date_modified_gmt" => "2015-08-11T18:55:13",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-63.jpg",
							"name" => "image2xxl (6)",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 2891,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2896,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2921,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 2922,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 32839,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32844,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32869,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 32870,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 38111,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38116,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38141,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 38142,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 173773,
							"key" => "_wpml_media_duplicate",
							"value" => "1"
					],
					[
							"id" => 173774,
							"key" => "_wpml_media_featured",
							"value" => "1"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/226"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 230,
			"name" => "Heritage Backpack in Feather Print",
			"slug" => "heritage-backpack-in-feather-print",
			"permalink" => "https://mstore.io/product/heritage-backpack-in-feather-print/",
			"date_created" => "2015-08-12T01:56:29",
			"date_created_gmt" => "2015-08-11T18:56:29",
			"date_modified" => "2015-08-12T01:56:29",
			"date_modified_gmt" => "2015-08-11T18:56:29",
			"type" => "simple",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Lightly textured durable fabric</li>\n<li>Grab handle</li>\n<li>Double zip around closure</li>\n<li>Outer pouch pocket</li>\n<li>Adjustable, padded straps</li>\n<li>Printed lining</li>\n<li>Do not wash</li>\n<li>100% Polyester</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"230\"><span class=\"woocommerce-Price-amount amount\">65.00<span class=\"woocommerce-Price-currencySymbol\">&#36;</span></span></span>",
			"on_sale" => false,
			"purchasable" => true,
			"total_sales" => 127,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "0.00",
			"rating_count" => 0,
			"related_ids" => [
					194,
					186,
					166,
					142,
					89
			],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 231,
							"date_created" => "2015-08-12T01:56:12",
							"date_created_gmt" => "2015-08-11T18:56:12",
							"date_modified" => "2015-08-12T01:56:12",
							"date_modified_gmt" => "2015-08-11T18:56:12",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image1xxl-63.jpg",
							"name" => "image1xxl (6)",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 233,
							"date_created" => "2015-08-12T01:56:17",
							"date_created_gmt" => "2015-08-11T18:56:17",
							"date_modified" => "2015-08-12T01:56:17",
							"date_modified_gmt" => "2015-08-11T18:56:17",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image3xxl-71.jpg",
							"name" => "image3xxl (7)",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 232,
							"date_created" => "2015-08-12T01:56:15",
							"date_created_gmt" => "2015-08-11T18:56:15",
							"date_modified" => "2015-08-12T01:56:15",
							"date_modified_gmt" => "2015-08-11T18:56:15",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-73.jpg",
							"name" => "image2xxl (7)",
							"alt" => "",
							"position" => 2
					]
			],
			"attributes" => [],
			"default_attributes" => [],
			"variations" => [],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 2923,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2928,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2953,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 2954,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 32874,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32879,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32904,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 32905,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 38146,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38151,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid" => [],
									"vc_grid_id" => []
							]
					],
					[
							"id" => 38176,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 38177,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					]
			],
			"better_featured_image" => null,
			"brands" => [],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/230"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	],
	[
			"id" => 206,
			"name" => "Tapestry Shopper",
			"slug" => "tapestry-shopper",
			"permalink" => "https://mstore.io/product/tapestry-shopper/",
			"date_created" => "2016-08-11T18:42:21",
			"date_created_gmt" => "2016-08-11T11:42:21",
			"date_modified" => "2019-08-17T17:50:34",
			"date_modified_gmt" => "2019-08-17T10:50:34",
			"type" => "variable",
			"status" => "publish",
			"featured" => false,
			"catalog_visibility" => "visible",
			"description" => "<ul>\n<li>Woven cotton-rich fabric</li>\n<li>Suede leather trim</li>\n<li>Twin shoulder straps</li>\n<li>Magnetic press stud fastening</li>\n<li>Zipped pocket to interior</li>\n<li>Do not wash</li>\n<li>63% Cotton, 20% Acrylic, 12% Viscose, 5% Real Leather</li>\n</ul>\n",
			"short_description" => "",
			"sku" => "",
			"price" => "",
			"regular_price" => "",
			"sale_price" => "",
			"date_on_sale_from" => null,
			"date_on_sale_from_gmt" => null,
			"date_on_sale_to" => null,
			"date_on_sale_to_gmt" => null,
			"price_html" => "<span class=\"woocs_price_code\" data-product-id=\"206\"></span>",
			"on_sale" => false,
			"purchasable" => false,
			"total_sales" => 463,
			"virtual" => false,
			"downloadable" => false,
			"downloads" => [],
			"download_limit" => -1,
			"download_expiry" => -1,
			"external_url" => "",
			"button_text" => "",
			"tax_status" => "taxable",
			"tax_class" => "",
			"manage_stock" => false,
			"stock_quantity" => null,
			"in_stock" => true,
			"backorders" => "no",
			"backorders_allowed" => false,
			"backordered" => false,
			"sold_individually" => false,
			"weight" => "",
			"dimensions" => [
					"length" => "",
					"width" => "",
					"height" => ""
			],
			"shipping_required" => true,
			"shipping_taxable" => true,
			"shipping_class" => "",
			"shipping_class_id" => 0,
			"reviews_allowed" => true,
			"average_rating" => "5.00",
			"rating_count" => 1,
			"related_ids" => [
					226,
					218,
					202,
					214,
					222
			],
			"upsell_ids" => [],
			"cross_sell_ids" => [],
			"parent_id" => 0,
			"purchase_note" => "",
			"categories" => [],
			"tags" => [],
			"images" => [
					[
							"id" => 16880,
							"date_created" => "2019-08-18T00:46:04",
							"date_created_gmt" => "2019-08-17T10:46:04",
							"date_modified" => "2019-08-18T00:46:04",
							"date_modified_gmt" => "2019-08-17T10:46:04",
							"src" => "https://mstore.io/wp-content/uploads/2016/08/example.jpeg",
							"name" => "example",
							"alt" => "",
							"position" => 0
					],
					[
							"id" => 209,
							"date_created" => "2015-08-12T01:42:00",
							"date_created_gmt" => "2015-08-11T18:42:00",
							"date_modified" => "2015-08-12T01:42:00",
							"date_modified_gmt" => "2015-08-11T18:42:00",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image3xxl-11.jpg",
							"name" => "image3xxl (1)",
							"alt" => "",
							"position" => 1
					],
					[
							"id" => 208,
							"date_created" => "2015-08-12T01:41:57",
							"date_created_gmt" => "2015-08-11T18:41:57",
							"date_modified" => "2015-08-12T01:41:57",
							"date_modified_gmt" => "2015-08-11T18:41:57",
							"src" => "https://mstore.io/wp-content/uploads/2015/08/image2xxl-12.jpg",
							"name" => "image2xxl (1)",
							"alt" => "",
							"position" => 2
					],
					[
							"id" => 16880,
							"date_created" => "2019-08-18T00:46:04",
							"date_created_gmt" => "2019-08-17T10:46:04",
							"date_modified" => "2019-08-18T00:46:04",
							"date_modified_gmt" => "2019-08-17T10:46:04",
							"src" => "https://mstore.io/wp-content/uploads/2016/08/example.jpeg",
							"name" => "example",
							"alt" => "",
							"position" => 3
					]
			],
			"attributes" => [
					[
							"id" => 2,
							"name" => "color",
							"position" => 0,
							"visible" => true,
							"variation" => false,
							"options" => [
									"Black",
									"blue",
									"Green",
									"Red",
									"Yellow"
							]
					],
					[
							"id" => 1,
							"name" => "size",
							"position" => 1,
							"visible" => true,
							"variation" => true,
							"options" => [
									"L",
									"M",
									"S",
									"XL"
							]
					]
			],
			"default_attributes" => [],
			"variations" => [
					14889
			],
			"grouped_products" => [],
			"menu_order" => 0,
			"meta_data" => [
					[
							"id" => 2731,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2736,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 2761,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 2762,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 32664,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32669,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 32694,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 32695,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 37936,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 37941,
							"key" => "_vc_post_settings",
							"value" => [
									"vc_grid_id" => []
							]
					],
					[
							"id" => 37966,
							"key" => "slide_template",
							"value" => "default"
					],
					[
							"id" => 37967,
							"key" => "sbg_selected_sidebar_replacement",
							"value" => "0"
					],
					[
							"id" => 104388,
							"key" => "onesignal_meta_box_present",
							"value" => "1"
					],
					[
							"id" => 104389,
							"key" => "onesignal_send_notification",
							"value" => ""
					],
					[
							"id" => 104390,
							"key" => "algolia_searchable_posts_records_count",
							"value" => "1"
					],
					[
							"id" => 172619,
							"key" => "_wpml_media_duplicate",
							"value" => "1"
					],
					[
							"id" => 172620,
							"key" => "_wpml_media_featured",
							"value" => "1"
					]
			],
			"better_featured_image" => null,
			"brands" => [
					[
							"id" => 310,
							"name" => "Adidas",
							"slug" => "adidas"
					]
			],
			"_links" => [
					"self" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products/206"
							]
					],
					"collection" => [
							[
									"href" => "https://mstore.io/wp-json/wc/v2/products"
							]
					]
			]
	]
];