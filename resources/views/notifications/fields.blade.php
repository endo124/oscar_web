<div class="col-md-6">
	<!-- Title Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('Title', 'Title:') !!}
		{!! Form::text('title', isset($notification)? $notification->title : null, ['class' => 'form-control']) !!}
    </div>
    
	<!-- Message Field -->
    <div class="form-group col-sm-12">
		{!! Form::label('Message', 'Message:') !!}
		{!! Form::text('message', isset($notification)? $notification->message : null, ['class' => 'form-control']) !!}
	</div>

	<!-- Image Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('Os', 'Os:') !!}
		{!!  Form::select('os',['android' => 'android', 'ios' => 'ios'],isset($notification)? $notification->os : null,['class' => 'form-control']) !!}
	</div>

	<div class="form-group col-sm-12">
		{!! Form::label('Language', 'Language:') !!}
		{!!  Form::select('language', ['ar' => 'ar', 'en' => 'en'],isset($notification)? $notification->language : null,['class' => 'form-control']) !!}
	</div>

	<!-- Image Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('image', 'Image:') !!}
		{!! Form::file('image', ['class' => 'form-control','accept' => 'image/png,image/jpeg']) !!}
	</div>

	<!-- <div class="form-group col-sm-12">
		{!! Form::label('published_at', 'Published_at:') !!}
		{!! Form::text('published_at',  isset($notification)? $notification->published_at : \Carbon\Carbon::now(),['class' => 'form-control', 'id' => 'datetimepicker1']) !!}
	</div> -->
	

	

	<!-- Submit Field -->
	<div class="form-group col-sm-12">
		{!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
		<a href="{!! route('notifications.index') !!}" class="btn btn-default">Cancel</a>
	</div>
</div>




@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<link rel="stylesheet"
  href="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/css/bootstrap-datetimepicker.min.css">
<script src="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/js/bootstrap-datetimepicker.min.js"></script>

<script>

  $(function() {
	$('#datetimepicker1').datetimepicker({
    defaultDate: new Date(),
    format: 'YYYY-MM-DD H:mm:ss',
    sideBySide: true
});
  });
  </script>
@stop