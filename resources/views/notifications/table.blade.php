<table class="table table-responsive" >
	<thead>
		<tr>
			<th>title</th>
			<th>message</th>
            <th>language</th>
            <th>os</th>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($notifications as $notification)
		<tr>
			<!-- <td>
				@if(isset($product['images']) && is_array($product['images']))
				<img src="{!! $product['images'][0]['src'] !!}" alt="" height="100">
				@endif
            </td> -->
            <td>{{$notification->title}}</td>
			<td>{{$notification->message}}</td>
            <td>{{$notification->language}}</td>
            <td>{{$notification->os}}</td>


			<td>
				{!! Form::open(['route' => ['notifications.destroy', $notification->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('notifications.show', [$notification->id]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('notifications.edit', [$notification->id]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-edit"></i></a>
					{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger
					btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>