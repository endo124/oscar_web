<div class="col-md-6">
	<!-- Id Field -->
	<div class="form-group">
		{!! Form::label('id', 'Id:') !!}
		<p>{!! $notification->id !!}</p>
	</div>

	<!-- Name Field -->
	<div class="form-group">
		{!! Form::label('Title', 'Title:') !!}
		<p>{!!  $notification->title !!}</p>
	</div>

	<!-- Description Field -->
	<div class="form-group">
		{!! Form::label('Message', 'Message:') !!}
		<p>{!! $notification->message  !!}</p>
	</div>

    <!-- Description Field -->
	<div class="form-group">
		{!! Form::label('Os', 'Os:') !!}
		<p>{!! $notification->os  !!}</p>
	</div>
    <div class="form-group">
		{!! Form::label('language', 'Language:') !!}
		<p>{!! $notification->language  !!}</p>
	</div>
</div>
<div class="col-md-6">

	<img src="{!! $notification->image !!}" class="img-responsive" />
</div>