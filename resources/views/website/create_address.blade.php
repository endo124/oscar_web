@extends('website.layouts.app')

@push('style')

   <link rel="stylesheet" href="{{ asset('/css/customer-profile.css') }}">
    <style>
        .form{

border: 1px solid #CB1104;
border-top: #09158C;
margin:  0 auto;
padding: 0px 0px 30px;
}

@media only screen and (min-width: 800px) {
.form{
width: 40%;
}
}
.modal-header{
  background: #09158C;
  color: #fff;
  border-radius: inherit;
}



    </style>
@endpush

@section('content')
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('message') }}</p>
@endif
    <div class="form my-5">
      <div class="modal-header">
        <h5 class="modal-title">{{ __('lang.add_address')}}</h5>
      </div>
        <div class="">
            @include('website.partials.errors')

            <form class="w-100 text-center" action="{{ url(app()->getLocale().'/address') }}" method="post">
                @csrf
                <div class="row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                      <label >{{ __('lang.phone')}} </label>
                  </div>
                  <div class="col-8" style="padding-left: 30px"> <input name="phone" type="text" class="form-control" value="{{ auth()->guard('customerForWeb')->user()->phone }}"></div> 
                </div> 
                <div class="row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                    <label >{{ __('lang.name')}}  </label>
                  </div>  
                    <div class="col-8" style="padding-left: 30px"><input name="name" type="text" class="form-control" value="{{ $address->name  ?? old('name')}}"></div> 
                </div>
                <div class="row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                    <label >{{ __('lang.address')}}  </label>
                  </div> 
                    <div class="col-8" style="padding-left: 30px"><input name="address" type="text" class="form-control" value="{{ $address->address  ?? old('address')}}"></div> 
                </div>
                <div class="row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                    <label >{{ __('lang.area')}}  </label>
                  </div> 
                    <div class="col-8" style="padding-left: 30px"><input name="area" type="text" class="form-control" value="{{ $address->area ?? old('area')}} "></div> 
                </div>
                <div class="row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                    <label >{{ __('lang.city')}}  </label>
                  </div>
                    <div class="col-8" style="padding-left: 30px"> <input name="city" type="text" class="form-control" value="{{ $address->city ?? old('city')}} "></div> 
                </div> 
                <input type="text" value=""  class="form-control "  name="coordinates" id="coordinates"   hidden>
                <div id="map"  style="width:100%;height:200px "></div>
                <button type="submit" class="btn mt-5 w-50"> {{ __('lang.create')}}</button>
        
                <!-- Grd row -->
            </form>
        </div>
    </div>
   
@endsection



@push('scripts')
<script>

let map, infoWindow;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 18,
  });
  infoWindow = new google.maps.InfoWindow();
  const locationButton = document.createElement("button");
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
        // //   infoWindow.setPosition(pos);
          infoWindow.open(map);
          map.setCenter(pos);
        var marker = new google.maps.Marker({
            position: pos,
            map: map,});
            var coordinates=(marker.getPosition().lng()).toString()+','+(marker.getPosition().lat()).toString();
            $('#coordinates').attr('value',coordinates);

            map.addListener("click", (mapsMouseEvent) => {
            // Close the current InfoWindow.
            marker.setMap(null);
            // Create a new InfoWindow.
            marker = new google.maps.Marker({
            position: mapsMouseEvent.latLng,
            map: map,});

            // console.log(marker.getPosition().lat())  ;
            // console.log(marker.getPosition().lng())  ;
            coordinates=(marker.getPosition().lng()).toString()+','+(marker.getPosition().lat()).toString();
            $('#coordinates').attr('value',coordinates);
            // $('#lat').attr('value',marker.getPosition().lat());

        });
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }


}

function initMap() {
        map = new google.maps.Map(document.getElementById("map"), {
          center: { lat: 30.0595581, lng: 31.223445 },
          zoom: 6,
        });
        infoWindow = new google.maps.InfoWindow();
        const pos = '30.0595581,31.223445';
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(
          pos
        );
        var marker = new google.maps.Marker({
            position: pos,
            map: map,});
            var coordinates='30.0595581,31.223445';
            $('#coordinates').attr('value',coordinates);

            map.addListener("click", (mapsMouseEvent) => {
            // Close the current InfoWindow.
            marker.setMap(null);
            // Create a new InfoWindow.
            marker = new google.maps.Marker({
            position: mapsMouseEvent.latLng,
            map: map,});

            // console.log(marker.getPosition().lat())  ;
            // console.log(marker.getPosition().lng())  ;
            coordinates=(marker.getPosition().lng()).toString()+','+(marker.getPosition().lat()).toString();
            $('#coordinates').attr('value',coordinates);
            // $('#lat').attr('value',marker.getPosition().lat());

        });
      }

     
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  initMap();
  infoWindow.open(map);
}

</script>

@endpush
