@extends('website.layouts.app')

@push('style')
	<style>
		
		.button5 a{
			color: #C5171C !important
		}
		.button5 a:hover {
			color: #fff !important
		}
		tbody .btn{
			margin: 0
		}
	</style>
@endpush

@section('content')

		
	<!-- Shopping Cart -->
	<div class="shopping-cart section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- Shopping Summery -->
					<table class="table shopping-summery">
						<thead>
							<tr class="main-hading">
								<th>{{ __('lang.product')}}</th>
								<th>{{ __('lang.name')}}</th>
								<th class="text-center">{{ __('lang.unite_price')}}</th>
								<th class="text-center">{{ __('lang.quantity')}}</th>
								<th class="text-center">{{ __('lang.total')}}</th> 
								<th class="text-center"><i class="ti-trash remove-icon"></i></th>
							</tr>
						</thead>
						<tbody>
							
							
							
						</tbody>
					</table>
					<!--/ End Shopping Summery -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<!-- Total Amount -->
					<div class="total-amount">
						<div class="row">
							
							<div class="offset-lg-8 offset-md-5 col-lg-4 col-md-7 col-12">
								<div class="right">
									<ul>
										<li>{{ __('lang.cart_sub_total')}}<span id="total_price"> </span></li>
										
									</ul>
									<div class="button5" >

										@if (Auth::guard('customerForWeb')->user())
											<a id="setprice" href="{{ url(app()->getLocale().'/checkout') }}" class="btn">
											{{ __('lang.check_out')}}
											</a>
										@else
										<a href="#" class="btn">
											You should Login first
										</a>
										@endif
										

									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/ End Total Amount -->
				</div>
			</div>
		</div>
	</div>
	<!--/ End Shopping Cart -->

@endsection

@push('scripts')

<script type="text/javascript">
var total_price=0;
var storage=JSON.parse(localStorage.getItem("cart"));
var lang=<?php echo json_encode(app()->getLocale()); ?>;

for (var i = 0; i < storage.length; i++){

	var pro=storage[i];
	var key=Object.keys(pro)[0];
	var cartProducts = [];
	
	var id=pro[key].id;
	cartProducts.push(id);
	var name=pro[key].name;
if(lang=='ar')
{
	name=pro[key].name_ar;
}
	
	if(pro[key].images[0]['src']){
		var src=pro[key].images[0]['src'];
	}else{
		var src ='';
	}
	var price=pro[key].regular_price;
	var unit=pro[key].PriceUnit;
	var qty=pro[key].qty;
	if(!qty){
		qty=1;
	}
	total_price= Number(price*Number(qty)) + Number(total_price);
 	var table_row='<tr id="'+id+'">' +
					'<td class="image" data-title="{{ __('lang.image')}}"><img src="'+src+'" alt="#"></td>' +
					'<td class="product-des" data-title="{{ __('lang.name')}}">' +
						'<p class="product-name"><a href="#">'+name+'</a></p>' +
					'</td>'+
					'<td class="price" data-title="{{ __('lang.unite_price')}}">EGP <span class="regular_price">'+price+'</span></td>'+
					'<td class="qty" data-title="{{ __('lang.quantity')}}"><!-- Input Order -->'+
						'<div class="input-group">'+
											'<button type="button" class="remove btn  btn-number"  data-type="minus" data-field="quant[2]" style="padding:5px">'+
												'<i class="ti-minus"></i>'+
											'</button>'+
											'<input type="text" unit="'+unit+'" class="qty" value="'+qty+'" style="width: 20%;border: 0;text-align: center;" readonly>'+
											'<button type="button"  class="add btn  btn-number" data-type="plus" data-field="quant[2]" style="padding:5px">'+
												'<i class="ti-plus"></i>'+
											'</button>'+
						'</div>'+
						'<!--/ End Input Order -->'+
					'</td>'+
					'<td class="product_total_pice" data-title="{{ __('lang.cart_sub_total')}}">'+Number(price*Number(qty)).toFixed(2)+'</td>'+
					'<td class="action" data-title="{{ __('lang.remove')}}"><a href="#"><i class="ti-trash remove-icon"></i></a></td>'+
				'</tr>';

				
	
		$('tbody').append(table_row);	

								

}
	$("#total_price").html(total_price.toFixed(2) +" EGP");
	$(".add").click(function(){
		var qty=$(this).prev("input").val();

		var proUnit=$(this).prev("input").attr('unit');
		if(proUnit =='kg'){
			qty=(Number(qty)+0.1).toFixed(1);
		}else{
			parseInt(qty);
			qty++;
		}
		
		 $(this).prev("input").val(qty);

		$('.remove').prop("disabled", false);
		var product_id=$(this).closest('tr').attr('id');
		var localStorage_key='product_'+product_id;
		var regular_price=$(this).closest('tr').find('.regular_price').text();
		var product_total_pice =$(this).closest('tr').find('.product_total_pice');
		product_total_pice=regular_price * qty;
		$(this).closest('tr').find('.product_total_pice').html(product_total_pice.toFixed(2));
		total_price =+total_price+ +regular_price;
		$("#total_price").html(total_price.toFixed(2));
		// Get the existing data
		var existing =JSON.parse(localStorage.getItem(localStorage_key));

		// Add new data to localStorage Array
		existing['qty'] = qty;
		// Save back to localStorage
		localStorage.setItem(localStorage_key, JSON.stringify(existing));	

	});
	$(".remove").click(function(){
		var product_id=$(this).closest('tr').attr('id');
		var localStorage_key='product_'+product_id;
		var qty=$(this).next("input").val();

		var proUnit=$(this).next("input").attr('unit');
		var price=$(this).closest('tr').find('.product_total_pice').html();
		var unit_price=$(this).closest('tr').find('.regular_price').text();


		if(proUnit =='kg'){
			if(qty >=0.1){
				qty=(Number(qty)-0.1).toFixed(1);
				$(this).next("input").val(qty);
				$(this).closest('tr').find('.product_total_pice').html((Number(qty) * Number(unit_price)).toFixed(2));
				total_price =total_price - unit_price;
				$("#total_price").html(total_price.toFixed(2));
				
			}else{
				$(this).prop("disabled", true);
			}
		}
		else if (proUnit !='kg' && qty >= 1) {

			qty--;
			$(this).next("input").val(qty);
			
			$(this).closest('tr').find('.product_total_pice').html((price - unit_price).toFixed(2));
			total_price =total_price - unit_price;
			$("#total_price").html(total_price.toFixed(2) +" EGP");
			// Get the existing data
			var existing =JSON.parse(localStorage.getItem(localStorage_key));
			// console.log(existing);

			// Add new data to localStorage Array
			existing['qty'] = qty;
			// Save back to localStorage
			localStorage.setItem(localStorage_key, JSON.stringify(existing));	

		}
		else {
			$(this).prop("disabled", true);
		}				
	});

	$('.action').click(function (e) {
		e.preventDefault();

		x=$(this).closest('tr').find('.product_total_pice').html();
		total_price =total_price - x;
		$("#total_price").html( total_price.toFixed(2) +" EGP");
		$(this).closest('tr').remove();
		var product_id=$(this).closest('tr').attr('id');
		
		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro)[0];
			if(key == 'product_'+product_id){
				storage.splice(i, 1); 
				// delete storage[key]
			}
		}

		var cart =[];
		localStorage.setItem('cart', JSON.stringify(cart));
		localStorage.setItem('cart', JSON.stringify(storage));
		localStorage.removeItem('product_'+product_id);

		var total=JSON.parse(localStorage.getItem("cart"));
		$('.total-count').html(JSON.parse(total.length));
	});


	$('#setprice').click(function(){
			var total=$("#total_price").html();

			localStorage.setItem("total_price", total);

	});

</script>

@endpush
