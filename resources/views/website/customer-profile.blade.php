@extends('website.layouts.app')

@push('style')

   <link rel="stylesheet" href="{{ asset('/css/customer-profile.css') }}">
    <style>
        .nav-link{
            background-color: #fff !important
        }

        #addresses th,#addresses td{
            padding: 8px
        }
        .button-5 {
			color: #C5171C !important;
            padding: 8px 8px 5px
		}
		h5 .button-5:hover {
			color: #fff !important
        }
        .pay_method img{
          width: 100px;
          margin-right: 20px;
          cursor: pointer;
      }
      .pay_method img:active{
        box-shadow: -1px 1px 14px -4px rgba(0,0,0,0.75);
      }
      [type=radio] { 
        position: absolute;
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* IMAGE STYLES */
        [type=radio] + img {
        cursor: pointer;
        }

        /* CHECKED STYLES */
        [type=radio]:checked + img {
        outline: 2px solid #f00;
        }
    </style>
@endpush

@section('content')

<div class="my-account">
    <div class="container">
        <h4>{{ __('lang.my_account')}}</h4>
    </div>
</div>



<div class="main mt-5">
    <div class="container">
        <div class="row" style="justify-content: space-around">
            <div class="col-md-4 col-sm-12">
                <div class="aside">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" style="display: table-footer-group">
                            <div class="float-left mb-3" style="font-size: 50px;color #838282;padding: 15px;">
                                @if (auth()->guard('customerForWeb')->user()->images != null)
                                    <img style="width: 70px;border-radius:50%" src="{{ asset('/images/customers/'.auth()->guard('customerForWeb')->user()->images) }}" alt="">
                                @else
                                     <i class="float-left mb-3 fas fa-user-circle"></i>
                                @endif 
                            </div>
                            <div class="float-left mb-3 ml-3 mt-2" style="padding-top: 15px;">
                                <h4>{{ auth()->guard('customerForWeb')->user()->name }}</h4>
                                <span>{{ auth()->guard('customerForWeb')->user()->email }}</span>
                            </div>
                        </li>
                        <li class="nav-item ">
                            <a  class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">
                            <i class="fas fa-user"></i>
                            {{ __('lang.profile')}}</a>
                        </li>
                        <li class="nav-item ">
                            <a  class="nav-link" id="addresses-tab" data-toggle="tab" href="#addresses" role="tab" aria-controls="addresses" aria-selected="false">
                            <i class="fas fa-address-card"></i>
                            {{ __('lang.addresses')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="lang-tab" data-toggle="tab" href="#lang" role="tab" aria-controls="lang" aria-selected="false">
                                <img src="{{ asset('images/lang.png') }}" alt="" width="20px">
                                {{ __('lang.languages')}}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="loyality-tab" data-toggle="tab" href="#loyality" role="tab" aria-controls="loyality" aria-selected="false">
                                <img src="{{ asset('images/loyality.png') }}" alt="" width="20px">
                                {{ __('lang.loyality')}}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="favorite-tab" data-toggle="tab" href="#favorite" role="tab" aria-controls="favorite" aria-selected="false">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            {{ __('lang.wishlist')}}</a>
                        </li>
                        <li class="nav-item">
                            <a  class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment" aria-selected="false">
                            <i class="fas fa-credit-card"></i>
                            {{ __('lang.payment_methods')}}</a>
                        </li>
                        <li class="nav-item">
                            <a  class="nav-link" id="order-tab" data-toggle="tab" href="#order" role="tab" aria-controls="order" aria-selected="false">
                            <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                            {{ __('lang.orders')}}</a>
                        </li>
                        <li class="nav-item">
                            <a  class="nav-link" id="message-tab" data-toggle="tab" href="#message" role="tab" aria-controls="message" aria-selected="false">
                            <i class="fas fa-comment-alt"></i>
                            {{ __('lang.messages')}}</a></a>
                        </li>
                        <li>
                            <a  class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                            <i class="fa fa-life-ring" aria-hidden="true"></i>
                            {{ __('lang.contact_support')}}</a>
                        </li>
                        <li style="    padding-left: 29px;">
                            <form action="{{route('web.logout', app()->getLocale())}}" method="post" style="display:inline">
                                @csrf
                                <input name="id" value="{{ Auth::guard('customerForWeb')->user()->id }}" hidden>
                                <button type="submit" style="background: none;border:0;"><i class="ti-power-off"></i>{{ __('lang.logout')}}</button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 my-5">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        @include('website.partials.errors')

                        <h5>{{ __('lang.personal_info')}}</h5>
                        <hr>
                        <!-- Default form row -->
                        <form class="w-100" action="{{ url( app()->getLocale().'/profile/'.auth()->guard('customerForWeb')->user()->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <!-- Grd row -->
                            <div class="form-row w-100" style="margin:0">
                                <div class="col-5">
                                    <div class="form-group">
                                        <div  class="dropzone dz-clickable" >
                                            <div class="dz-default dz-message">
                                            <label>{{ __('lang.user_image')}}</label>
                                                <br>
                                                <input class="box__file" type="file" name="files" id="file" onchange="showImage(this)" value="{{ auth()->guard('customerForWeb')->user()->images }}"  data-multiple-caption="{count} files selected"    />
                                                {{-- <label for="file"><strong>{ __('lang.choose_file')}}</strong></label> --}}
                                            </div>
                                        </div>
                                        <div class="upload-wrap">
        
        
                                        </div>
                                    </div>
                                </div>
                                <!-- Grid column -->
                                <div class="form-group col-5">
                                    <label > {{ __('lang.name')}}</label>
                                    <input name="name" type="text" class="form-control" value="{{ auth()->guard('customerForWeb')->user()->name }}">
                                </div>
                                <div class="form-group  col-5">
                                    <label >{{ __('lang.email')}}</label>
                                    <input name="email" type="text" class="form-control" value="{{ auth()->guard('customerForWeb')->user()->email }}">
                                </div>
                                <div class="form-group  col-5">
                                    <label >{{ __('lang.phone')}} </label>
                                    <input name="phone" type="text" class="form-control" value="{{ auth()->guard('customerForWeb')->user()->phone }}">
                                </div> 
                                <div class="form-group  col-5">
                                </div> 
                                
                                
                               
                                
                              

                            </div>
                            

                            <button type="submit" class="btn mt-5" >  {{ __('lang.update')}}</button>

                            <!-- Grd row -->
                        </form>
                        <!-- Default form row -->
                    </div>
                    <div class="tab-pane fade"             id="lang" role="tabpanel" aria-labelledby="lang-tab">
                        <h5>{{ __('lang.languages')}}</h5>
                        <hr>
                        <div class="row text-center my-5">
                            <div class="col-md-8 col-sm-12  mb-1 text-center" >
                                <select class="form-control" name="lang" id="">
                                    <option  value="">English</option>
                                    <option value="">Arabic</option>
                                </select>
                            </div>
                            
                        </div>
                    </div>
                    <div class="tab-pane fade"             id="addresses" role="tabpanel" aria-labelledby="addresses-tab">
                        <h5>{{ __('lang.addresses')}} <a href="{{ url(app()->getLocale().'/address') }}" class="btn button-5 float-right"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('lang.add')}} </a></h5> 

                        <hr>
                       
                            @if (count(auth()->guard('customerForWeb')->user()->address) > 0 )
                            <div class="row text-center my-5">
                                <table class="table-bordered table-hover">
                                    <thead>
                                        <tr >
                                            <th>{{ __('lang.name')}}</th>
                                            <th>{{ __('lang.phone')}}</th>
                                            <th>{{ __('lang.address')}}</th>
                                            <th>{{ __('lang.city')}}</th>
                                            <th>{{ __('lang.area')}}</th>
                                            <th>{{ __('lang.action')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach (auth()->guard('customerForWeb')->user()->address as $index=>$address)
                                        <tr>
                                            <td>{{ $address->name }}</td>
                                            <td>{{ $address->phone }}</td>
                                            <td>{{ $address->address }}</td>
                                            <td>{{ $address->city }}</td>
                                            <td>{{ $address->area }}</td>
                                            <td>
                                                <a class="success" style="color: rgb(123, 223, 105)" href="{{ url(app()->getLocale().'/address/'.$address->id.'/edit' ) }}"><i class=" fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                <a class="delete warning" style="color: rgb(189, 55, 55)" href=""  id="{{ $address->id }}"><i class=" fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>  
                            </div>
                            @else
                                <h4 class="text-center" style="color: #8a8686">{{ __('lang.no_address_found')}}/h4>
                            @endif
                                                             
                        
                    </div>
                    <div class="tab-pane fade" id="loyality" role="tabpanel" aria-labelledby="loyality-tab">
                        <h5>Loyality</h5>
                        <hr>
                        <div class="row text-center my-5">
                            <div class="col-md-8 col-sm-12  mb-1 text-center" >
                               <h6 >{{ $loyality ?? ' No Loyality Yet' }}</h6>
                            </div>
                            
                        </div>
                    </div>
                    <div class="tab-pane fade" id="favorite" role="tabpanel" aria-labelledby="favorites-tab">
                        <h5>Wishlist</h5>
                        <hr>
                        <div class="row text-center">
                            @if (isset($pros) && count($pros) > 0)

                            <table class="table-bordered table-hover">
                                <thead>
                                    <tr >
                                        <th>Name</th>
                                        <th>Image</th>
                                        {{-- <th>Category</th> --}}
                                        <th>Description</th>
                                        <th>Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pros  as $pro)
                                    <tr>
                                        <td>{{ $pro->name }}</td>
                                        <td><img width="40" src="{{ $pro->images[0]->src }}" alt=""></td>
                                        {{-- <td></td> --}}
                                        <td>{{ $pro->description }}</td>
                                        <td>{{ $pro->regular_price }} EGP</td>
                                        <td>
                                            {{-- <a class="success" style="color: rgb(123, 223, 105)" href=""><i class=" fa fa-pencil-square-o" aria-hidden="true"></i></a> --}}
                                            <a class=" warning Unlike" style="color: rgb(189, 55, 55)" title="{{ $pro->id }}"><i class=" fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>  
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                        <h5>Payment Methods</h5>
                        <hr>
                        <div class="row text-center">
                            <div class="single-widget get-button" style="margin: 30px auto">
                                <div class="content">
                                    <div class="pay_method">
                                        <label>
                                            <input type="radio" name="pay" value="credit"disabled >
                                            <img src="{{ asset('images/card.png') }}">
                                        </label>
                                        <label>
                                            <input  id="pay_cash" type="radio" name="pay" value="cash" checked>
                                            <img src="{{ asset('images/cash.jpeg') }}">
                                        </label>
                         
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="tab-pane fade" id="order" role="tabpanel" aria-labelledby="order-tab">
                        <h4 class="pt-2 pl-3 ">Orders</h4>
                        <hr>
                        <div class="row text-center"> <h6> current orders </h6></div>
                        <div class="row text-center">
                            <section class="Orders mb-5 w-100">
                                <div class="container-fluid">
                                    <div class="row justify-content-start align-items-center">
                                        <div class="col-md-12 mt-1 p-0">

                                                    
                                            @foreach ($orders as $order)
                                                <div class="col-md-12 mt-3 gray-line1 pt-1" style="color:#fff">
                                                    <h5 class="mb-1" style="display: inline;float: left;">Order# {{ $order->id }}</h5>
                                                    <div class="card-header" id="heading2" style="background-color: #fff; padding: 0px; border-bottom: 0px;">
                                                        <button class="btn btn-link text-left button-1" style="display: inline;float: right;color:#fff;text-decoration: underline;padding: 0;background: #666666;" type="button" data-toggle="collapse" data-target="#collapse{{ $order->id }}" aria-expanded="true" aria-controls="collapse">
                                                                show details
                                                        </button>
                                                    </div>
                                                </div>
                                                @php
                                                    $date =explode(" ", $order->created_at);
                                                    $items=json_decode($order->OrderEntry);
                                                    $total_price=0;
                                                    foreach ($items as  $item) {
                                                        $total_price+=(floatval ($item->price) * floatval ($item->quantity));
                                                    }
                                                @endphp
                                                <p class="py-3"><span class="float-left">Order Date: </span><span class="float-right">{{ $date[0] }} </span></p>
                                                <p class="py-3"><span class="float-left">Order Status: </span><span class="float-right">{{ $order->order_status ==''? 'pending' : '' }} </span></p>
                                                <div class="accordion-{{ $order->id  }}" id="accordion-{{ $order->id  }}">
                                                    <div  style="border: none;">
                                                        <div  id="heading{{ $order->id  }}" style="background-color: #fff; padding: 0px; border-bottom: 0px;">
                                                            
                                                        </div>
                                                        <div id="collapse{{ $order->id }}" class="collapse" aria-labelledby="heading{{ $order->id  }}" data-parent="#accordion-{{ $order->id  }}">
                                                            <div class="card-body" style="padding: 5px 0px 0px 0px;">
                                                                <p class="py-3" style="text-align: left">Shipping Address: {{ $order->CustomerAddress }} </p>
                                                                <span  for="" style="float: left !important;font-weight:bold;margin 5px 0;color:#827E8B">Order Items :</span>
                                                                <table style="text-align: left">
                                                                
                                                                @foreach ($items as $item)
                                                                    <tr>
                                                                        <td>{{ $item->name }}</td>
                                                                        <td>x{{ $item->quantity }}</td>
                                                                        <td>{{ floatval ($item->price) * floatval ($item->quantity) }} EGP</td>
                                                                    </tr>
                                                                    {{-- <p class="py-3"><span class="float-left"> </span><span class="float-right"> </span></p> --}}
                                                                
                                                                @endforeach
                                                            </table>
                                                                
                                                                <hr>
                                                            </div>
                                                            {{ $total_price }} EGP

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                                
                                        </div>

                                        
                                    </div>
                                </div>
                            </section>
                        </div>
                        <hr>
                        <div class="row text-center"> <h6> past orders </h6></div>
                        <div class="row text-center">
                            <section class="Orders mb-5 w-100">
                                <div class="container-fluid">
                                    <div class="row justify-content-start align-items-center">
                                        <div class="col-md-12 mt-1 p-0">

                                                    
                                            @foreach ($pastorders as $order)
                                                <div class="col-md-12 mt-3 gray-line1 pt-1" style="color:#fff">
                                                    <h5 class="mb-1" style="display: inline;float: left;">Order# {{ $order->id }}</h5>
                                                    <div class="card-header" id="heading2" style="background-color: #fff; padding: 0px; border-bottom: 0px;">
                                                        <button class="btn btn-link text-left button-1" style="display: inline;float: right;color:#fff;text-decoration: underline;padding: 0;background: #666666;" type="button" data-toggle="collapse" data-target="#collapse{{ $order->id }}" aria-expanded="true" aria-controls="collapse">
                                                                show details
                                                        </button>
                                                    </div>
                                                </div>
                                                @php
                                                    $date =explode(" ", $order->created_at);
                                                    $items=json_decode($order->OrderEntry);
                                                    $total_price=0;
                                                    foreach ($items as  $item) {
                                                        $total_price+=(floatval ($item->price) * floatval ($item->quantity));
                                                    }
                                                @endphp
                                                <p class="py-3"><span class="float-left">Order Date: </span><span class="float-right">{{ $date[0] }} </span></p>
                                                <p class="py-3"><span class="float-left">Order Status: </span><span class="float-right">{{ $order->order_status ==''? 'pending' : '' }} </span></p>
                                                <div class="accordion-{{ $order->id  }}" id="accordion-{{ $order->id  }}">
                                                    <div  style="border: none;">
                                                        <div  id="heading{{ $order->id  }}" style="background-color: #fff; padding: 0px; border-bottom: 0px;">
                                                            
                                                        </div>
                                                        <div id="collapse{{ $order->id }}" class="collapse" aria-labelledby="heading{{ $order->id  }}" data-parent="#accordion-{{ $order->id  }}">
                                                            <div class="card-body" style="padding: 5px 0px 0px 0px;">
                                                                <p class="py-3" style="text-align: left">Shipping Address: {{ $order->CustomerAddress }} </p>
                                                                <span  for="" style="float: left !important;font-weight:bold;margin 5px 0;color:#827E8B">Order Items :</span>
                                                                <table style="text-align: left">
                                                                
                                                                @foreach ($items as $item)
                                                                    <tr>
                                                                        <td>{{ $item->name }}</td>
                                                                        <td>x{{ $item->quantity }}</td>
                                                                        <td>{{ floatval ($item->price) * floatval ($item->quantity) }} EGP</td>
                                                                    </tr>
                                                                    {{-- <p class="py-3"><span class="float-left"> </span><span class="float-right"> </span></p> --}}
                                                                
                                                                @endforeach
                                                            </table>
                                                                
                                                                <hr>
                                                            </div>
                                                            {{ $total_price }} EGP

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                            <hr class="ml-3 mr-3">
                                                
                                        </div>

                                        
                                    </div>
                                </div>
                            </section>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="message" role="tabpanel" aria-labelledby="message-tab">
                        <h5>Messages</h5>
                        <hr>
                        <div class="row text-center">
                            {{-- <div class="col-md-8 col-sm-12  mb-1 text-center" style="margin: 20% auto">
                                <h4>Download the DishDivvy App to be able to message your cook</h4>
                                <a href="#"> <img width="150" src="https://shop.dishdivvy.com/assets/dd-appstore-button.png" alt=""></a>
                                <a href="#"> <img width="180" src="https://shop.dishdivvy.com/assets/dd-google-play-button.png" alt=""></a>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <a href="#"> <img  width="250" height="500" src="https://shop.dishdivvy.com/assets/iphone-image.png" alt=""></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class=" tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <h5>{{__('lang.contact_support')}}
                        </h5>
                        <hr>
                        <h6 class="my-2">{{__('lang.how_can_i_help_you')}}?</h6>
                        <form action="{{ url(app()->getLocale().'/profile/email') }}" method="POST" >
                            @csrf
                            <!-- Group of default radios - option 1 -->
                            <div class="custom-control custom-radio1">
                                <input type="radio" class="custom-control-input" id="defaultGroupExample1"value="order" name="number">
                                <label class="custom-control-label" for="defaultGroupExample1">I have an issue with my order</label>
                                <div class="radio1"  style="display:none">
                                    <input class="form-control radio1" type="text" name="order" placeholder="# Order Number">
                                    <div class="form-group mt-2 shadow-textarea">
                                        <textarea class="form-control z-depth-1" name="order-message" id="exampleFormControlTextarea6" rows="3" placeholder="Please include a detailed description of your customer support issue, and a epresentative will respond to you as soon as possible."></textarea>
                                    </div>
                                </div>
                            </div>

                            <!-- Group of default radios - option 2 -->
                            <div class="custom-control custom-radio2">
                                <input type="radio" class="custom-control-input" id="defaultGroupExample2" value="driver" name="number" >
                                <label class="custom-control-label" for="defaultGroupExample2">I have a comment about my delivery driver</label>
                                <div class="radio2" style="display:none">
                                    <input class="form-control " type="text" name="driver"  placeholder="Driver Name">
                                    <div class="form-group mt-2 shadow-textarea">
                                        <textarea class="form-control z-depth-1" name="driver-message" id="exampleFormControlTextarea6" rows="3" placeholder="Please include a detailed description of your customer support issue, and a representative will respond to you as soon as possible."></textarea>
                                    </div>
                                </div>
                            </div>

                            <!-- Group of default radios - option 3 -->
                            <div class="custom-control custom-radio3">
                                <input type="radio" class="custom-control-input" value="other" id="defaultGroupExample3" name="number">
                                <label class="custom-control-label" for="defaultGroupExample3">Other customer support request</label>
                                <div class="radio3"  style="display:none">
                                    <div class="form-group mt-2 shadow-textarea">
                                        <textarea class="form-control z-depth-1" name="other-message" id="exampleFormControlTextarea6" rows="3" placeholder="Please include a detailed description of your customer support issue, and a representative will respond to you as soon as possible."></textarea>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn mt-5">{{__('lang.send')}} </button>
                        </form>

                    </div>
                  </div>
            </div>

        </div>
    </div>
</div>




@endsection




@push('scripts')
<script src="{{ asset('/js/customer-profile.js') }}"></script>

<script >

let url='/html/oscar_web/public/';


 function removeDiv(elem){
            $(elem).parent('div').remove();
            $('#file').val('');
        }


function showImage(file){


for (var i = 0; i < file.files['length']; i++) {

var img='<div class="upload-image float-left" id="upload-image" style="display:inline-block">'+
        "<img id='img_src' width=50 src="+"'" +window.URL.createObjectURL(file.files[i])+"'"+">"+
        '<a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a>'+
    '</div>';

$(".upload-wrap").append(img);

$('.upload-image').css('display','block');
};
};

$.ajaxSetup({

headers: {

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

}

});

$(".delete").click(function(e){
    var select = $(this);
    var id =select.attr('id');
    e.preventDefault();
    
    '/{{app()->getLocale()}}/address/'

    $.ajax({
    type:'DELETE',
    url:url+'/{{app()->getLocale()}}/address/'+id,
  

    success:function(data){
           
        select.closest('tr').remove();

        
        
        }	
    });


});



$(".Unlike").click(function(e){
			e.preventDefault();
			
	
			var id = $(this).attr("title");
			$.ajax({
			   type:'POST',
			   url:url+'/{{app()->getLocale()}}/homeWeb/unLike',
	
			   data:{ id:id },

			   success:function(data){
				//    console.log(data);

			}
	
			});
			
            $(this).closest('tr').remove();
            var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)-Number(1));
		});


</script>
@endpush
