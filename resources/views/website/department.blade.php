@extends('website.layouts.app')

<style>
.restaurant{
    margin: 30 auto;
}
.restaurant .image-group{
}
.restaurant h1{
    color: #EA0706;
    text-align: center;
    margin: 10px

}



/* 
.content {
  position: relative;
  width: 90%;
  max-width: 400px;
  margin: auto;
  overflow: hidden;
} */

.content .content-overlay {
  background: rgba(0,0,0,0.7);
  position: absolute;
  height: 99%;
  width: 92%;
  left: 15px;
  top: 0;
  bottom: 0;
  right: 0;
  opacity: 1;
  -webkit-transition: all 0.4s ease-in-out 0s;
  -moz-transition: all 0.4s ease-in-out 0s;
  transition: all 0.4s ease-in-out 0s;
}
/* 
.content:hover .content-overlay{
 
} */

.content-image{
  width: 100%;
}

.content-details {
  position: absolute;
  text-align: center;
  padding-left: 1em;
  padding-right: 1em;
  width: 100%;
  top: 50%;
  left: 50%;
  opacity: 1;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  -webkit-transition: all 0.3s ease-in-out 0s;
  -moz-transition: all 0.3s ease-in-out 0s;
  transition: all 0.3s ease-in-out 0s;
}

/* .content:hover .content-details{
  top: 50%;
  left: 50%;
  opacity: 1;
} */

.content-details h3{
  color: #fff;
  font-weight: 500;
  letter-spacing: 0.15em;
  margin-bottom: 0.5em;
  text-transform: uppercase;
}

.content-details p{
  color: #fff;
  font-size: 0.8em;
}

.fadeIn-bottom{
  top: 80%;
}



.fadeIn-right{
  left: 80%;
}

</style>
@section('content')
<?php $lang= app()->getLocale();?>
    <div class="department my-5">
        <div class="container pl-0 pr-0">
            <div class=" my-5">
                <div class="row" style="text-align: center">
                    @if (isset($categories))
                      @foreach ($categories as $category)
                        <div class="col-md-4 col-sm-6 col-xs-12 mb-2">
                            <a class="content" href="{{ url(app()->getLocale().'/homeWeb/all_products/'. $category->id ) }}" >
                                <div class="content-overlay">  <h3  style="color:#fff; margin-top:15px; font-weight: 500;  letter-spacing: 0.15em;margin-bottom: 0.5em;text-transform: uppercase;">{{ $category->name->$lang }}</h3></div>

                                <img class="content-image" src="{{ $category->image['src'] }}">
                                <div class="content-details fadeIn-bottom fadeIn-right" style="display:none ;">
                               
                                {{-- <p>{{ $category->description  ?? ' '}}</p> --}}
                                </div>
                            </a>
                        </div>
                      @endforeach
                    @else
                    <div class="col-md-4 col-sm-6 col-xs-12 mb-2">
                      <a class="content" href="{{ url('homeWeb/all_products/'. $cat->id ) }}" >
                          <div class="content-overlay"> <h3 style="color:#fff; margin-top:15px; font-weight: 500;  letter-spacing: 0.15em;margin-bottom: 0.5em;text-transform: uppercase;">{{ $category->name->$lang }}</h3></div>

                          <img class="content-image" src="{{ $cat->image['src'] }}">
                          <div class="content-details fadeIn-bottom fadeIn-right" style="display:none ;">
                          <h3>{{ $cat->name->$lang  }}</h3>
                          {{-- <p>{{ $category->description  ?? ' '}}</p> --}}
                          </div>
                      </a>
                  </div>
                    @endif
                </div>
               
            </div>
        </div>
    </div>
@endsection
@push('scripts')
	<script type="text/javascript">


	</script>
@endpush
