@extends('website.layouts.app')
<style>

	.contact .col-md-3{
        /* background: #C5171C;
        color: #fff; */
		padding: 4%;
		border-top-left-radius: 0.5rem;
		border-bottom-left-radius: 0.5rem;
	}
	.contact-info{
		margin-top:10%;
	}
	.contact-info img{
		margin-bottom: 15%;
	}
	.contact-info h2{
		margin-bottom: 10%;
	}
	.contact .col-md-9{
		background: #fff;
		padding: 3%;
		border-top-right-radius: 0.5rem;
		border-bottom-right-radius: 0.5rem;
	}
	.contact-form label{
		font-weight:600;
	}
	.contact-form button{
		background: #25274d;
		color: #fff;
		font-weight: 600;
		width: 25%;
	}
	.contact-form button:focus{
		box-shadow:none;
	}
</style>

@section('content')

    <div class="container  " >
        <div class="contact my-3">
         
            <div class="row">
                <h2>OSCAR Grand Stores</h2>
                <div class="col-md-9">
                    <p>Oscar is Egypt’s favored shopping destination, and has been that way for decades. Since our inauguration in 1982, we have been the destination of choice for those who seek quality products from across the globe at affordable, optimum value-for-money prices. <br>

                        What we offer at Oscar stores is a shopping experience that is pleasurable and simultaneously cost-effective. Oscar experts travel the four corners of the world and return with the best that the world has to offer. Our local and imported product varieties are chosen carefully to answer to the needs of those who appreciate quality and find pleasure in satisfying their eclectic taste. <br>
                        
                        The choices you have at Oscar are endless. The integrated varieties of products on display at our stores make it unnecessary to consider going elsewhere for your home shopping. Our philosophy also revolves around making your shopping experience fun and enjoyable, where shopping for groceries is no longer a chore, but an event to look forward to! <br>
                        
                        Oscar Grandstores has taken this philosophy a few steps further by providing shoppers with a destination that will become one of Cairo’s most distinguished landmarks. With more than 20 departments on show, Oscar Grandstores promises to be the exclusively favored shopping destination for dwellers of Cairo and surrounding cities. <br>
                        
                        Oscar Grandstores is located in the heart of New Cairo and is built on an impressive area of 14,000 square meters over five floors. The property contains an ample parking area for visitors that gives quick and easy access to the all the different departments available. Oscar Grandstores also contains 5 different elevators, ample signage and is smartly designed to give you easy and comfortable access to whatever it is you are looking for. <br>
                        
                        Oscar Grandstores was conceptualized, designed and built to offer an unbeatably enjoyable shopping experience. With that in mind, no expense was spared in building this property, and every corner has been invested in utilizing the best materials and facilities the world has to offer.</p>
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('images/about1.gif') }}" alt="">
                </div>
            </div>
        </div>
    </div>
 
@endsection

