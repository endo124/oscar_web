<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Meta Tag -->
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Title Tag  -->
	<title>Oscar</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="{{asset('images/favicon.png')}}">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"rel="stylesheet">
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDst9gRFVsQsqj4-XzWY65uHKT8Ak_RC1E&callback=initMap&libraries=&v=weekly"defer></script>

	<!-- StyleSheet -->

	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{asset('css/magnific-popup.min.css')}}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
	<!-- Fancybox -->
	<link rel="stylesheet" href="{{asset('css/jquery.fancybox.min.css')}}">
	<!-- Themify Icons -->
	<link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
	<!-- Nice Select CSS -->
	<link rel="stylesheet" href="{{asset('css/niceselect.css')}}">
	<!-- Animate CSS -->
	<link rel="stylesheet" href="{{asset('css/animate.css')}}">
	<!-- Flex Slider CSS -->
	<link rel="stylesheet" href="{{asset('css/flex-slider.min.css')}}">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{asset('css/owl-carousel.css')}}">
	<!-- Slicknav -->
	<link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}">

	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="{{asset('css/reset.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('css/responsive.css')}}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
	<!-- Google Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
	<!-- Bootstrap core CSS -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDst9gRFVsQsqj4-XzWY65uHKT8Ak_RC1E&callback=initMap&libraries=&v=weekly"defer></script>
	{{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> --}}
	<style>
		.nav-tabs .nav-item.show .nav-link,
		.nav-tabs .nav-link.active {
			background-color: #09158C;
		}

		.nav-tabs .nav-item.show .nav-link,
		.nav-tabs .nav-link {
			background-color: #fff;
		}
		.pagination{
			margin: 50px auto 0
		}
		
		.ti-heart:hover, .ti-heart.red { color: #fff; }

		.ti-heart{
		color :#000
		}
		.liked{
			color:#f00
		}
		.navbar-nav li:hover>.dropdown-menu {
			display: block;
		}
		.lang li{
			display: block !important;

		}
		.panel-heading i{
			font-size: small !important
		}
		@media (min-width: 576px){
			.modal-dialog {
				max-width: 575px;
			}
		}


		.single-icon{
			position: relative;
		}
		.total-count ,.wishlist-total-count{
			background-color: #C5171C;
			color: #fff;
			border-radius: 55%;
			padding: 2px 7px;
			font-size: small;
			position: absolute;
			top: -13px;
			left: -18px;
		}
		.prefix{
			left: 0;
		}

		/* Dropdown Button */
.dropbtn {
  background-color: white;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #ddd;}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {display: block;}

/* Change the background color of the dropdown button when the dropdown content is shown */
/* .dropdown:hover .dropbtn {background-color: #3e8e41;} */
.row{
	width: 100% !important;
	margin: 0
}

.slicknav_menu a{
	margin-top: 12px
}
/* .bottom-nav{
	display: none
} */

.bottom-nav {
	display: none;
	z-index: 999999999999999999999999999;

}
@media (min-width: 768px) { 
	#cartandwishlist{
		display: none;
	}
	
 }
@media (max-width: 768px) { 
	footer{
		display: none;

	}
	.topbar{
		display: none
	}

	.bottom-nav {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  width: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
}

.bottom-nav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  z-index: 99999;
}

.branches{
	display: block
}
}



.bottom-nav .modal-footer{
	padding:0 1rem !important;
}

.bottom-nav .nav-tabs .nav-item a{
	color: #000
}
.bottom-nav .nav-tabs .nav-item .active{
	color: #fff
}

.modal-dialog.cascading-modal .modal-c-tabs .md-tabs{
	box-shadow: none
}
.branches{
	display: none
}
.single-slider{
	display: flex !important;
	align-items: center;
}
.product-action{
	width: 62px;
}

.dropdown .dropbtn{
	padding-top: 0;
	padding-bottom: 0;
}

.right-content .down{
	margin-bottom: 0px;
    display: flex;
    align-items: center;
}

.top-left .top{
	margin-bottom: 0px;

}

	</style>

@stack('style')
@stack('top_script')
</head>

<body class="js">
 
 
	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->

	@isset($errors)
		@if ($errors == 'email or password is invalid')
			<h1 id="error" class="alert alert-danger"  style="display:
			@if($errors == 'email or password is invalid')
			block
			@else
			none
			@endif
			">{{ $errors }}</h1>
		@endif
	
	@endisset

	
	<?php $lang= app()->getLocale();?>

	<!-- Header -->
	<header class="header shop">
		<!-- Topbar -->
		<div class="topbar" style="    padding-top: 10px;
		padding-bottom: 10px;">
		

			<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-12 col-12" style="display: flex;    align-items: center;">
							<!-- Top Left -->
							<div class="top-left">
								<ul class="list-main top" >
									<li><i class="ti-headphone-alt"></i><a href="tel:0224184183">0224184183 </a> </li>
									<li><i class="ti-email"></i><a href="mailto:info@oscarstores.com">info@oscarstores.com</a>	</li>
								</ul>
							</div>
							<!--/ End Top Left -->
						</div>
						
						<div class="col-lg-8 col-md-12 col-12">
							<!--Modal: Login / Register Form-->
							<!-- Top Right -->
							<div class="right-content">
								<ul class="list-main down"  style="
								display: flex;
								align-items: center;
							">
									
									<li>
										<div class="dropdown">
										@if($lang=='ar')
										<button class="dropbtn"><img src="{{asset('images/ar.png') }}" style="width:10%"></button>

										@else
										<button class="dropbtn"><img src="{{asset('images/en.png') }}" style="width:10%"></button>

										@endif
											<div class="dropdown-content">
											<!-- <a class="dropdown-item" href="lang/en"><img> English</a>
                                           <a class="dropdown-item" href="lang/ar"><img > Arabic</a> -->
										   @foreach (config('app.available_locales') as $locale)
@if(is_null(\Illuminate\Support\Facades\Route::currentRouteName()))
@if(!is_null(\Request::segment(3)) && \Request::segment(3)=='branch')
<?php $id=\Request::segment(4);?>
<a class="nav-link"
href="{{ url($locale.'/homeWeb/branch/'.$id) }}"
@if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>

@elseif(!is_null(\Request::segment(3)) && \Request::segment(3)=='all_products')
<?php $id=\Request::segment(4);?>
<a class="nav-link"
href="{{ url($locale.'/homeWeb/all_products/'.$id) }}"
@if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>

@else
<a class="nav-link"
href="{{ route('homeWeb', $locale) }}"
@if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>
@endif
@elseif(\Illuminate\Support\Facades\Route::currentRouteName()=='homeWeb.show')
<a class="nav-link"
<?php $slug=\Request::segment(3);
$url='homeWeb/'.$slug?>
href="{{ url($locale.'/homeWeb/'.$slug) }}"
@if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>
@elseif(\Illuminate\Support\Facades\Route::currentRouteName()=='homeWeb.index')
<a class="nav-link"
<?php $slug=\Request::segment(4);
?>
href="{{ url($locale.'/homeWeb/branch/'.$slug) }}"
@if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>
@elseif(!is_null(\Request::segment(3)) && \Request::segment(3)=='search_product')
<?php $search=\Request::query('search')?>
<a class="nav-link"
href="{{ url($locale.'/homeWeb/search_product?search='.$search) }}"
@if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>
@elseif(!is_null(\Request::segment(2)) && \Request::segment(2)=='address')
<?php $id=\Request::segment(3)?>
<a class="nav-link"
href="{{ url($locale.'/address/'.$id.'/edit') }}"
@if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>

@else
<a class="nav-link"
href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), $locale) }}"
@if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($locale) }}</a>
@endif

            
     
    @endforeach
        
											</div>
										  </div>
									</li>
									<li>
										<div class="dropdown">
											<button class="dropbtn" style="color: #000"> <i class="ti-location-pin"></i>{{ $branch['title']->$lang }}</button>
											<div class="dropdown-content">
												@for ($i = 0; $i < count($branches); $i++)
													@if ($branches[$i]->store_id != $branch['store_id'])
														<a id="{{ $branches[$i]->store_id }}" onclick="set_branch(this)" href="{{ url(app()->getLocale().'/homeWeb/branch/'.$branches[$i]->store_id) }}"  >{{ $branches[$i]->title->$lang  }}</a>
													@endif
												@endfor
											</div>
										  </div>
									</li>
									
									{{-- <li><i class="ti-alarm-clock"></i> <a href="#">Daily deal</a></li> --}}
									<!--Modal: Login / Register Form-->
									<div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog cascading-modal" role="document">
										<!--Content-->
										<div class="modal-content">
									
											<!--Modal cascading tabs-->
											<div class="modal-c-tabs">
									
												<!-- Nav tabs -->
												<ul class="nav nav-tabs md-tabs tabs-2  darken-3" role="tablist">
													<li class="nav-item" >
														<a class="nav-link active" id="active"  data-toggle="tab" href="#panel7"
															role="tab"><i class="fas fa-user mr-1"></i>
															{{ __('lang.login')}}</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i
																class="fas fa-user-plus mr-1"></i>
																{{ __('lang.register')}}</a>
													</li>
												</ul>
									
											<!-- Tab panels -->
											<div class="tab-content">
												{{-- @include('website.partials.errors') --}}

												<!--Panel 7-->
												<div class="tab-pane fade in show active" id="panel7" role="tabpanel">
									
												<!--Body-->
												<div class="modal-body mb-1">
													<form action="{{route('web.login', app()->getLocale())}}" method="post" class="needs-validation" novalidate>
														{{ csrf_field() }}
														<div class="md-form form-sm mb-5">
															<i class="fas fa-envelope prefix"></i>
															<input type="email" id="validationCustom032" name="email" placeholder="{{ __('lang.email')}}"
																class="form-control form-control-sm validate" required>
																
															{{-- <label for="validationCustom032"></label> --}}
														</div>
	
														<div class="md-form form-sm mb-4">
															<i class="fas fa-lock prefix"></i>
															<input type="password" id="modalLRInput11" name="password" placeholder="{{ __('lang.password')}}"
																class="form-control form-control-sm validate" required>
																
															{{-- <label data-error="wrong" data-success="right"
																for="modalLRInput11"></label> --}}
														</div>
														<div class="text-center form-sm mt-2">
															<button class="btn btn-info" type="submit">{{ __('lang.login')}} <i
																	class="fas fa-sign-in ml-1"></i></button>
														</div>
													</form>
												</div>
												<!--Footer-->
												<div class="modal-footer">
													<div class="options text-center text-md-right mt-1">
														<p>{{ __('lang.forget_password')}} <a href="{{route('web.forgot_password', app()->getLocale())}}" class="blue-text">?</a></p>
													</div>
													<button type="button"
														class="btn btn-outline-info waves-effect ml-auto"
														data-dismiss="modal">{{ __('lang.close')}}</button>
												</div>
									
												</div>
												<!--/.Panel 7-->
									
												<!--Panel 8-->
												<div class="tab-pane fade" id="panel8" role="tabpanel">
									
												<!--Body-->
												<div class="modal-body">
													<form action="{{route('web.register', app()->getLocale())}}" method="post" class="needs-validation" novalidate>
														{{ csrf_field() }}
																<div class="md-form form-sm mb-5">
																	<i class="fas fa-user prefix"></i>
																	<input type="text" name="name"
																		class="form-control form-control-sm validate" placeholder="{{ __('lang.name')}}" value="{{ old('name') }}">
																	{{-- <label data-error="wrong" data-success="right"></label> --}}
																</div>
																<div class="md-form form-sm mb-5">
																	<i class="fas fa-mobile prefix"></i>
																	<input type="number" name="phone" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
																	type = "number"
																	maxlength = "12"
																		class="form-control form-control-sm validate" placeholder="{{ __('lang.phone')}}" value="{{ old('phone') }}">
																	{{-- <label data-error="wrong" data-success="right"></label> --}}
																</div>
		
																<div class="md-form form-sm mb-5">
																	<i class="fas fa-envelope prefix"></i>
																	<input type="email" id="email" name="email"
																		class="form-control form-control-sm validate" placeholder="{{ __('lang.email')}}" required value="{{ old('email') }}">
																	{{-- <label data-error="wrong" data-success="right"
																		for="modalLRInput12"></label> --}}
																</div>
		
																<div class="md-form form-sm mb-5">
																	<i class="fas fa-lock prefix"></i>
																	<input type="password" name="password"
																		class="form-control form-control-sm validate" placeholder="{{ __('lang.password')}}">
																	{{-- <label data-error="wrong" data-success="right"
																		for="modalLRInput13"></label> --}}
																</div>
		
																<div class="md-form form-sm mb-4">
																	<i class="fas fa-lock prefix"></i>
																	<input type="password"  name="password_confirmation"
																		class="form-control form-control-sm validate" placeholder="{{ __('lang.confirm_password')}}">
																	{{-- <label data-error="wrong" data-success="right"
																		for="modalLRInput14"></label> --}}
																</div>
		
																<div class="text-center form-sm mt-2">
																	<button class="btn btn-info" type="submit">{{ __('lang.register')}} <i
																			class="fas fa-sign-in ml-1"></i></button>
																</div>
															</form>
									
												</div>
												<!--Footer-->
												<div class="modal-footer">

													<button type="button"
														class="btn btn-outline-info waves-effect ml-auto"
														data-dismiss="modal">{{ __('lang.close')}}</button>
												</div>
												</div>
												<!--/.Panel 8-->
											</div>
									
											</div>
										</div>
										<!--/.Content-->
										</div>
									</div>
									<!--Modal: Login / Register Form-->
									@if(Auth::guard('customerForWeb')->user() )
									<li><i class="ti-user"></i> <a id="account" href="{{ url(app()->getLocale().'/profile') }}">{{ __('lang.my_account')}}</a></li>

										<li>
											<form action="{{route('web.logout', app()->getLocale())}}" method="post" style="display:inline">
												@csrf
												<input name="id" value="{{ Auth::guard('customerForWeb')->user()->id }}" hidden>
												<button type="submit" style="background: none;border:0;"><li><i class="ti-power-off"></i><span id="logout">{{ __('lang.logout')}}</span></button>
											</form>
										</li>
									@else
										<li><i class="ti-power-off"></i><a href="#" data-toggle="modal"
												data-target="#modalLRForm">{{ __('lang.login')}} | {{ __('lang.register')}}</a>
										</li>								
									
									@endif
									
								</ul>
							</div>
							<!-- End Top Right -->
						</div>
						

					</div>
			</div>
		</div>
		<!-- End Topbar -->
		<div class="middle-inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-12">
						<!-- Logo -->
						<div class="logo d-none d-xl-block">
							<a href="{{ url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id) }}"><img src="{{ asset('images/OscarFullLogo.png') }}" alt="logo" ></a>
						</div>
						<a href="{{ url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id) }}">
							<div class="logo d-xl-none">
								<img src="{{ asset('images/OscarFullLogo.png') }}" alt="logo"  width="120">
							</div>
						</a>
						<!--/ End Logo -->
						<!-- Search Form -->
						<div class="search-top">
							<!-- Search Form -->
							<div class="search-top">
								<form class="search-form" action="{{ route('search',app()->getLocale()) }}" method="GET">
								
									<input type="text" placeholder="{{ __('lang.search_here')}}" name="search">
									<button value="search" type="submit"><i class="ti-search"></i></button>
								</form>
							</div>
							<!--/ End Search Form -->
						</div>
						<!--/ End Search Form -->
						<div class="mobile-nav  " style="position: relative">
							
						</div>
					</div>
					<div class="col-lg-10 col-md-7 col-12">
						<div class="search-bar-top">
							<div class="search-bar">
								
								<form class="w-100" action="{{ route('search',app()->getLocale()) }}" method="GET">
									
									
									<input  class="w-100" name="search" placeholder="{{ __('lang.search_here')}}" type="search">
									<button class="btnn"><i class="ti-search"></i></button>
								</form>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>

		<!-- Header Inner -->
		<div class="header-inner d-none d-xl-block d-lg-block d-md-block">
			<div class="container-fluid">
				<div class="cat-nav-head">
					<div class="row" style="justify-content: space-around" >
						<div class="col-3">
							<div class="all-category" style="height: 100%">
								<h3 class="cat-heading" style="    display: flex;"><i class="fa fa-bars"></i>{{ __('lang.categories')}}</h3>
								<ul class="main-category mt-2" style="height: 500px;overflow-y:scroll;width:400%;z-index:999"  >
									@if (isset($categories))
										@foreach ($categories as $category)
											<li><a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $category->id ) }}">{{  $category->name->$lang }} <i class="fa fa-angle-right" aria-hidden="true"></i></a>
												<ul class="sub-category " >
													<li ><a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $category->id ) }}">{{ __('lang.show_all')}}</a></li>
													@foreach ($category->children as $sub1_child)
														@if(count($sub1_child->children) > 1)
														<li class="sub1">
															<a href=" {{ url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id ) }}">{{$sub1_child->name->$lang }} <i class="fa fa-angle-right"aria-hidden="true"></i></a>
															<ul class="sub1-category"  >
																<li><a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id ) }}">{{ __('lang.show_all')}}</a></li>
																@foreach ($sub1_child->children as $sub2_child)
																	@if(count($sub2_child->children) > 1)
																		<li class="sub2">
																			<a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $sub2_child->id ) }}">{{$sub2_child->name->$lang  }} <i class="fa fa-angle-right"aria-hidden="true"></i></a>
																			<ul class="sub2-category" >																	
																				<li><a href="{{ url(app()->getLocale().'/homeWeb/all_products/'. $sub2_child->id ) }}">{{ __('lang.show_all')}}</a></li>
																				
																				@foreach ($sub2_child->children as $sub3_child)
																					@if(count($sub3_child->children) > 1)

																					<li class="sub">
																						<a href="{{ url( app()->getLocale().'/homeWeb/'.$sub3_child->slug) }}">{{$sub3_child->name->$lang  }} </a>
																					</li>
																					@else
																					<li ><a href="{{ url( app()->getLocale().'/homeWeb/'.$sub3_child->slug) }}">{{$sub3_child->name->$lang }}</a></li>
						
																					@endif
																				@endforeach
																			</ul>
																		</li>
																	@else
																		<li ><a href="{{ url( app()->getLocale().'/homeWeb/'.$sub2_child->slug) }}">{{$sub2_child->name->$lang }}</a></li>
																	@endif
																@endforeach
															</ul>
														</li>
															
														@else
															<li ><a href="{{ url( app()->getLocale().'/homeWeb/'.$sub1_child->slug) }}">{{$sub1_child->name->$lang }}</a></li>
														@endif
													@endforeach
												</ul>
											</li>
										@endforeach
									@else
									<li><a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $cat->id ) }}">{{  $cat->name->$lang }} <i class="fa fa-angle-right" aria-hidden="true"></i></a>
										<ul class="sub-category " >
											<li ><a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $cat->id ) }}">{{ __('lang.show_all')}}</a></li>
											@foreach ($cat->children as $sub1_child)
												@if(count($sub1_child->children) > 1)
												<li class="sub1">
													<a href=" {{ url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id ) }}">{{$sub1_child->name->$lang }} <i class="fa fa-angle-right"aria-hidden="true"></i></a>
													<ul class="sub1-category"  >
														<li><a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id ) }}">{{ __('lang.show_all')}}</a></li>
														@foreach ($sub1_child->children as $sub2_child)
															@if(count($sub2_child->children) > 1)
																<li class="sub2">
																	<a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $sub2_child->id ) }}">{{$sub2_child->name->$lang  }} <i class="fa fa-angle-right"aria-hidden="true"></i></a>
																	<ul class="sub2-category" >																	
																		<li><a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $sub2_child->id ) }}">{{ __('lang.show_all')}}</a></li>
																		
																		@foreach ($sub2_child->children as $sub3_child)
																			@if(count($sub3_child->children) > 1)

																			<li class="sub">
																				<a href="{{ url( app()->getLocale().'/homeWeb/'.$sub3_child->slug) }}">{{$sub3_child->name->$lang  }} </a>
																			</li>
																			@else
																			<li ><a href="{{ url( app()->getLocale().'/homeWeb/'.$sub3_child->slug) }}">{{$sub3_child->name->$lang }}</a></li>
				
																			@endif
																		@endforeach
																	</ul>
																</li>
															@else
																<li ><a href="{{ url( app()->getLocale().'/homeWeb/'.$sub2_child->slug) }}">{{$sub2_child->name->$lang }}</a></li>
															@endif
														@endforeach
													</ul>
												</li>
													
												@else
													<li ><a href="{{ url( app()->getLocale().'/homeWeb/'.$sub1_child->slug) }}">{{$sub1_child->name->$lang }}</a></li>
												@endif
											@endforeach

									@endif
									
								</ul>
							</div>
						</div>
						<div class="col-9" style=" display:flex;justify-content: space-between;">
							<div class="menu-area" style="height:100%">
								<!-- Main Menu -->
								<nav class="navbar navbar-expand-lg" style="height:100%;box-shadow:none">
									<div class="navbar-collapse" style="    justify-content: center;">
										<div class="nav-inner">
											<ul class="nav main-menu menu navbar-nav">
												<li class="branches" style="    margin-left: -3px;">
													<div class="dropdown">
														<button class="dropbtn" style="color: #000 ;padding:0"> <i class="ti-location-pin"></i>{{ $branch['title']->en }}</button>
														<div class="dropdown-content">
															@for ($i = 0; $i < count($branches); $i++)
																@if ($branches[$i]->store_id != $branch['store_id'])
																	<a id="{{ $branches[$i]->store_id }}" onclick="set_branch(this)" href="{{ url(app()->getLocale().'/homeWeb/branch/'.$branches[$i]->store_id) }}"  >{{ $branches[$i]->title->en  }}</a>
																@endif
															@endfor
														</div>
													  </div>
												</li>
												<li class="d-none" style="color: #000 !;margin:0;font-weight:bold">CATEGORIES <i class="fa fa-angle-right" aria-hidden="true"></i></li>
												
												<li class="{{ Request::segment(2) =='homeWeb' ? 'active' : ' ' }} "><a href="{{ url(app()->getLocale().'/homeWeb/branch/'.$branch->store_id) }}">{{ __('lang.home')}}</a></li>
												<li class="{{ Request::segment(2) =='department' ? 'active' : ' ' }}"> <a href="{{ url(app()->getLocale().'/department') }}"  data-content="Link Hover" aria-hidden="true">{{ __('lang.departments')}}</a></li>
												<li class="{{ Request::segment(2) =='restaurant' ? 'active' : ' ' }}"> <a href="{{ url(app()->getLocale().'/restaurant') }}"  data-content="Link Hover" aria-hidden="true">{{ __('lang.restaurants')}}</a></li>
												<li class="{{ Request::segment(2) =='aboutus' ? 'active' : ' ' }}"><a href="{{ url (app()->getLocale().'/aboutus') }}"  data-content="Link Hover" aria-hidden="true">{{ __('lang.about_us')}}</a></li>
												
												{{-- <li class="{{ Request::segment(1) =='Product' ? 'active' : ' ' }} "><a href="#">Product</a></li>												 --}}
												<li class="{{ Request::segment(1) =='contact' ? 'active' : ' ' }}"> <a href="{{ url(app()->getLocale().'/contact') }}"  data-content="Link Hover" aria-hidden="true">{{ __('lang.contact_us')}}</a></li>
												
											</ul>
										</div>
									</div>
								</nav>
								<!--/ End Main Menu -->
							</div>
							<div class="right-bar">
								
								<a href="{{ route('cart', app()->getLocale()) }}" class=" single-icon icon-nav" style="margin-right:20px"> <span id="cart-count2" class="total-count"></span><i class="ti-bag " style=" font-size:larger !important;margin-right:2px"></i></a>
								@if(auth()->guard('customerForWeb')->user())
								<a href="{{ url(app()->getLocale().'/profile') }}" class="single-icon icon-nav">
									<a href="{{ url(app()->getLocale().'/profile') }}" class="single-icon icon-nav">
										@isset($pros)
											@if (count($pros) > 0)
												<span id="wishlist-count" class="wishlist-total-count">{{ count($pros) }}</span> 
											@endif
										@endisset
										<i style="font-size: large;" class="ti-heart " ></i>
									</a>
									
								</a>
								@endif	
								
									
								
	
								<div class="sinlge-bar shopping">
	
										@isset($customer)
											<!-- Shopping Item -->
											<div class="shopping-item">
												<div class="dropdown-cart-header">
													<span>{{ count($customer->products) }} Items</span>
													<a href="#">View WishList</a>
												</div>
												<ul class="shopping-list">
													@foreach ($customer->products as $product)
													<li id="{{ $product->id }}">
														<a href="" class="remove" title="Remove this item"><i
																class="fa fa-remove"></i></a>
														<a class="cart-img" href="#"><img src="{{$product->images[0]->src  }}"
																alt="#"></a>
														<h4><a href="#">{{ $product->name }}</a></h4>
														<p class="quantity"> <span class="amount">{{ $product->regular_price }}</span></p>
													</li>
													@endforeach
													
												</ul>
												{{-- <div class="bottom">
													<div class="total">
														<span>Total</span>
														<span class="total-amount">$0000000</span>
													</div>
													<a href="checkout.html" class="btn animate">Checkout</a>
												</div> --}}
											</div>
											<!--/ End Shopping Item -->
										@endisset
	
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<!--/ End Header Inner -->
	</header>
	<!--/ End Header -->
	<div id="alert-cart" class="alert alert-success" style="position: fixed ;display:none;z-index:9999" >
	
	</div>
	
	<div id="alert-wishlist" class="alert alert-danger" style="position: fixed ;display:none;z-index:9999" >
		
	</div>
	
		
	{{-- @endif --}}
	
    @yield('content')
    
    
	
	<div  class="bottom-nav" style="justify-content: space-around"> 
			<div class="modal fade" id="modalLRFormmobile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="    z-index: 999999;
		     	" aria-hidden="true">
				<div class="modal-dialog cascading-modal" role="document">
				<!--Content-->
				<div class="modal-content">
			
					<!--Modal cascading tabs-->
					<div class="modal-c-tabs">
			
						<!-- Nav tabs -->
						<ul class="nav nav-tabs md-tabs tabs-2  darken-3" role="tablist">
							<li class="nav-item" >
								<a class="nav-link active" id="active"  data-toggle="tab" href="#panel7mobile"
									role="tab"><i class="fas fa-user mr-1"></i>
									{{ __('lang.login')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#panel8mobile" role="tab"><i
										class="fas fa-user-plus mr-1"></i>
										{{ __('lang.register')}}</a>
							</li>
						</ul>
				
						<!-- Tab panels -->
						<div class="tab-content">
							{{-- @include('website.partials.errors') --}}

							<!--Panel 7-->
							<div class="tab-pane fade in show active" id="panel7mobile" role="tabpanel">
				
							<!--Body-->
							<div class="modal-body mb-1">
								<form action="{{route('web.login', app()->getLocale())}}" method="post" class="needs-validation" novalidate>
									{{ csrf_field() }}
									<div class="md-form form-sm mb-5">
										<i class="fas fa-envelope prefix"></i>
										<input type="email" id="validationCustom032" name="email" placeholder="Your email"
											class="form-control form-control-sm validate" required>
											
										{{-- <label for="validationCustom032"></label> --}}
									</div>

									<div class="md-form form-sm mb-4">
										<i class="fas fa-lock prefix"></i>
										<input type="password" id="modalLRInput11" name="password" placeholder="Your password"
											class="form-control form-control-sm validate" required>
											
										{{-- <label data-error="wrong" data-success="right"
											for="modalLRInput11"></label> --}}
									</div>
									<div class="text-center form-sm mt-2">
										<button class="btn btn-info" type="submit">{{ __('lang.login')}} <i
												class="fas fa-sign-in ml-1"></i></button>
									</div>
								</form>
							</div>
							<!--Footer-->
							<div class="modal-footer">
								<div class="options text-center text-md-right mt-1">
									<p> <a href="{{route('web.forgot_password', app()->getLocale())}}" class="blue-text">{{ __('lang.forget_password')}}?</a></p>
								</div>
								<button type="button" style="padding: 10px"
									class="btn btn-outline-info waves-effect ml-auto"
									data-dismiss="modal">{{ __('lang.close')}}</button>
							</div>
				
							</div>
							<!--/.Panel 7-->
				
							<!--Panel 8-->
							<div class="tab-pane fade" id="panel8mobile" role="tabpanel">
				
							<!--Body-->
							<div class="modal-body">
								<form action="{{route('web.register', app()->getLocale())}}" method="post" class="needs-validation" novalidate>
									{{ csrf_field() }}
											<div class="md-form form-sm mb-1">
												<i class="fas fa-user prefix"></i>
												<input type="text" name="name"
													class="form-control form-control-sm validate" placeholder="Your Name" value="{{ old('name') }}">
												{{-- <label data-error="wrong" data-success="right"></label> --}}
											</div>
											<div class="md-form form-sm mb-1">
												<i class="fas fa-mobile prefix"></i>
												<input type="number" name="phone" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
												type = "number"
												maxlength = "12"
													class="form-control form-control-sm validate" placeholder="Your Phone" value="{{ old('phone') }}">
												{{-- <label data-error="wrong" data-success="right"></label> --}}
											</div>

											<div class="md-form form-sm mb-1">
												<i class="fas fa-envelope prefix"></i>
												<input type="email" id="email" name="email"
													class="form-control form-control-sm validate" placeholder="Your email" required value="{{ old('email') }}">
												{{-- <label data-error="wrong" data-success="right"
													for="modalLRInput12"></label> --}}
											</div>

											<div class="md-form form-sm mb-1">
												<i class="fas fa-lock prefix"></i>
												<input type="password" name="password"
													class="form-control form-control-sm validate" placeholder="Your password">
												{{-- <label data-error="wrong" data-success="right"
													for="modalLRInput13"></label> --}}
											</div>

											<div class="md-form form-sm mb-1">
												<i class="fas fa-lock prefix"></i>
												<input type="password"  name="password_confirmation"
													class="form-control form-control-sm validate" placeholder="Repeat password">
												{{-- <label data-error="wrong" data-success="right"
													for="modalLRInput14"></label> --}}
											</div>

											<div class="text-center form-sm mt-2">
												<button class="btn btn-info" type="submit">Sign up <i
														class="fas fa-sign-in ml-1"></i></button>
											</div>
										</form>
				
							</div>
							<!--Footer-->
							<div class="modal-footer" style="padding-right:1rem !important;padding-left:1rem !important">

								<button type="button"
									class="btn btn-outline-info waves-effect ml-auto"
									data-dismiss="modal">Close</button>
							</div>
							</div>
							<!--/.Panel 8-->
						</div>
			
					</div>
				</div>
				<!--/.Content-->
				</div>
			</div>

		@if(Auth::guard('customerForWeb')->user() )
			<a id="account" href="{{ url(app()->getLocale().'/profile') }}"><i class="ti-user"></i></a>

			<a href="{{ url(app()->getLocale().'profile')  }}" class="single-icon icon-nav">
				@isset($pros)
					@if (count($pros) > 0)
						<span id="wishlist-count"  style="top: 4px;left: -9px;" class="wishlist-total-count">{{ count($pros) }}</span> 
					@endif
				@endisset
				<i style="font-size: large;color:#fff" class="ti-heart " ></i>
			</a>
			

				

		@else
					
				{{-- <i class="ti-power-off"></i><a href="#" data-toggle="modal" data-target="#modalLRForm">{{ __('lang.login')}} | {{ __('lang.register')}}</a> --}}
				 <a href="#" data-toggle="modal" data-target="#modalLRFormmobile"><i class="ti-user" style="color: #fff;font-size:large"></i></a>								
			{{-- </ul> --}}
				
		@endif
	
		<a href="{{ route('cart', app()->getLocale()) }}" class=" single-icon icon-nav" style="margin-right:20px">
		 <span id="cart-count1" style="top: 4px;left: -9px;" class="total-count"></span>
		 <i class="ti-bag " style=" font-size:larger !important;margin-right:2px;color:#fff"></i>
		</a>

		<div class="top-search">
			<a href="#0"><i class="ti-search"></i></a>
		</div>
	</div>
	<!-- Start Footer Area -->
	<footer class="footer">
		<!-- Footer Top -->
		<div class="footer-top section1">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about social">
							<h2 style="font-weight: 500;">ABOUT US</h2>
							<p class="text">Oscar is Egypt’s favored shopping destination, and has been that way for decades. Since our inauguration in 1982, we have been the destination of choice for those who seek quality products from across the globe at affordable, optimum value-for-money prices.</p>
							
							<ul>
								<li><a href="#"><i class="ti-facebook"></i></a></li>
								<li><a href="#"><i class="ti-instagram"></i></a></li>
							</ul>
						</div>

						<!-- End Single Widget -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
							<h2  style="font-weight: 500;">GET IN TOUCH</h2>
							<!-- Single Widget -->
							<div class="contact">
								
									<p   style=" color:white;" ><span  style="font-weight: 500; color:white" > Address: </span>105 Omar Ibn El Khatab street, Heliopolis, Cairo.</p>
									<p class="pt-3"><span  style="font-weight: 500; color:white"> Telephone: </span><a   style="color:white" href="tel:0224184183">0224184183.</a></p>
									<p class="pt-3"><span  style="font-weight: 500; color:white"> Email: </span><a   style="color:white" href="mailto:info@oscarstores.com">info@oscarstores.com</a></p>
									<p class="pt-4"  style="font-weight: 500;  color:white;" >Got Question? Call us 24/7</p>
									<p  style="font-weight: 500; color:white;">Don't hesitate to <a  style="color:white" href="mailto:info@oscarstores.com"> contact us</a></p>
								
							</div>
							<!-- End Single Widget -->
							
						</div>
						<!-- End Single Widget -->
					</div>
				</div>
			</div>
			<div class="container">
				<div class="inner">
					<div class="row">
						<div class="col-lg-12 col-12">
							<div class="right float-right">
								<p>© 2021 <a style="color:white" href="http://momentum-sol.com/">Momentum-sol</a>. All Rights Reserved.	 </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Top -->
		
	</footer>
	<!-- /End Footer Area -->

   

	<!-- Jquery -->
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<script src="{{asset('js/jquery-migrate-3.0.0.js')}}"></script>
	<script src="{{asset('js/jquery-ui.min.js')}}"></script>
	<!-- Popper JS -->
	<script src="{{asset('js/popper.min.js')}}"></script>
	<!-- Bootstrap JS -->
	<script src="{{asset('js/bootstrap.min.js')}}"></script>

	<!-- Slicknav JS -->
	<script src="{{asset('js/slicknav.min.js')}}"></script>
	<!-- Owl Carousel JS -->
	<script src="{{asset('js/owl-carousel.js')}}"></script>
	<!-- Magnific Popup JS -->
	<script src="{{asset('js/magnific-popup.js')}}"></script>
	<!-- Waypoints JS -->
	<script src="{{asset('js/waypoints.min.js')}}"></script>
	<!-- Countdown JS -->
	<script src="{{asset('js/finalcountdown.min.js')}}"></script>
	<!-- Nice Select JS -->
	<script src="{{asset('js/nicesellect.js')}}"></script>
	<!-- Flex Slider JS -->
	<script src="{{asset('js/flex-slider.js')}}"></script>
	<!-- ScrollUp JS -->
	<script src="{{asset('js/scrollup.js')}}"></script>
	<!-- Onepage Nav JS -->
	<script src="{{asset('js/onepage-nav.min.js')}}"></script>
	<!-- Easing JS -->
	<script src="{{asset('js/easing.js')}}"></script>
	<!-- Active JS -->
	<script src="{{asset('js/active.js')}}"></script>



	
	<script>


		
	// $(document).scroll(function() {


	// var y = $(this).scrollTop();
	// if (y > 50) {
	// 	$('.slicknav_nav').fadeOut();
	// } else {
	// 	$('.slicknav_nav').fadeIn();
	// }
	// });

	function set_branch(){
		var cart =[];
		localStorage.setItem('cart', JSON.stringify(cart));
	}

	 	$( document ).ready(function() {

			$('body').on('hidden.bs.modal', function () {
				
				$('.input-number').val(0);
			});
			var storage=JSON.parse(localStorage.getItem("cart"));

			 if(storage ==null){
				 $('#cart-count2').css('background', 'transparent');
				 $('#cart-count2').hide();
				 $('#cart-count1').css('background', 'transparent');
				 $('#cart-count1').hide();
				
			 }else{
				
				$('#cart-count2').html(storage.length);
				$('#cart-count1').html(storage.length);

			 }
			

			// $('#cartandwishlist').hide()

			if ($(window).width() < 442) {
				$('#logout').hide();
			}
			if($(window).width() <402){
				$('#account').hide();
			}
			// if($(window).width() <768){
				
			// 	$('#cartandwishlist').show()
			// }
	// 		getLocation();
	// 		var x = document.getElementById("demo");
	// 		function getLocation() {
	// 		if (navigator.geolocation) {
	// 		navigator.geolocation.getCurrentPosition(showPosition);


	// 		} else {
	// 			x.innerHTML = "Geolocation is not supported by this browser.";
	// 		}
			
	// 		}
	// 		function showPosition(position) {
	// 			setCookie(position.coords.latitude,position.coords.longitude);

	// 		}			
			// function setCookie(lat, lat, exdays) {
			// 	var d = new Date();
			// 	d.setTime(d.getTime() + (exdays*24*60*60*1000));
			// 	var expires = "expires="+ d.toUTCString();
			// 	document.cookie = 'lat' + "=" + lat + ";" + expires + ";path=/";
			// 	document.cookie = 'long' + "=" + long + ";" + expires + ";path=/";
			// }

	
	 });
	 $( document ).ready(function() {

						setTimeout(function() { 
							$('#error').fadeOut('fast'); 
						}, 6000);
	});
	</script>


	@stack('scripts')
</body>









</html>