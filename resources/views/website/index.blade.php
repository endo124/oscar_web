@extends('website.layouts.app')

<style>
	@media only screen and (min-width: 768px) {
		.tab-height img{
			height: 450 !important;
		}
		.banner1 img{
			height: 900 !important;

		}
	}

	@media only screen and (max-width: 992px) {
		.tab-height img{
			height: 140 !important;
		}
		.banner1 img{
			height: 300 !important;
		}
	}



	@media only screen and (min-width: 992px) {
		.banner{
			height: 700px;
			margin:  0 auto;
			width: 100%;
			object-fit: fill;
		
		}

	}
	.ti-heart:hover{
color: red !important
}
</style>

<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">



@section('content')

@isset($error)
	<h1 class="modal_error">{{ $error }}</h1>
@endisset
<div class="row">
	{{-- <div class="sticky " style="top: 73"><a href="#"><img src="{{ asset('images/google-play-badge.png') }}" alt="" style="display: inline" width="130"></a> <a href="#"><img style="display: inline" src="{{ asset('images/ios badge.png') }}"width="130"  alt=""></a></div> --}}
	<img class="w-100 banner" src="{{ $slider->image ?? '' }}" alt="">
	
</div>
<?php $lang= app()->getLocale();?>
<!-- Slider Area -->

<!--/ End Slider Area -->

<!-- Start Small Banner  -->
<section class="small-banner section">
	<div class="container-fluid d-sm-block d-md-block d-lg-none d-xl-none ">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h2>{{ __('lang.promotions')}}</h2>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<!-- Single Banner  -->
			<div class="col-md-4 col-6">
				<div class="single-banner banner1">
					<img class="banner" src="{{ $banners[0]['image'] }}" width="600" alt="#" class="img-fluid">
				</div>
			</div>
			<!-- /End Single Banner  -->
			<!-- Single Banner  -->
			<div class="col-md-4 col-6 " width="600">
				@for ($i = 1; $i < count($banners); $i++)

				<div class="row">
						<div class="col-12" >
							<div class="single-banner tab-height">
								<img  src="{{ $banners[$i]['image'] }}" alt="#"  class=" img-fluid" style="height: 150px;object-fit: cover;">
							</div>
						</div>
				</div>		
				@endfor

			</div>
			<!-- /End Single Banner  -->
		</div>
	</div>
	<div class="container-fluid d-none d-lg-block d-xl-block ">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h2>{{ __('lang.promotions')}}</h2>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			@foreach ($banners as $banner)
				<div class="col-4">
					<div class="single-banner tab-height">
						<img  src="{{ $banner['image'] }}"  class=" img-fluid" style=";object-fit: cover;">
					</div>
				</div>
			@endforeach
			
			<!-- /End Single Banner  -->
		</div>
	</div>
</section>

<!-- End Small Banner -->

<!-- Start Product Area -->
{{-- <div class="product-area section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h2>Trending Item</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="product-info">
					<div class="nav-main">
						<!-- Tab Nav -->
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#man"
									role="tab">Man</a></li>
							<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#women"
									role="tab">Woman</a></li>
							<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#kids"
									role="tab">Kids</a></li>
							<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#accessories"
									role="tab">Accessories</a></li>
							<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#essential"
									role="tab">Essential</a></li>
							<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#prices"
									role="tab">Prices</a></li>
						</ul>
						<!--/ End Tab Nav -->
					</div>
					<div class="tab-content" id="myTabContent">
						<!-- Start Single Tab -->
						<div class="tab-pane fade show active" id="man" role="tabpanel">
							<div class="tab-single">
								<div class="row">
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Hot Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Pink Show</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="new">New</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Pant Collectons</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="price-dec">30% Off</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Cap For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Polo Dress For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="out-of-stock">Hot</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Black Sunglass For Women</a></h3>
												<div class="product-price">
													<span class="old">$60.00</span>
													<span>$50.00</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--/ End Single Tab -->
						<!-- Start Single Tab -->
						<div class="tab-pane fade" id="women" role="tabpanel">
							<div class="tab-single">
								<div class="row">
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Hot Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Pink Show</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="new">New</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Pant Collectons</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="price-dec">30% Off</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Cap For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Polo Dress For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="out-of-stock">Hot</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Black Sunglass For Women</a></h3>
												<div class="product-price">
													<span class="old">$60.00</span>
													<span>$50.00</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--/ End Single Tab -->
						<!-- Start Single Tab -->
						<div class="tab-pane fade" id="kids" role="tabpanel">
							<div class="tab-single">
								<div class="row">
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Hot Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Pink Show</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="new">New</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Pant Collectons</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="price-dec">30% Off</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Cap For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Polo Dress For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="out-of-stock">Hot</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Black Sunglass For Women</a></h3>
												<div class="product-price">
													<span class="old">$60.00</span>
													<span>$50.00</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--/ End Single Tab -->
						<!-- Start Single Tab -->
						<div class="tab-pane fade" id="accessories" role="tabpanel">
							<div class="tab-single">
								<div class="row">
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Hot Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Pink Show</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="new">New</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Pant Collectons</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="price-dec">30% Off</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Cap For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Polo Dress For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="out-of-stock">Hot</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Black Sunglass For Women</a></h3>
												<div class="product-price">
													<span class="old">$60.00</span>
													<span>$50.00</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--/ End Single Tab -->
						<!-- Start Single Tab -->
						<div class="tab-pane fade" id="essential" role="tabpanel">
							<div class="tab-single">
								<div class="row">
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Hot Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Pink Show</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="new">New</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Pant Collectons</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="price-dec">30% Off</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Cap For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Polo Dress For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="out-of-stock">Hot</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Black Sunglass For Women</a></h3>
												<div class="product-price">
													<span class="old">$60.00</span>
													<span>$50.00</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--/ End Single Tab -->
						<!-- Start Single Tab -->
						<div class="tab-pane fade" id="prices" role="tabpanel">
							<div class="tab-single">
								<div class="row">
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Hot Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Pink Show</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="new">New</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Women Pant Collectons</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Bags Collection</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="price-dec">30% Off</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Awesome Cap For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Polo Dress For Women</a></h3>
												<div class="product-price">
													<span>$29.00</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4 col-12">
										<div class="single-product">
											<div class="product-img">
												<a href="product-details.html">
													<img class="default-img"
														src="https://via.placeholder.com/550x750" alt="#">
													<img class="hover-img" src="https://via.placeholder.com/550x750"
														alt="#">
													<span class="out-of-stock">Hot</span>
												</a>
												<div class="button-head">
													<div class="product-action">
														<a data-toggle="modal" data-target="#exampleModal"
															title="Quick View" href="#"><i
																class=" ti-eye"></i><span>Quick Shop</span></a>
														<a title="Wishlist" href="#"><i
																class=" ti-heart "></i><span>Add to
																Wishlist</span></a>
														<a title="Compare" href="#"><i
																class="ti-bar-chart-alt"></i><span>Add to
																Compare</span></a>
													</div>
													<div class="product-action-2">
														<a title="Add to cart" href="#">Add to cart</a>
													</div>
												</div>
											</div>
											<div class="product-content">
												<h3><a href="product-details.html">Black Sunglass For Women</a></h3>
												<div class="product-price">
													<span class="old">$60.00</span>
													<span>$50.00</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--/ End Single Tab -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div> --}}
<!-- End Product Area -->

<!-- Start Midium Banner  -->
{{-- <section class="midium-banner">
	<div class="container">
		<div class="row">
			<!-- Single Banner  -->
			<div class="col-lg-6 col-md-6 col-12">
				<div class="single-banner">
					<img src="https://via.placeholder.com/600x370" alt="#">
					<div class="content">
						<p>Man's Collectons</p>
						<h3>Man's items <br>Up to<span> 50%</span></h3>
						<a href="#">Shop Now</a>
					</div>
				</div>
			</div>
			<!-- /End Single Banner  -->
			<!-- Single Banner  -->
			<div class="col-lg-6 col-md-6 col-12">
				<div class="single-banner">
					<img src="https://via.placeholder.com/600x370" alt="#">
					<div class="content">
						<p>shoes women</p>
						<h3>mid season <br> up to <span>70%</span></h3>
						<a href="#" class="btn">Shop Now</a>
					</div>
				</div>
			</div>
			<!-- /End Single Banner  -->
		</div>
	</div>
</section> --}}
<!-- End Midium Banner -->

<!-- Start Most Popular -->
<div class="product-area most-popular section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h2>{{ __('lang.members_offers')}}</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel popular-slider">
					<!-- Start Single Product -->
					@foreach ($offers as $offer)
						<div class="single-product">
							<div class="product-img">
								<a   href="{{ url(app()->getLocale().'/show_product/'.$offer->id) }}">
									<img class="default-img" src="{{ $offer->images[0]->src  }}" style="background-image: url({{   asset('images/image.png') }}); height:247px;background-size: contain;background-repeat:no-repeat" >
									
								</a>
								<div class="button-head">
									<div class="product-action">
										<a data-toggle="modal" data-target="#exampleModal{{ $offer->id }}" title="Quick View"href="#"><i class=" ti-eye"></i><span>Quick Shop</span></a>
										@isset($pros)
											{{-- @if(in_array($product->id,$pros)) --}}
											<a  class="Unlike" title="{{ $offer->id }}" href="#" style="display: {{ in_array($offer->id,$pros) ? 'block' : 'none' }};margin-right: 0;margin-left: 15px;">
												<i class=" ti-heart liked"></i>
												<span>Remove From Wishlist</span>
											<a>
											{{-- @else --}}
											<a  class="Wishlist" title="{{ $offer->id }}" href="#" style="display: {{ in_array($offer->id,$pros) ? 'none' : 'block' }}">
												<i class=" ti-heart "></i>
												<span>Add to Wishlist</span>
											</a>
											{{-- @endif --}}
										@endisset
									</div>
									<div class="product-action-2">
										<a class="cart" unit={{ $offer->PriceUnit }}  title="{{ $offer->id }} " >{{ __('lang.add_to_cart')}}</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<h3><a href="#">{{ $offer->name }}</a></h3>
								<div class="product-price">
									<span class="old">{{   $offer->regular_price}}</span>
									<span>{{$offer->discountprice  }}</span>
								</div>
							</div>
						</div>
					@endforeach
					<!-- End Single Product -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Most Popular Area -->

<!-- Start Shop Home List  -->
{{-- <section class="shop-home-list section">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<div class="shop-section-title">
							<h1>On sale</h1>
						</div>
					</div>
				</div>
				<!-- Start Single List  -->
				<div class="single-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="list-image overlay">
								<img src="https://via.placeholder.com/115x140" alt="#">
								<a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12 no-padding">
							<div class="content">
								<h4 class="title"><a href="#">Licity jelly leg flat Sandals</a></h4>
								<p class="price with-discount">$59</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Single List  -->
				<!-- Start Single List  -->
				<div class="single-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="list-image overlay">
								<img src="https://via.placeholder.com/115x140" alt="#">
								<a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12 no-padding">
							<div class="content">
								<h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
								<p class="price with-discount">$44</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Single List  -->
				<!-- Start Single List  -->
				<div class="single-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="list-image overlay">
								<img src="https://via.placeholder.com/115x140" alt="#">
								<a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12 no-padding">
							<div class="content">
								<h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
								<p class="price with-discount">$89</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Single List  -->
			</div>
			<div class="col-lg-4 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<div class="shop-section-title">
							<h1>Best Seller</h1>
						</div>
					</div>
				</div>
				<!-- Start Single List  -->
				<div class="single-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="list-image overlay">
								<img src="https://via.placeholder.com/115x140" alt="#">
								<a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12 no-padding">
							<div class="content">
								<h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
								<p class="price with-discount">$65</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Single List  -->
				<!-- Start Single List  -->
				<div class="single-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="list-image overlay">
								<img src="https://via.placeholder.com/115x140" alt="#">
								<a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12 no-padding">
							<div class="content">
								<h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
								<p class="price with-discount">$33</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Single List  -->
				<!-- Start Single List  -->
				<div class="single-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="list-image overlay">
								<img src="https://via.placeholder.com/115x140" alt="#">
								<a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12 no-padding">
							<div class="content">
								<h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
								<p class="price with-discount">$77</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Single List  -->
			</div>
			<div class="col-lg-4 col-md-6 col-12">
				<div class="row">
					<div class="col-12">
						<div class="shop-section-title">
							<h1>Top viewed</h1>
						</div>
					</div>
				</div>
				<!-- Start Single List  -->
				<div class="single-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="list-image overlay">
								<img src="https://via.placeholder.com/115x140" alt="#">
								<a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12 no-padding">
							<div class="content">
								<h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
								<p class="price with-discount">$22</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Single List  -->
				<!-- Start Single List  -->
				<div class="single-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="list-image overlay">
								<img src="https://via.placeholder.com/115x140" alt="#">
								<a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12 no-padding">
							<div class="content">
								<h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
								<p class="price with-discount">$35</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Single List  -->
				<!-- Start Single List  -->
				<div class="single-list">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="list-image overlay">
								<img src="https://via.placeholder.com/115x140" alt="#">
								<a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12 no-padding">
							<div class="content">
								<h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
								<p class="price with-discount">$99</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Single List  -->
			</div>
		</div>
	</div>
</section> --}}
<!-- End Shop Home List  -->

<!-- Start Cowndown Area -->
{{-- <section class="cown-down">
	<div class="section-inner ">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-12 padding-right">
					<div class="image">
						<img src="https://via.placeholder.com/750x590" alt="#">
					</div>
				</div>
				<div class="col-lg-6 col-12 padding-left">
					<div class="content">
						<div class="heading-block">
							<p class="small-title">Deal of day</p>
							<h3 class="title">Beatutyful dress for women</h3>
							<p class="text">Suspendisse massa leo, vestibulum cursus nulla sit amet, frungilla
								placerat lorem. Cars fermentum, sapien. </p>
							<h1 class="price">$1200 <s>$1890</s></h1>
							<div class="coming-time">
								<div class="clearfix" data-countdown="2021/02/30"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> --}}
<!-- /End Cowndown Area -->

<!-- Start Shop B  -->
{{-- <section class="shop-blog section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h2>From Our Blog</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-6 col-12">
				<!-- Start Single Blog  -->
				<div class="shop-single-blog">
					<img src="https://via.placeholder.com/370x300" alt="#">
					<div class="content">
						<p class="date">22 July , 2020. Monday</p>
						<a href="#" class="title">Sed adipiscing ornare.</a>
						<a href="#" class="more-btn">Continue Reading</a>
					</div>
				</div>
				<!-- End Single Blog  -->
			</div>
			<div class="col-lg-4 col-md-6 col-12">
				<!-- Start Single Blog  -->
				<div class="shop-single-blog">
					<img src="https://via.placeholder.com/370x300" alt="#">
					<div class="content">
						<p class="date">22 July, 2020. Monday</p>
						<a href="#" class="title">Man’s Fashion Winter Sale</a>
						<a href="#" class="more-btn">Continue Reading</a>
					</div>
				</div>
				<!-- End Single Blog  -->
			</div>
			<div class="col-lg-4 col-md-6 col-12">
				<!-- Start Single Blog  -->
				<div class="shop-single-blog">
					<img src="https://via.placeholder.com/370x300" alt="#">
					<div class="content">
						<p class="date">22 July, 2020. Monday</p>
						<a href="#" class="title">Women Fashion Festive</a>
						<a href="#" class="more-btn">Continue Reading</a>
					</div>
				</div>
				<!-- End Single Blog  -->
			</div>
		</div>
	</div>
</section> --}}
<!-- End Shop Blog  -->

<!-- Start Shop Services Area -->
{{-- <section class="shop-services section home">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-12">
				<!-- Start Single Service -->
				<div class="single-service">
					<i class="ti-rocket"></i>
					<h4>Free shiping</h4>
					<p>Orders over $100</p>
				</div>
				<!-- End Single Service -->
			</div>
			<div class="col-lg-3 col-md-6 col-12">
				<!-- Start Single Service -->
				<div class="single-service">
					<i class="ti-reload"></i>
					<h4>Free Return</h4>
					<p>Within 30 days returns</p>
				</div>
				<!-- End Single Service -->
			</div>
			<div class="col-lg-3 col-md-6 col-12">
				<!-- Start Single Service -->
				<div class="single-service">
					<i class="ti-lock"></i>
					<h4>Sucure Payment</h4>
					<p>100% secure payment</p>
				</div>
				<!-- End Single Service -->
			</div>
			<div class="col-lg-3 col-md-6 col-12">
				<!-- Start Single Service -->
				<div class="single-service">
					<i class="ti-tag"></i>
					<h4>Best Peice</h4>
					<p>Guaranteed price</p>
				</div>
				<!-- End Single Service -->
			</div>
		</div>
	</div>
</section> --}}
<!-- End Shop Services Area -->

<!-- Start Shop Newsletter  -->
{{-- <section class="shop-newsletter section">
	<div class="container">
		<div class="inner-top">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 col-12">
					<!-- Start Newsletter Inner -->
					<div class="inner">
						<h4>Newsletter</h4>
						<p> Subscribe to our newsletter and get <span>10%</span> off your first purchase</p>
						<form action="mail/mail.php" method="get" target="_blank" class="newsletter-inner">
							<input name="EMAIL" placeholder="Your email address" required="" type="email">
							<button class="btn">Subscribe</button>
						</form>
					</div>
					<!-- End Newsletter Inner -->
				</div>
			</div>
		</div>
	</div>
</section> --}}
<!-- End Shop Newsletter -->


	<!-- Modal -->
	@foreach ($offers as $offer)
		<div class="modal fade" id="exampleModal{{ $offer->id }}" tabindex="-1" role="dialog" style="margin-top: 50px">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
								aria-hidden="true"></span></button>
					</div>
					<div class="modal-body">
						<div class="row no-gutters">
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
								<!-- Product Slider -->
								<div class="product-gallery">
									<div class="quickview-slider-active">
										<div class="single-slider">
											<img class="w-100   "   src="{{ $offer->images[0]->src  }}"  style="background-image: url({{ asset('images/image.png') }}); height:247px;background-size: contain;background-repeat:no-repeat" alt="#">
										</div>
										
									</div>
								</div>
								<!-- End Product slider -->
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="background-color: #ccc">
								<div class="quickview-content">
									<h2 style="margin-top: 100px;font-size: large;">{{ $offer->name  }}</h2>
										<h5 style=" margin-top: 20px;font-size: medium;"><span style="font-weight:bold;font-size:small">{{ __('lang.category')}} :</span>{{ $offer->categories_all->name->$lang }}</h5>
									
										<div class="product-price" style="font-size: small">
											<span style="font-weight:bold;font-size:small">{{ __('lang.unite_price')}} : </span>
											<span class="old" style="text-decoration: line-through">{{   $offer->regular_price}} EGP</span>
											<span >{{$offer->discountprice  }} EGP</span>
										</div>
										@if (!is_null($offer->discription))
											<div class="quickview-peragraph">
												<p><span style="font-weight:bold;font-size:small">>{{ __('lang.description')}} : </span> {{$offer->discription  }}.</p>
											</div>
										@endif
									
									
									<div class="quantity my-3">
										<!-- Input Order -->
										<div class="input-group">
											<div class="button minus" id="Quantity">
												<button type="button" class="btn  btn-number" disabled="disabled"
													data-type="minus" data-field="quant[1]" style="height:33px">
													<i class="ti-minus"></i>
												</button>
											</div>
											<input type="text" name="quant[1]" class="input-number qty" data-min="{{ $offer->PriceUnit =='Quantity'?'1' : '0.1' }}"
											data-max="9000" value="{{ $offer->PriceUnit =='Quantity'?'1' : '0.1' }}" style="border:0">
										<div class="button plus" id="Quantity">
											<button id="{{  $offer->id }}" type="button" class="btn add  btn-number"   data-type="plus"
												data-field="quant[1]" style="height:33px" data={{ $offer }}>
												<i class="ti-plus"></i>
											</button>
										</div>
										</div>
										<!--/ End Input Order -->
									</div>
									<div class="add-to-cart mt-2" style="display: flex;justify-content: space-between;align-items: center;">
										<a class="quickcart" onclick="quickcart(this,event)" title="{{ $offer->id }}" href="#" class="btn" style="background-color: #333;color:#fff;padding: 10px 42px;" >{{ __('lang.add_to_cart')}}</a>
										@if(auth()->guard('customerForWeb')->user())
											<a  class="Wishlist" title="{{ $offer->id }}" href="#" class="btn min"  style="display: {{ in_array($offer->id,$pros) ? 'none' : 'block' }};background-color: #333;color:#fff;padding:0 10px"><i class="ti-heart" style="font-weight: bold;color: #fff;margin:auto; line-height: 43px"></i></a>
											<a  class="Unlike" title="{{ $offer->id }}" href="#" class="btn min"  style="display: {{ in_array($offer->id,$pros) ? 'block' : 'none' }};background-color: #333;color:#fff;padding:0 10px"><i class="ti-heart" style="font-weight: bold;color: #fff;margin:auto; line-height: 43px"></i></a>
										@endif
										{{-- <a href="#" class="btn min"><i class="fa fa-compress"></i></a> --}}
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- Modal end -->
	@endforeach
 
@endsection


@push('scripts')



	<script type="text/javascript">


	
	let url='/html/oscar_web/public/';

		$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}

		});

		$(".Wishlist").click(function(e){
			e.preventDefault();
			
			$('#alert-wishlist').show();
			$('#alert-wishlist').html('added to wishlist succssfully');
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
			var id = $(this).attr("title");
			
			$.ajax({
			   type:'POST',
			   url: url+'/{{app()->getLocale()}}/homeWeb/wishList',
	
			   data:{ id:id },
	
			   success:function(data){
					// var newProduct = '<li id="'+data.id+'">' +
					// 						'<a href="" class="remove" title="Remove this item">' +
					// 							'<i class="fa fa-remove"></i>' +
					// 						'</a>' +
					// 						'<a class="cart-img" href="#"><img src="'+data.images[0]['src']+'" ></a>' +
					// 						'<h4><a href="#">'+data.name+'</a></h4>' +
					// 						'<p class="quantity"> <span class="amount">'+data.regular_price+'</span></p>' +
					// 				'</li>' ;

					// 		$(".shopping-list").append(newProduct);
	
				}	
			});
			
			// if(! $(this).hasClass('quick')){
				$(this).parent().find('.Wishlist').css('display','none');
			// }else{
				// $(this).removeClass('Wishlist');
				// $(this).addClass('Wishlist');
			// }
			$(this).parent().find('.Unlike').css('display','block');

			

			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)+Number(1));
	
		});

		$(".Unlike").click(function(e){
			e.preventDefault();
			e.preventDefault();
			$('#alert-wishlist').show();
			$('#alert-wishlist').html('removed from wishlist succssfully');
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
	
	
			var id = $(this).attr("title");
	
			$.ajax({
			   type:'POST',
			   url:url+'/{{app()->getLocale()}}/homeWeb/unLike',
	
			   data:{ id:id },

			   success:function(data){
				//    console.log(data);
				$('li#'+data).remove();
			}
	
			});
			
			$(this).parent().find('.Wishlist').css('display','block');
			$(this).parent().find('.Unlike').css('display','none');
			
			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)-Number(1));
		});
		if( !JSON.parse(localStorage.getItem("cart"))){
			var cart =[];
			localStorage.setItem('cart', JSON.stringify(cart));
		}
		var products= [];
		var local_storage=[];
		$(".cart").click(function(e){
			
			e.preventDefault();

			var id = $(this).attr("title");
			var unit=$(this).attr('unit');

			$.ajax({
			type:'POST',
			
			url:url+'/{{app()->getLocale()}}/homeWeb/add-to-cart',

			data:{ id:id },
			success:function(data){
				if(unit == 'kg'){
					data['qty']=0.1;
				}else{
					data['qty']=1;
				}
					var res=jQuery.inArray(id,localStorage);
					var key='product_'+data.id;
					var storage=localStorage['cart'];
					if(storage.includes(key)){
						$('#alert-cart').show();
						$('#alert-cart').html(data.name+' already in cart ');
						setTimeout(function() { 
								$('#alert-cart').fadeOut('fast'); 
						}, 2000);
					}else{
						addToCart(data);
						$('#alert-cart').show();
						$('#alert-cart').html('added to cart succssfully');
						setTimeout(function() { 
								$('#alert-cart').fadeOut('fast'); 
						}, 2000);
						$('.total-count').html('');
						var total=JSON.parse(localStorage.getItem("cart"));
						$('.total-count').html(JSON.parse(total.length));
					}
				}	
			});
			
		});



		function addToCart(product) {
			if (localStorage) {
				var cart;
				if (!localStorage['cart']) cart = [];
				else cart = JSON.parse(localStorage['cart']);            
				if (!(cart instanceof Array)) cart = [];
				var key ='product_'+product.id;
				var obj = {};
				obj[key] = product;
				cart.push(obj);
				localStorage.setItem('cart', JSON.stringify(cart));
			} 
		}


		function quickcart(elem,event){

		event.preventDefault();
		var id=elem.title;
		var qty= $(elem).parent().parent().find('.quantity').find('input').val();
		var storage=JSON.parse(localStorage.getItem("cart"));

		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro);
			if(key== 'product_'+id){
				storage.splice(i, 1); 
				var cart =[];
				localStorage.setItem('cart', JSON.stringify(cart));
				localStorage.setItem('cart', JSON.stringify(storage));
				localStorage.removeItem(key);
			}
		}

		
		$.ajax({
		type:'POST',

		url:url+'/{{app()->getLocale()}}/homeWeb/add-to-cart',

		data:{ id:id },
		success:function(data){
			
			data['qty']=qty;
			var res=jQuery.inArray(id,localStorage);
			var key='product_'+data.id;
			var storage=localStorage['cart'];
			if(storage.includes(key)){
				// console.log(JSON.parse(storage));

				$('#alert-cart').show();
				$('#alert-cart').html(data.name+' already in cart ');
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
			}else{
				
				addToCart(data);
				$('#alert-cart').show();
				$('#alert-cart').html('added to cart succssfully');
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
				$('.total-count').html('');
				var total=JSON.parse(localStorage.getItem("cart"));
				$('.total-count').html(total.length);

			}
		}
	});
	var modal=$(elem).closest(".modal").modal("hide");
	}


	// if($errors >= 1 && $errors->register){
	// 	alert('dsf')
	// 		// $(".nav-item a").each(function() {
	// 		// 	if($(this).attr("href") == '#panel8') {
	// 		// 		$('#active').removeClass('active');
	// 		// 		$(this).click();
	// 		// 	}
	// 		// });
	// 		$('#modalLRForm').modal('show');
	// }

	// if($errors >= 1 && $errors->register){
	// 	alert('dsf')
	// 		// $(".nav-item a").each(function() {
	// 		// 	if($(this).attr("href") == '#panel8') {
	// 		// 		$('#active').removeClass('active');
	// 		// 		$(this).click();
	// 		// 	}
	// 		// });
	// 		$('#modalLRForm').modal('show');
	// }

	// if( count($errors) >= 1 && $errors->login){
	// 		$('#modalLRForm').modal('show');
	// }
	// modal_error


				
</script>
@endpush