@extends('website.layouts.app')
<style>

	.contact .col-md-3{
        background: #C5171C;
        color: #fff;
		padding: 4%;
		border-top-left-radius: 0.5rem;
		border-bottom-left-radius: 0.5rem;
	}
	.contact-info{
		margin-top:10%;
	}
	.contact-info img{
		margin-bottom: 15%;
	}
	.contact-info h2{
		margin-bottom: 10%;
	}
	.contact .col-md-9{
		background: #fff;
		padding: 3%;
		border-top-right-radius: 0.5rem;
		border-bottom-right-radius: 0.5rem;
	}
	.contact-form label{
		font-weight:600;
	}
	.contact-form button{
		background: #25274d;
		color: #fff;
		font-weight: 600;
		/* width: 25%; */
	}
	.contact-form button:focus{
		box-shadow:none;
	}
</style>

@section('content')

    @if(\Session::has('success'))
         <p class="alert {{ \Session::get('alert-class', 'alert-info') }}">{{ \Session::get('success') }}</p>
    @endif
    

    <div class="container  " >
        <div class="contact my-3">
            <div class="row">
                <div class="col-md-3">
                    <div class="contact-info">
                        <img src="https://image.ibb.co/kUASdV/contact-image.png" alt="image"/>
                        <h2>Contact Us</h2>
                        <h4>We would love to hear from you !</h4>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="contact-form">
                        <form action="{{ url('profile/email') }}" method="POST" class="mb-5">
                            @csrf
                            <input type="text" name="contact" value="1" hidden>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="fname">First Name:</label>
                                <div class="col-sm-10">          
                                    <input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname" required>
                                </div>
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-2" for="lname">Last Name:</label>
                                <div class="col-sm-10">          
                                    <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lname" required>
                                </div>
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-2" for="email">Email:</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required>
                                </div>
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-2" for="comment">Comment:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" name="comment" id="comment" required></textarea>
                                </div>
                                </div>
                                <div class="form-group mb-5">        
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-red">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
@endsection

