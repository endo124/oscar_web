@extends('website.layouts.app')

<style>
.restaurant{
    margin: 30 auto;
}
.restaurant .image-group{
}
.restaurant h1{
    color: #EA0706;
    text-align: center;
    margin: 10px

}
</style>
@section('content')

    <div class="restaurant">
        <div class="container">
            <div class="row justify-content-center">
                    <img class="image-group" src="{{ asset('images/restaurant/resturantsGroup.gif') }}" alt="">
            </div>
            {{-- <h1>Opening Soon!</h1> --}}

            <div class="row justify-content-center mt-5" style="text-align: center;">

                @foreach ($resaurants as $resaurant)
                @php
                   $title= (array)$resaurant['title']
                @endphp
                <div class="col-md-3 col-sm-4 col-xs-6 mb-2">
                     <h4 >{{ $title['en'] }}</h4>
                    <img class="" src="{{$resaurant->image}}" alt="">
                </div>
                @endforeach
                
            </div>
        </div>
    </div>
@endsection
@push('scripts')
	<script type="text/javascript">


	</script>
@endpush
