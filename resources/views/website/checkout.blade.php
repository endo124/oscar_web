@extends('website.layouts.app')

@push('style')
	<style>
		

      
        .zone input{
            opacity: 1;
    width: 20px;
    height: 13px;
    margin-top: 2px
        }
        .zone label{
            margin-left: 25px;
            margin-bottom: 0
        }
		.button-5 {
			color: #C5171C !important
		}
		.button-5 :hover {
			color: #fff !important
        }
        .checktoggle {
        background-color: #e0001a;
        border-radius: 12px;
        cursor: pointer;
        display: block;
        font-size: 0;
        height: 24px;
        margin-bottom: 0;
        position: relative;
        width: 48px;
        }
        .checktoggle:after {
            content: ' ';
            display: block;
            position: absolute;
            top: 50%;
            left: 0;
            transform: translate(5px, -50%);
            width: 16px;
            height: 16px;
            background-color: #fff;
            border-radius: 50%;
            transition: left 300ms ease, transform 300ms ease;
        }
        .check:checked + .checktoggle {
            background-color: #55ce63;
        }
        .check:checked + .checktoggle:after {
            left: 100%;
            transform: translate(calc(-100% - 5px), -50%);
        }
        #map {
        height: 300px;
        width: 100%;
        }    
        .custom-map-control-button {
        appearance: button;
        background-color: #fff;
        border: 0;
        border-radius: 2px;
        box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
        cursor: pointer;
        margin: 10px;
        padding: 0 0.5em;
        height: 40px;
        font: 400 18px Roboto, Arial, sans-serif;
        overflow: hidden;
        }
        .custom-map-control-button:hover {
        background: #ebebeb;
        }
      .pay_method img{
          width: 100px;
          margin-right: 20px;
          cursor: pointer;
      }
      .pay_method img:active{
        box-shadow: -1px 1px 14px -4px rgba(0,0,0,0.75);
      }
      [type=radio] { 
        position: absolute;
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* IMAGE STYLES */
        [type=radio] + img {
        cursor: pointer;
        }

        /* CHECKED STYLES */
        [type=radio]:checked + img {
        outline: 2px solid #f00;
        }

        /* Preloader */
.preloader {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 999999999;
  width: 100%;
  height: 100%;
  background-color: #fff;
  overflow: hidden;
}
.preloader-inner {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%,-50%);
  -moz-transform: translate(-50%,-50%);
  transform: translate(-50%,-50%);
}
.preloader-icon {
  width: 100px;
  height: 100px;
  display: inline-block;
  padding: 0px;
}
.preloader-icon span {
  position: absolute;
  display: inline-block;
  width: 100px;
  height: 100px;
  border-radius: 100%;
  background:#C5171C;
  -webkit-animation: preloader-fx 1.6s linear infinite;
  animation: preloader-fx 1.6s linear infinite;
}
.preloader-icon span:last-child {
  animation-delay: -0.8s;
  -webkit-animation-delay: -0.8s;
}
@keyframes preloader-fx {
  0% {transform: scale(0, 0); opacity:0.5;}
  100% {transform: scale(1, 1); opacity:0;}
}
@-webkit-keyframes preloader-fx {
  0% {-webkit-transform: scale(0, 0); opacity:0.5;}
  100% {-webkit-transform: scale(1, 1); opacity:0;}
}
/* End Preloader */
	</style>
@endpush
@push('top_script')
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
@endpush
@section('content')
    <!-- Preloader -->
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <!-- End Preloader -->
    <!-- Start Checkout -->
		<section class="shop checkout section">
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-danger') }}"></p>
            @endif
			<div class="container">
                {{-- <div class="alert alert-danger" style="display:none"></div> --}}
                    <!-- Form -->
                    <form class="form" method="post" action="{{ url('homeWeb') }}">
                        @csrf
                        <input type="text" name="zone" value="" id="zoneflag" hidden>
                        <input type="text" name="delivery_date" value="" id="delivery_date" hidden>
                        <input type="text" name="checkout" value="0" id="checkout" hidden>
                        <div class="row"> 

                        <div class=" col-12">
                            <div class="checkout-form">
                                <h2>{{ __('lang.make_your_checkout_here')}}</h2>
                                
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-12">
                                            <div class="form-group">
                                                <label>{{ __('lang.username')}}<span>*</span></label>
                                                <input type="text" name="userName" placeholder="" value="{{ $customer->name }}" required="required">
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-6 col-md-6 col-12">
                                            <div class="form-group">
                                                <label>{{ __('lang.email')}}<span>*</span></label>
                                                <input type="email" name="Email" placeholder="" value="{{ $customer->email }}" required="required">
                                                <span class="error"hidden> </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12">
                                            <div class="form-group">
                                                <label>{{ __('lang.address')}} <span>*</span></label>
                                                {{-- <input name="address"  placeholder="Type Your Address"> --}}
                                                <select name="address" id="address" >
                                                    <option value="">select address</option>
                                                    @foreach ($addresses as $address)
                                                    @dump($address)
                                                        <option value="{{ $address->id }}">{{ $address->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12" id="city">
                                            <div class="form-group">
                                                <label>{{ __('lang.city')}}<span>*</span></label>
                                                <input type="text" name="city"   value="" required="required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12" id="name">
                                            <div class="form-group">
                                                <label>{{ __('lang.name')}}<span>*</span></label>
                                                <input type="text" name="address_name"   value="" required="required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12" id="area">
                                            <div class="form-group">
                                                <label>{{ __('lang.area')}}<span>*</span></label>
                                                <input type="text" name="area"   value="" required="required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12" id="phone" >
                                            <div class="form-group">
                                                <label>{{ __('lang.phone')}}<span>*</span></label>
                                                <input type="text" name="number"  value="" max="11" required="required">
                                            </div>
                                        </div> 
                                        <div class="col-lg-6 col-md-6 col-12" id="coordinates" >
                                            <div class="form-group">
                                                <input type="text" name="coordinates"  id="coor" value=""  hidden>
                                            </div>
                                        </div> 
                                        <div class="col-lg-12 col-md-6 col-12"  >
                                            <label>{{ __('lang.notes')}}</label>

                                            <div class="form-group">
                                                <textarea name="notes"  class="w-100" rows="4"></textarea>
                                            </div>
                                        </div> 
                                       
                                      
                                        {{-- <div class="col-lg-6 col-md-6 col-12" style="bottom: 35px;">
                                            <div class="status-toggle">
                                                <span>Express Shopping</span>
                                                <input type="checkbox" class="check" checked>
                                                <label for="status_1" class="checktoggle" style="display: inline-block"></label>
                                            </div>
                                        </div> --}}
                                    </div>
                        
                            </div>
                        </div>
                        <div id="map" ></div>
                    </form>
                </div>
                <!--/ End Form -->
                <div class="zone my-5" style="border:1px solid #C5171C;padding:15px;display:none">
                </div>  
                <div  id="payment">
                    <div class="order-details col-md-6 col-12" style="display: none ;margin:0 auto">
                        <!-- Order Widget -->
                        <div class="single-widget">
                            <h2>{{ __('lang.total')}}</h2>
                            <div class="content">
                                <ul>
                                    <li>{{ __('lang.select_pay_method')}}<span id="total_price"> </span></li>
                                </ul>
                            </div>
                        </div>
                        <!--/ End Order Widget -->
                        <!-- Button Widget -->
                        <div class="single-widget get-button">
                            <div class="content">
                                <div class="pay_method">
                                    <h5>{{ __('lang.cart_subtotal')}}</h5>
                                    <label>
                                        <input type="radio" name="pay" value="credit" >
                                        <img src="{{ asset('images/card.png') }}">
                                    </label>
                                    <label>
                                        <input  id="pay_cash" type="radio" name="pay" value="cash">
                                        <img src="{{ asset('images/cash.jpeg') }}">
                                    </label>
                     
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <input type="text" name="flag_checked" value="" id="flag_checked" hidden>
                <input type="text" name="cost_flag" value="" id="cost_flag" hidden>
            <div class="row my-5" style="justify-content: flex-end">
                <button class="button-5 float-right" id="next"  style="padding: 10px 20px"> {{ __('lang.next')}}</button>
            </div>

		</section>
		<!--/ End Checkout -->
@endsection

@push('scripts')
<script>
		let url='/html/oscar_web/public/';

var $loading = $('.preloader').hide();
$(document)
  .ajaxStart(function () {
    $loading.show();
  })
  .ajaxStop(function () {
    $loading.hide();
  });

let map, infoWindow;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 20,
  });
  infoWindow = new google.maps.InfoWindow();
  const locationButton = document.createElement("button");
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
        // //   infoWindow.setPosition(pos);
          infoWindow.open(map);
          map.setCenter(pos);
        var marker = new google.maps.Marker({
            position: pos,
            map: map,});
            var coordinates=(marker.getPosition().lng()).toString()+','+(marker.getPosition().lat()).toString();
            $('#coordinates').attr('value',coordinates);
            
            map.addListener("click", (mapsMouseEvent) => {
            // Close the current InfoWindow.
            marker.setMap(null);
            // Create a new InfoWindow.
            marker = new google.maps.Marker({
            position: mapsMouseEvent.latLng,
            map: map,});
       
            // console.log(marker.getPosition().lat())  ;          
            // console.log(marker.getPosition().lng())  ; 
            coordinates=(marker.getPosition().lng()).toString()+','+(marker.getPosition().lat()).toString();
            $('#coordinates').attr('value',coordinates);
            // $('#lat').attr('value',marker.getPosition().lat());
            
        });
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }

    
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}





        var total_price=0;
        var local_storage=localStorage['cart'];

        // for (var i = 0; i < localStorage.length; i++){


            
        // local_storage.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
        // var price=JSON.parse(localStorage.getItem(localStorage.key(i))).regular_price;
        // total_price= Number(price) + Number(total_price);
        // $("#total_price").html(total_price +" EGP");

        // }
        $( document ).ready(function() {
            var totalPrice=localStorage.getItem('total_price');
            $("#total_price").html(totalPrice);
            total_price=totalPrice;

        });
        // console.log(local_storage);
        $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

        });

        $(".button-5").click(function(e){
            var button_value=$(".button-5").attr('id');
            // if(button_value == 'next'){


                $(".zone").empty();
                e.preventDefault();
                
                var name = $("input[name=userName]").val();
                var email = $("input[name=Email]").val();
                var number = $("input[name=number]").val();
                var city = $("input[name=city]").val();
                var area = $("input[name=area]").val();
                var address = $("input[name=address]").val();
                var coordinates = $("input[name=coordinates]").val();
                var delivery_date=$("input[name=delivery_date]").val();
                var zone=$("input[name=zone]").val();
                var checkout=$("input[name=checkout]").val();
                var pay = $( 'input[name=pay]:checked').val();
                var flag=document.getElementById('flag_checked').value;
                var cost=document.getElementById('cost_flag').value;


                $.ajax({
                    type:'POST',
                    url: url+'/{{app()->getLocale()}}/homeWeb',

                    data:{
                            local_storage: local_storage,
                            total_price: total_price,
                            name:name,
                            email:email,
                            number:number,
                            city:city,
                            area:area,
                            address:address,
                            pay:pay,
                            coordinates:coordinates,
                            delivery_date:delivery_date,
                            zone:zone,
                            checkout:checkout,
                            flag:flag,
                            cost:cost
                        },

                    success:function(data){
                        var checkout=$("input[name=checkout]").val();
                        $('.alert-danger').empty();
                        if(data.errors){
                            $('.alert-danger').empty();
                            
                        jQuery.each(data.errors, function(key, value){
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<p>'+value+'</p>');
                            });
                            
                        }
                        else if($("input[name=checkout]").val() == '0'){
                            $('.alert-danger').empty();
                            jQuery('.alert-danger').hide();

                            $('#payment').css('display','none');

                            $('.form').hide();
                            var data =data.data;
                            for (let i = 0; i < data.length; i++) {
                                
                                var flag=   '<div class="my-5 flag'+i+'"  >'+
                                                '<input type="radio" id="flag'+i+'" name="flag" value="'+data[i].flag+'" onclick="flaag()" checked required>'+
                                                '<label for='+data[i].flag+'>'+data[i].flag+'  <span>'+data[i].cost+' EGP</span></label>'+
                                                '<input type="text"  name="cost" value="'+data[i].cost+'" "  hidden>'+
                                            '</div>';
                                $('.zone').append(flag);
                                
                            }
                            $('.zone').css('display','block');

                            var schedule ='<div class="schedule">'+
                                            '<input class="form-control" id="date" value="" onchange="replace()" type="datetime-local" style="width:40%;height:auto" selected>'+
                                        '</div>';
                            $('.zone').append(schedule);  
                            $('#zoneflag').val('1');
                            $("input[name=checkout]").val('1');
                        }
                        else{
                            $("input[name=checkout]").val('1');
                            $('#zoneflag').val('1');
                            $('.button-5').text('{{ __('lang.check_out')}}');
                            $('.button-5').attr('id','checkout');
                            $('.zone').css('display','none');
                            $('.order-details').css( 'display', 'block');
                            // $('.form').css( 'display', 'block');
                            // $('.form').css( 'visibility', 'hidden');
                            $('#payment').css('display','block');
                            $("#pay_cash").attr('checked', 'checked');


                        }
                        if (data.redirect) {
                            var cart =[];
		                    localStorage.setItem('cart', JSON.stringify(cart));
                            window.location.href = data.redirect;
                        }  
                       
                    }
                    ,error: function (data) {

                        $('.alert').html(JSON.parse(data.responseText).message);
                        $('.alert').css('display','block');
                        setTimeout(function() { 
								$('.alert').fadeOut('fast'); 
						}, 2000);
                    }	
                });
            // }else{

            //     window.location.href='/homeWeb'
            // }
     
            


    });
        
        function flaag() {
            // alert('fdg');
            var elem=$('input[name="flag"]:checked').val()
            if ( elem!= 'Scheduled') {
                $('.schedule').css('display','none');
                document.getElementById('flag_checked').value=elem;
                var cost=$('input[name="flag"]:checked').parent().find('input[name="cost"]').val();
                $('#cost_flag').val(cost);


            }else if (elem == 'Scheduled') {
                $('.schedule').css('display','block');
                document.getElementById('flag_checked').value=elem;
                var cost=$('input[name="flag"]:checked').parent().find('input[name="cost"]').val();
                $('#cost_flag').val(cost);
            }
            var totalPrice=localStorage.getItem('total_price');
            var totalWithDelivery=parseFloat(totalPrice)+ parseFloat(cost);
            // console.log(parseFloat(totalPrice)+ parseFloat(cost),parseFloat(totalPrice),Number(cost),cost,totalPrice);
            total_price=totalWithDelivery;
            $("#total_price").html(totalWithDelivery +'  EGP');

        }

        function replace(){
            $('#delivery_date').val($('#date').val());
        }
      



        document.getElementById('map').style.display="none"
        document.getElementById('city').style.display="none"
        document.getElementById('name').style.display="none"
        document.getElementById('area').style.display="none"
        document.getElementById('phone').style.display="none"
        $("#address").change(function(){
            var selected = $(this).children("option:selected").val();
            var id = { id : selected }
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
   
    var getData = $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type: 'POST',
            url:url +'/{{app()->getLocale()}}/address/getaddress',
            data: id,
            dataType : 'json',
        
        
            success: function(resultData) { 
                $('input[name="city"]').val(resultData.city)
                $('input[name="address_name"]').val(resultData.name)
                $('input[name="city"]').val(resultData.city)
                $('input[name="number"]').val(resultData.phone)
                $('input[name="area"]').val(resultData.area)
                $('input[name="coordinates"]').val(resultData.coordinates)
            
                document.getElementById('city').style.display="block"
                document.getElementById('name').style.display="block"
                document.getElementById('area').style.display="block"
                document.getElementById('phone').style.display="block"

                document.getElementById('map').style.display="none"

                coordinate=String(resultData.coordinates);
                let res = coordinate.split(",");
                initialize(parseFloat(res[1]),parseFloat(res[0]));


            }

            
        });
    });

    function initialize(lat,lng) {
    document.getElementById('map').style.display="block"

         map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: lat, lng: lng },
        zoom: 18,
    });
  infoWindow = new google.maps.InfoWindow();
  const locationButton = document.createElement("button");
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          var pos = {
            lat: lat,
            lng:lng ,
          };
        // //   infoWindow.setPosition(pos);
          infoWindow.open(map);
          map.setCenter(pos);
        var marker = new google.maps.Marker({
            position: pos,
            map: map,});
            var coordinates=(marker.getPosition().lng()).toString()+','+(marker.getPosition().lat()).toString();
            $('#coordinates').attr('value',coordinates);

            map.addListener("click", (mapsMouseEvent) => {

            // Close the current InfoWindow.
            marker.setMap(null);
            // Create a new InfoWindow.
            marker = new google.maps.Marker({
            position: mapsMouseEvent.latLng,
            map: map,});

          
            coordinates=(marker.getPosition().lng()).toString()+','+(marker.getPosition().lat()).toString();
            $('#coor').attr('value',' ');
            $('#coor').attr('value',coordinates);

            // $('#lat').attr('value',marker.getPosition().lat());

        });
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }


}

function initMap() {
         map = new google.maps.Map(document.getElementById("map"), {
           center: { lat: 30.0595581, lng: 31.223445 },
           zoom: 6,
         });
         infoWindow = new google.maps.InfoWindow();
         const pos = '30.0595581,31.223445';
         map.controls[google.maps.ControlPosition.TOP_CENTER].push(
           pos
         );
         var marker = new google.maps.Marker({
             position: pos,
             map: map,});
             var coordinates='30.0595581,31.223445';
             $('#coordinates').attr('value',coordinates);
 
             map.addListener("click", (mapsMouseEvent) => {
             // Close the current InfoWindow.
             marker.setMap(null);
             // Create a new InfoWindow.
             marker = new google.maps.Marker({
             position: mapsMouseEvent.latLng,
             map: map,});
 
             // console.log(marker.getPosition().lat())  ;
             // console.log(marker.getPosition().lng())  ;
             coordinates=(marker.getPosition().lng()).toString()+','+(marker.getPosition().lat()).toString();
             $('#coordinates').attr('value',coordinates);
             // $('#lat').attr('value',marker.getPosition().lat());
 
         });
       };
              
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  initMap();
  infoWindow.open(map);
}
  </script>


@endpush