<table class="table table-responsive" id="onBoardingValues-table">
    <thead>
        <tr>
            <th>Question Id</th>
        <th>Value</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($onBoardingValues as $onBoardingValue)
        <tr>
            <td>{!! $onBoardingValue->question_id !!}</td>
            <td>{!! $onBoardingValue->value !!}</td>
            <td>
                {!! Form::open(['route' => ['onBoardingValues.destroy', $onBoardingValue->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('onBoardingValues.show', [$onBoardingValue->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('onBoardingValues.edit', [$onBoardingValue->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>