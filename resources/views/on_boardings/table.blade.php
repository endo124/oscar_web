<table class="table table-responsive" id="onBoardings-table">
	<thead>
		<tr>
			<th>Order</th>
		<th>Question</th>
		<th>Answers</th>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($onBoardings as $onBoarding)
		<tr>
			<td>{!! $onBoarding->order !!}</td>
			<td>{!! $onBoarding->question !!}</td>
			<td class="limit-column">{!! $onBoarding->answers !!}</td>
			<td>
				{!! Form::open(['route' => ['onBoardings.destroy', $onBoarding->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('onBoardings.show', [$onBoarding->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('onBoardings.edit', [$onBoarding->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
					{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
@section('css')
	<link rel="stylesheet" href="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.css" />
@endsection
@section('scripts')
<script src="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.js"></script>
<script>
	var options = {
		collapsed: true,
		withQuotes: true
	};
	$('.limit-column').each(function(i, e) {
		try {
			var input = eval('(' + $(e).html() + ')');
			$(e).jsonViewer(input, options);
		} catch (err) {
			var tmp = $(e).html();
			var i = 0;
			if((i = tmp.split('.').length) > 1) {
				if(['jpg', 'png'].indexOf(tmp.split('.')[i-1].toLowerCase()) != -1) {
					$(e).html('<img src="'+$(e).html()+'" width=50 />');
				}
			}
			//console.error(err);
			//console.log($(e).html());
		}
	})
</script>
@endsection