<table class="table table-responsive" id="promoVideos-table">
	<thead>
		<tr>
			<th>Title</th>
			<th>Url</th>
			<th>Is Active</th>
			<th>Views</th>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($promoVideos as $promoVideo)
		<tr>
			<td>{!! $promoVideo->title !!}</td>
			<td>{!! $promoVideo->url !!}</td>
			<td>
				{!! Form::model($promoVideo, ['route' => ['promoVideos.activate', $promoVideo->id], 'method' => 'post']) !!}
				{!! Form::button('<i class="glyphicon glyphicon-film"></i>', ['type' => 'submit', 'class' => $promoVideo->is_active? 'btn btn-primary btn-xs' : 'btn btn-default btn-xs']) !!}
				{!! Form::close() !!}
			</td>
			<td>{!! $promoVideo->views !!}</td>
			<td>
				{!! Form::open(['route' => ['promoVideos.destroy', $promoVideo->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('promoVideos.show', [$promoVideo->id]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('promoVideos.edit', [$promoVideo->id]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-edit"></i></a>
					{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger
					btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
