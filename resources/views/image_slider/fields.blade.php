<div class="col-md-6">
	<!-- Title Field -->



	<!-- Image Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('image', 'Image:') !!}
		{!! Form::file('image', ['class' => 'form-control','accept' => 'image/png,image/jpeg']) !!}
	</div>


		<!-- Message Field -->
        <div class="form-group col-sm-12">
		{!! Form::label('Order', 'Order:') !!}
		{!! Form::number('order', isset($image)? $image->order : null, ['class' => 'form-control']) !!}
	</div>

	

	<!-- Submit Field -->
	<div class="form-group col-sm-12">
		{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
		<a href="{!! route('imageSlider.index') !!}" class="btn btn-default">Cancel</a>
	</div>
</div>




@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<link rel="stylesheet"
  href="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/css/bootstrap-datetimepicker.min.css">
<script src="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/js/bootstrap-datetimepicker.min.js"></script>

<script>

  $(function() {
	$('#datetimepicker1').datetimepicker({
    defaultDate: new Date(),
    format: 'YYYY-MM-DD H:mm:ss',
    sideBySide: true
});
  });
  </script>
@stop