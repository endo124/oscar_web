@extends('layouts.app')

@section('content')
<section class="content-header">
	<h1 class="pull-left">Images</h1>
  <h1 class="pull-right">
		<a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px "
			href="{!! route('imageSlider.create') !!}">Add New</a>
	
	</h1>
</section>
<div class="content">
	<div class="clearfix"></div>

	@include('flash::message')

	<div class="clearfix"></div>
	<div class="box box-primary">
		<div class="box-body">
			<table class="table table-responsive" id="products-table">
				<thead>
					<tr>
						<th>image</th>
						<th>order</th>
						<th colspan="3">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($images as $image)
					<tr>
						<td>
						<img src="{!! $image->image !!}"  height="100"/>
						</td>
						<td>{!! $image->order !!}</td>
						<td>
						{!! Form::open(['route' => ['imageSlider.destroy', $image->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('imageSlider.edit', [$image->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
						</td>
				
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="text-center">

	</div>
</div>
@endsection