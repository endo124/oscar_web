<div class="col-md-6">
	<!-- Name Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('name[en]', 'Name:') !!}
		{!! Form::text('name[en]', null, ['class' => 'form-control']) !!}
	</div>

	<!-- Slug Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('slug', 'Slug:') !!}
		{!! Form::text('slug', null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group col-sm-12">
		{!! Form::label('parent', 'Parent:') !!}
		{!! Form::select('parent', ['' => 'Select Parent Category']+$categories , null, ['class' => 'form-control select2 ','id'=>'role_id']) !!}
	</div>

	<!-- Image Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('image', 'Image:') !!}
		{!! Form::file('image', ['class' => 'form-control', 'accept' => 'image/png,image/jpeg']) !!}
	</div>

	<!-- Description Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('description[en]', 'Description:') !!}
		{!! Form::textarea('description[en]', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="col-md-6">
		<!-- Name Field -->
		<div class="form-group col-sm-12">
			{!! Form::label('name[ar]', 'الاسم:', ['class' => 'label-ar']) !!}
			{!! Form::text('name[ar]', null, ['class' => 'form-control label-ar']) !!}
		</div>
		<!-- Description Field -->
		<div class="form-group col-sm-12">
			{!! Form::label('description[ar]', 'الوصف:', ['class' => 'label-ar']) !!}
			{!! Form::textarea('description[ar]', null, ['class' => 'form-control input-ar']) !!}
		</div>
		
		<?php 
	
		$sign=\App\Models\Category::where('slug', $productCategory->slug.'-signature')->first();
		?>
		@if(!strpos($productCategory->slug, '-signature'))
		@if( is_null($sign))
		<div class="form-group col-sm-12">
		<label for="Signatures">
			{!! Form::checkbox('signature', 'true',  []) !!}
			Signature
		</label>
	</div>
	
	@else
	<div class="form-group col-sm-12">
		<label for="Signatures">
			{!! Form::checkbox('signature', 'true',  'false') !!}
			Signature
		</label>
	</div>
	@endif
	@endif
</div>

<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('productCategories.index') !!}" class="btn btn-default">Cancel</a>
</div>
@section('scripts')
    @parent


    <script>

		$(document).ready(function() {

$('.select2').select2();
		    })    </script>
 @endsection