<div class="col-md-6">
	<!-- Title Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('title[rendered][en]', 'Title:') !!}
		{!! Form::text('title[rendered][en]', null, ['class' => 'form-control']) !!}
	</div>

	<!-- Contect Field -->
	<div class="form-group col-sm-12 col-lg-12">
		{!! Form::label('content[rendered][en]', 'Content:') !!}
		{!! Form::textarea('content[rendered][en]', null, ['class' => 'form-control']) !!}
	</div>

	<!-- Slug Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('slug', 'Slug:') !!}
		{!! Form::text('slug', null, ['class' => 'form-control']) !!}
	</div>

	<!-- Better Featured Image Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('better_featured_image', 'Image:') !!}
		{!! Form::file('better_featured_image', ['class' => 'form-control', 'accept' => 'image/png,image/jpeg']) !!}
	</div>
	<div class="clearfix"></div>

	<!-- Submit Field -->
	<div class="form-group col-sm-12">
		{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
		<a href="{!! route('kitchens.index') !!}" class="btn btn-default">Cancel</a>
	</div>
</div>
<div class="col-md-6">
	<!-- Title Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('title[rendered][ar]', 'الاسم:', ['class' => 'label-ar']) !!}
		{!! Form::text('title[rendered][ar]', null, ['class' => 'form-control input-ar']) !!}
	</div>
	<!-- Contect Field -->
	<div class="form-group col-sm-12 col-lg-12">
		{!! Form::label('content[rendered][ar]', 'المحتوي:', ['class' => 'label-ar']) !!}
		{!! Form::textarea('content[rendered][ar]', null, ['class' => 'form-control input-ar']) !!}
	</div>
</div>