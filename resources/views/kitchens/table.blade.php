<table class="table table-responsive" id="kitchens-table">
	<thead>
		<tr>
			<th>Image</th>
			<th>Title</th>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($kitchens as $kitchen)
		<tr>
			<td><img src="{!! $kitchen->better_featured_image !!}" height="50"></td>
			<td>{!! $kitchen->title->rendered->en !!}</td>
			<td>
				{!! Form::open(['route' => ['kitchens.destroy', $kitchen->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('kitchens.show', [$kitchen->id]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('kitchens.edit', [$kitchen->id]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-edit"></i></a>
					{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger
					btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>