<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $kitchen->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $kitchen->gettitle() !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $kitchen->slug !!}</p>
</div>

<!-- Contect Field -->
<div class="form-group">
    {!! Form::label('contect', 'Contect:') !!}
    <p>{!! $kitchen->contect !!}</p>
</div>

<!-- Better Featured Image Field -->
<div class="form-group">
    {!! Form::label('better_featured_image', 'Better Featured Image:') !!}
    <p>{!! $kitchen->better_featured_image !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $kitchen->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $kitchen->updated_at !!}</p>
</div>

