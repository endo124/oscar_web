<!-- resources/views/chat.blade.php -->

@extends('layouts.app_chat')

@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="container-fluid"  id="app">
    <div class="row justify-content-center">
    <user-new-component ></user-new-component>

    <chat-component></chat-component>
   
    <user_info-component></user_info-component>       
        
    </div>
</div>
<script>
   window.localStorage.setItem("token", '{!! Auth::guard('customers')->user()->token !!}');
   window.localStorage.setItem("id", '{!! Auth::guard('customers')->user()->id !!}');
   window.localStorage.setItem("user", '{!! Auth::guard('customers')->user() !!}');
   window.localStorage.setItem("user-name", '{!! Auth::guard('customers')->user()->name !!}');

</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>

@endsection