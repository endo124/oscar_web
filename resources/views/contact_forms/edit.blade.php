@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Contact Form
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($contactForm, ['route' => ['contactForms.update', $contactForm->id], 'method' => 'patch']) !!}

                        @include('contact_forms.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection