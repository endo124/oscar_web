<div class="col-md-6">
	<!-- Name Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('id', 'ID:') !!}
		{!! Form::text('id', isset($product)? $product['id'] : null,['required'], ['class' => 'form-control']) !!}
	</div>
	<div class="form-group col-sm-12">
		{!! Form::label('name', 'Name:') !!}
		{!! Form::text('name', isset($product)? $product['name'] : null, ['required'],['class' => 'form-control']) !!}
	</div>

	<!-- Image Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('image', 'Image:') !!}
		<input type="file" class="form-control" name="image" id="image" >
	</div>
	<div class="form-group col-sm-12">
		{!! Form::label('barcode', 'Barcode:') !!}
		{!! Form::text('barcode', isset($product)? $product['barcode'] : null, ['required'],['class' => 'form-control']) !!}
	</div>

	<div class="form-group col-sm-12">
		<label for="Signatures">
			{!! Form::checkbox('signature', 'true', '5637169327' ==$product['category_id']? true :
			false,  []) !!}
			Signature
		</label>
	</div>
	<!-- Hot price Field -->
	<div class="form-group col-sm-12">
		<label for="hot_price">
			{!! Form::checkbox('hot_price', 'true', isset($product) && isset($product['hot_price'])? $product['hot_price'] :
			false, []) !!}
			Hot Price
		</label>
	</div>
	<div class="form-group col-sm-12">
		<label for="in_stock">
			{!! Form::checkbox('in_stock', 'true', isset($product) && isset($product['in_stock'])? $product['in_stock'] :
			false, []) !!}
			In Stock
		</label>
	</div>
	<!-- Regular price Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('sale_price', 'Sale Price:') !!}
		{!! Form::text('sale_price', isset($product)? $product['sale_price'] : null, ['class' => 'form-control']) !!}
	</div>
	<!-- Regular price Field -->
	<div class="form-group col-sm-6">
		{!! Form::label('regular_price', 'Regular Price:') !!}
		{!! Form::text('regular_price', isset($product)? $product['regular_price'] : null,['class' => 'form-control']) !!}
	</div>
	<div class="form-group col-sm-6">
		{!! Form::label('limit', 'Limit:') !!}
		{!! Form::text('limit', isset($product)? $product['limit'] : null,['class' => 'form-control']) !!}
	</div>
	<div class="form-group col-sm-12">
		{!! Form::label('categories', 'Categories:') !!}
		{!! Form::select('categories[]',$categories, isset($product)? json_encode($product['categories']) : null, ['required'],['class' =>
		'form-control
		select2','id'=>'categories','multiple' => true]) !!}
	</div>
	<!-- Description Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('description', 'Description:') !!}
		<textarea name="description" class="form-control">
			@if(isset($product))
				{!! $product['description'] !!}
			@endif
		</textarea>
	</div>

	<!-- Submit Field -->
	<div class="form-group col-sm-12">
		{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
		<a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
	</div>
</div>

<div class="col-md-6">
	<!-- Name Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('name_ar', 'الاسم:', ['class' => 'label-ar']) !!}
		{!! Form::text('name_ar', (isset($product) && isset($product['name_ar']))? $product['name_ar'] : null, ['class' =>
		'form-control input-ar']) !!}
	</div>
	<!-- Description Field -->
	<div class="form-group col-sm-12">
		{!! Form::label('description_ar', 'الوصف:', ['class' => 'label-ar']) !!}
		<textarea name="description_ar" class="form-control input-ar">
			@if(isset($product) && isset($product['description_ar']))
				{!! $product['description_ar'] !!}
			@endif
		</textarea>
	</div>
</div>

@section('scripts')
<script>
	tinymce.init({ selector:'textarea' });
</script>
@stop