<div class="col-md-6">
	<!-- Id Field -->
	<div class="form-group">
		{!! Form::label('id', 'Id:') !!}
		<p>{!! $product['id'] !!}</p>
	</div>

	<!-- Name Field -->
	<div class="form-group">
		{!! Form::label('name', 'Name:') !!}
		<p>{!! $product['name'] !!}</p>
	</div>

	<!-- Description Field -->
	<div class="form-group">
		{!! Form::label('description', 'Description:') !!}
		<p>{!! $product['description'] !!}</p>
	</div>
</div>
@for($i=0;$i<count($product['images']);$i++)
<div class="col-md-6">
	<img src="{!! $product['images'][$i]->src !!}" class="img-responsive" />
</div>
@endfor
