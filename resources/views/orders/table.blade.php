<table class="table table-responsive" >
	<thead>
		<tr>
			<th>Order Number</th>
			<th>Customer Phone</th>
            <th>Customer Account</th>
            <th>Order Status</th>
			
		</tr>
	</thead>
	<tbody>
		@foreach($orders as $order)
		<tr>
			<!-- <td>
				@if(isset($product['images']) && is_array($product['images']))
				<img src="{!! $product['images'][0]['src'] !!}" alt="" height="100">
				@endif
            </td> -->
            <td>{{$order->OrderNumber}}</td>
			<td>{{$order->CustomerPhone}}</td>
			<td>{{$order->CustomerAccount}}</td>
			
           @if($order->order_status == 'open')
            <td>
				<input id="toggle-event-{{$order->id}}" type="checkbox" data-toggle="toggle" checked onchange='Toggle( {{$order->id}})' data-on="Open" data-off="Close">				
			</td>
			@elseif($order->order_status == 'close')
			<td>
				<input id="toggle-event-{{$order->id}}" type="checkbox" data-toggle="toggle"  onchange='Toggle( {{$order->id}})' data-on="Open" data-off="Close">				
			</td>
			@else
			<td>
				<input id="toggle-event-{{$order->id}}" type="checkbox" data-toggle="toggle"  checked onchange='Toggle( {{$order->id}})' data-on="Open" data-off="Close">				
			</td>
			@endif


		</tr>
		@endforeach
	</tbody>
</table>