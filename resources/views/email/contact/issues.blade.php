@component('mail::message')

@if (isset($data['data']['order']))
    @if (!is_null($data['data']['order']))

    Order Number : {{ $data['data']['order'] }}
    <br>
    Message info : {{ $data['data']['order-message'] }}
    @elseif(!is_null($data['data']['driver']))

    Driver name : {{ $data['data']['driver'] }}
    <br>
    Message info : {{ $data['data']['driver-message'] }}
    @else
    Message info : {{ $data['data']['other-message'] }}

    @endif
@else
<div>
    <span>Customer Name:</span><span>{{ $data['data']['fname'] ?? Auth::guard('customerForWeb')->user()->name }}  {{ $data['data']['lname'] ??' '  }} </span>
    <p>{{ $data['data']['email']  ??  Auth::guard('customerForWeb')->user()->email}}</p>
    <p>{{ $data['data']['other-message'] ?? ' ' }}</p>
</div>


@endif



Thanks,<br>
Oscar
@endcomponent
