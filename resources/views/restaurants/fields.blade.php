<!-- Title Field -->
<div class="form-group col-sm-6">
	{!! Form::label('title[en]', 'Title:') !!} {!! Form::text('title[en]', null, ['class' => 'form-control']) !!}
</div>
<!-- Title Field -->
<div class="form-group col-sm-6">
	{!! Form::label('title[ar]', 'الاسم:', ['class' => 'label-ar']) !!}
	{!! Form::text('title[ar]', null, ['class' => 'form-control input-ar']) !!}
</div>
<!-- Image Field -->
<div class="form-group col-sm-6">
	{!! Form::label('image', 'Image:') !!} {!! Form::file('image') !!}
</div>
<!-- Menu Field -->
<div class="form-group col-sm-6">
	{!! Form::label('menu', 'Menu:') !!} {!! Form::file('menu') !!}
</div>
<div class="clearfix"></div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('restaurants.index') !!}" class="btn btn-default">Cancel</a>
</div>