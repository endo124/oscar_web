@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Page Type
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pageType, ['route' => ['pageTypes.update', $pageType->id], 'method' => 'patch']) !!}

                        @include('page_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection