<!-- Title Field -->
<div class="form-group col-sm-6">
	{!! Form::label('title[en]', 'Title:') !!}
	{!! Form::text('title[en]', null, ['required'],['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
	{!! Form::label('title[ar]', 'الاسم:', ['class' => 'label-ar']) !!}
	{!! Form::text('title[ar]', null,['required'], ['class' => 'form-control input-ar']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
	{!! Form::label('image', 'Image:') !!}
	<input type="file" class="form-control" name="image" id="image" >

</div>
<div class="clearfix"></div>


<div class="form-group col-sm-6">
	{!! Form::label('store id', 'Store Id:') !!}
	{!! Form::text('store_id', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
	{!! Form::label('latitudes', 'latitudes:') !!}
	{!! Form::text('latitudes', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
	{!! Form::label('longitudes', 'Longitudes:') !!}
	{!! Form::text('longitudes', null, ['class' => 'form-control']) !!}
</div>
<!-- Location Field -->
<div class="form-group col-sm-6">
	{!! Form::label('location', 'Location:') !!}
	{!! Form::text('location', null, ['class' => 'form-control']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('branches.index') !!}" class="btn btn-default">Cancel</a>
</div>