<?php

namespace App\Http\Middleware;

use Closure;

class CheckCustomerVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      
        if (!auth('customers')->user()->verified) {
            return response()->json(['error' => 'Not Verified'], 401);

        }
        return $next($request);
    }
}
