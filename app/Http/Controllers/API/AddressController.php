<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Customer;
use App\Models\Address;
use Illuminate\Support\Facades\Validator;


class AddressController extends AppBaseController
{
	//

	public function add(Request $request)
	{

		$customer = auth('customers')->user();
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'address' => 'required',
			'area' => 'required',
			'city' => 'required',
			'phone' => 'required',
			'area' => 'required',
			'default' => 'required',
			'coordinates'=>'required',

		]);
		if ($validator->fails()) {
			return response()->json($validator->errors());
		}
		$address = Address::create([
			'name' => $request->get('name'),
			'address' => $request->get('address'),
			'area' => $request->get('area'),
			'city' => $request->get('city'),
			'phone' => $request->get('phone'),
			'customer_id' => $customer->id,
			'coordinates'=>$request->coordinates
		]);
		if ($request->get('default') == 1) {
			$customer->update(['default_address' => $address->id]);
		}
		return response()->json([
			'success' => true,
			'message' => 'Address Created successfully.',
			'data' => $address



		], 200);
	}

	public function update(Request $request, $id)
	{

		$customer = auth('customers')->user();
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'address' => 'required',
			'area' => 'required',
			'city' => 'required',
			'phone' => 'required',
			'area' => 'required',
			'default' => 'required',

		]);
		if ($validator->fails()) {
			return response()->json($validator->errors());
		}
		$address = Address::where('id', $id)->update([
			'name' => $request->get('name'),
			'address' => $request->get('address'),
			'area' => $request->get('area'),
			'city' => $request->get('city'),
			'phone' => $request->get('phone'),
			'customer_id' => $customer->id,
		]);
		if ($request->get('default') == 1) {
			$customer->update(['default_address' => $id]);
		}
		return response()->json([
			'success' => true,
			'message' => 'Address Updated successfully.',
		], 200);
	}

	public function delete($id)
	{
		$customer = auth('customers')->user();
		$address = Address::find($id);
		if (is_null($address)) {
			return response()->json([
				'error' => true,
				'message' => 'Address Not Found.',
			], 500);
		} else {
			Address::where('id', $id)->delete();
			$customer->where('default_address', $id)->update(['default_address' => null]);
			return response()->json([
				'success' => true,
				'message' => 'Address Deleted successfully.',
			], 200);
		}
	}

	public function get()
	{
		$customer = auth('customers')->user();
		$address = $customer->address;

		return response()->json([
			'success' => true,
			'message' => 'Get Address successfully.',
			'data' => $address,
		], 200);
	}


}
