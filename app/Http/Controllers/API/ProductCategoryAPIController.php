<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Repositories\CategoryRepository;
class ProductCategoryAPIController extends AppBaseController
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->middleware('auth');
        $this->categoryRepository = $categoryRepo;
    }

	public function index(Request $request)
	{
        $categories = $this->categoryRepository->all();

		$categories = [
			[
				"id" => 1,
				"name" => "Oscar Signature Specialties",
				"slug" => "signature",
				"parent" => 0,
				"description" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
					"title" => "signature",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 11,
				"name" => "Dairy",
				"slug" => "dairy",
				"parent" => 1,
				"descrirption" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
					"title" => "signature",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 12,
				"name" => "Cocoaccino",
				"slug" => "cocoaccino",
				"parent" => 1,
				"descrirption" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
					"title" => "signature",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 13,
				"name" => "Marjaayoun",
				"slug" => "marjaayoun",
				"parent" => 1,
				"descrirption" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
					"title" => "signature",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 14,
				"name" => "Make Your Own Sandwich",
				"slug" => "Make Your Own sandwich",
				"parent" => 1,
				"descrirption" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
					"title" => "signature",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 15,
				"name" => "Fresh Juices",
				"slug" => "Fresh juices",
				"parent" => 1,
				"descrirption" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
					"title" => "signature",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 16,
				"name" => "Salads/Dips",
				"slug" => "Salads/dips",
				"parent" => 1,
				"descrirption" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Oscar Signature Specialties.jpg",
					"title" => "signature",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 2,
				"name" => "Oscar Bakery",
				"slug" => "bakery",
				"parent" => 0,
				"description" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/2 Oscar Bakery.jpg",
					"title" => "Oscar Bakery",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 3,
				"name" => "Fresh Food",
				"slug" => "fresh",
				"parent" => 0,
				"description" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/3 Fresh Food.jpg",
					"title" => "Fresh Food",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 30,
				"name" => "Fruits & Vegetables",
				"parent" => 3
			],
			[
				"id" => 31,
				"name" => "Butchery",
				"parent" => 3
			],
			[
				"id" => 32,
				"name" => "Imported Meat",
				"parent" => 3
			],
			[
				"id" => 33,
				"name" => "Poultry",
				"parent" => 3
			],
			[
				"id" => 34,
				"name" => "Sea-Food",
				"parent" => 3
			],
			[
				"id" => 4,
				"name" => "Deli",
				"slug" => "men",
				"parent" => 0,
				"description" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Deli.jpg",
					"title" => "man",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 40,
				"name" => "Cheese",
				"parent" => 4
			],
			[
				"id" => 41,
				"name" => "Cold Cuts",
				"parent" => 4
			],
			[
				"id" => 42,
				"name" => "Pickles",
				"parent" => 4
			],
			[
				"id" => 5,
				"name" => "Frozen",
				"slug" => "frozen",
				"parent" => 0,
				"description" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/5 Frozen.jpg",
					"title" => "Frozen",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 6,
				"name" => "Groceries",
				"slug" => "groceries",
				"parent" => 0,
				"description" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Groceries.jpg",
					"title" => "Groceries",
					"alt" => ""
				],
				"menu_order" => 0,
				"count" => 8,
			],
			[
				"id" => 7,
				"name" => "Detergents and Cleaning Supplies",
				"slug" => "detergents",
				"parent" => 0,
				"description" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Detergents and Cleaning Supplies.jpg",
					"title" => "Detergents",
					"alt" => ""
				],
				"menu_order" => 0,
				"count" => 8,
			],
			[
				"id" => 8,
				"name" => "Health & Beauty",
				"slug" => "health",
				"parent" => 0,
				"description" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Health & Beauty.jpg",
					"title" => "Health",
					"alt" => ""
				],
				"menu_order" => 0,
				"count" => 4,
			],
			[
				"id" => 9,
				"name" => "Oscar Home",
				"slug" => "men",
				"parent" => 0,
				"description" => "",
				"display" => "default",
				"image" => [
					"src" => "http://oscar.momentum-sol.com/storage/media/Oscar-Home.jpg",
					"title" => "man",
					"alt" => ""
				],
				"count" => 54
			],
			[
				"id" => 90,
				"name" => "Light Household",
				"parent" => 9
			],
			[
				"id" => 91,
				"name" => "Heavy Household",
				"parent" => 9
			],
		];
		return $this->sendResponse($categories->toArray(), 'Categories retrieved successfully');
	}

	public function stores(Request $request)
	{
		$stores = [
			[
				'id' => 1,
				'title' => 'North 90 Street',
				'image' => 'http://www.oscarstores.com/images+/about1.gif',
				'location' => 'https://goo.gl/maps/2nvm9UWVEM8B6Wp99'
			],
			[
				'id' => 2,
				'title' => 'North 90 Street',
				'image' => 'http://www.oscarstores.com/images+/about1.gif',
				'location' => 'https://goo.gl/maps/2nvm9UWVEM8B6Wp99'
			]
		];
		return $this->sendResponse($stores, 'Stores retrieved successfully');
	}
}
