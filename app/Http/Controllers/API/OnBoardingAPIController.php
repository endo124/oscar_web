<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateOnBoardingAPIRequest;
use App\Http\Requests\API\UpdateOnBoardingAPIRequest;
use App\Models\OnBoarding;
use App\Repositories\OnBoardingRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class OnBoardingController
 * @package App\Http\Controllers\API
 */

class OnBoardingAPIController extends AppBaseController
{
	/** @var  OnBoardingRepository */
	private $onBoardingRepository;

	public function __construct(OnBoardingRepository $onBoardingRepo)
	{
		$this->onBoardingRepository = $onBoardingRepo;
	}

	/**
	 * Display a listing of the OnBoarding.
	 * GET|HEAD /onBoardings
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->onBoardingRepository->pushCriteria(new RequestCriteria($request));
		$this->onBoardingRepository->pushCriteria(new LimitOffsetCriteria($request));
		$onBoardings = $this->onBoardingRepository->all()->load('values');

		return $this->sendResponse($onBoardings->toArray(), 'On Boardings retrieved successfully');
	}

	/**
	 * Store a newly created OnBoarding in storage.
	 * POST /onBoardings
	 *
	 * @param CreateOnBoardingAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateOnBoardingAPIRequest $request)
	{
		$input = $request->all();

		$onBoardings = $this->onBoardingRepository->create($input);

		return $this->sendResponse($onBoardings->toArray(), 'On Boarding saved successfully');
	}

	/**
	 * Display the specified OnBoarding.
	 * GET|HEAD /onBoardings/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var OnBoarding $onBoarding */
		$onBoarding = $this->onBoardingRepository->findWithoutFail($id);

		if (empty($onBoarding)) {
			return $this->sendError('On Boarding not found');
		}

		return $this->sendResponse($onBoarding->toArray(), 'On Boarding retrieved successfully');
	}

	/**
	 * Update the specified OnBoarding in storage.
	 * PUT/PATCH /onBoardings/{id}
	 *
	 * @param  int $id
	 * @param UpdateOnBoardingAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateOnBoardingAPIRequest $request)
	{
		$input = $request->all();

		/** @var OnBoarding $onBoarding */
		$onBoarding = $this->onBoardingRepository->findWithoutFail($id);

		if (empty($onBoarding)) {
			return $this->sendError('On Boarding not found');
		}

		$onBoarding = $this->onBoardingRepository->update($input, $id);

		return $this->sendResponse($onBoarding->toArray(), 'OnBoarding updated successfully');
	}

	/**
	 * Remove the specified OnBoarding from storage.
	 * DELETE /onBoardings/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var OnBoarding $onBoarding */
		$onBoarding = $this->onBoardingRepository->findWithoutFail($id);

		if (empty($onBoarding)) {
			return $this->sendError('On Boarding not found');
		}

		$onBoarding->delete();

		return $this->sendResponse($id, 'On Boarding deleted successfully');
	}
}
