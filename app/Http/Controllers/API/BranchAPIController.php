<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBranchAPIRequest;
use App\Http\Requests\API\UpdateBranchAPIRequest;
use App\Models\Branch;
use App\Repositories\BranchRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BranchController
 * @package App\Http\Controllers\API
 */

class BranchAPIController extends AppBaseController
{
	/** @var  BranchRepository */
	private $branchRepository;

	public function __construct(BranchRepository $branchRepo)
	{
		$this->branchRepository = $branchRepo;
	}

	/**
	 * Display a listing of the Branch.
	 * GET|HEAD /branches
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		
		$this->branchRepository->pushCriteria(new RequestCriteria($request));
		$this->branchRepository->pushCriteria(new LimitOffsetCriteria($request));
		$branches = $this->branchRepository->all();

		$branches = collect($branches->toArray());
		// $branches = $branches->map(function ($branch) use ($request) {
		// 	$lang = $request->get('lang', 'en');
		// 	$branch['title'] = $branch['title']->$lang;

		// 	return $branch;
		// });
$all=collect();
		$lang = $request->get('lang', 'en');
		$lat1=$request->latitudes;
		$lon1=$request->longitudes;
		foreach($branches as $branch)
		{
			$lat2=$branch['latitudes'];
			$lon2=$branch['longitudes'];
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$km = $miles * 1.609344;
$all->push(['id'=>$branch['id'],'title'=>$branch['title']->$lang,'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
		
		}

		return $this->sendResponse($all->sortBy('km')->values()->toArray(), 'Branches retrieved successfully');
	}

	/**
	 * Store a newly created Branch in storage.
	 * POST /branches
	 *
	 * @param CreateBranchAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateBranchAPIRequest $request)
	{
		$input = $request->all();

		$branches = $this->branchRepository->create($input);

		return $this->sendResponse($branches->toArray(), 'Branch saved successfully');
	}

	/**
	 * Display the specified Branch.
	 * GET|HEAD /branches/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var Branch $branch */
		$branch = $this->branchRepository->findWithoutFail($id);

		if (empty($branch)) {
			return $this->sendError('Branch not found');
		}

		return $this->sendResponse($branch->toArray(), 'Branch retrieved successfully');
	}

	/**
	 * Update the specified Branch in storage.
	 * PUT/PATCH /branches/{id}
	 *
	 * @param  int $id
	 * @param UpdateBranchAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateBranchAPIRequest $request)
	{
		$input = $request->all();

		/** @var Branch $branch */
		$branch = $this->branchRepository->findWithoutFail($id);

		if (empty($branch)) {
			return $this->sendError('Branch not found');
		}

		$branch = $this->branchRepository->update($input, $id);

		return $this->sendResponse($branch->toArray(), 'Branch updated successfully');
	}

	/**
	 * Remove the specified Branch from storage.
	 * DELETE /branches/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var Branch $branch */
		$branch = $this->branchRepository->findWithoutFail($id);

		if (empty($branch)) {
			return $this->sendError('Branch not found');
		}

		$branch->delete();

		return $this->sendResponse($id, 'Branch deleted successfully');
	}
}
