<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\OrderHistory;
use App\Models\Customer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use DB;
use App\Notifications\ResetPasswordCustomer;
class APIForgotPasswordController extends AppBaseController
{
     /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function forgot_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'email' => 'required|string|email|max:255',
			
		]);
		if ($validator->fails()) {
			return response()->json($validator->errors());
		}
        $customer =Customer::where('email', '=', $request->email)
        ->first();
    //Check if the user exists
    if (is_null($customer)) {
        return response()->json(['error' => 'Customer Doesnt Existed'], 401);
    }
    
    //Create Password Reset Token
    DB::table('password_resets')->insert([
        'email' => $request->email,
        'token' => str_random(60),
        'created_at'=>date('Y-m-d H:i:s'),
        
    ]);
    //Get the token just created above
    $tokenData = DB::table('password_resets')
        ->where('email', $request->email)->latest()->first();
    
    if ($this->sendResetEmail($request->email, $tokenData->token)) {
        return response()->json([
			'success' => true,
			'message' => 'A Forget email has been sent to your email address..',
		 // time to expiration

        ], 200);
        } else {
            return response()->json(['error' => 'A Network Error occurred. Please try again.'], 401);

    }
        
    }

                private function sendResetEmail($email, $token)
                {
                //Retrieve the user from the database
                $customer =Customer::where('email', $email)->select('name', 'email')->first();
                //Generate, the password reset link. The token generated is embedded in the link

                try {
                $data=['customer'=>$customer,'token'=>$token];
                $customer->notify(new ResetPasswordCustomer($data));
                //Here send the link with CURL with an external email API 
                return true;
                } catch (\Exception $e) {

                return false;
                }
                }


                //show 
                public function showForgetForm(Request $request, $token = null)
                {
                return view('auth.passwords.forget')->with(
                ['token' => $token, 'email' => $request->email]
                );
                }
                public function forget_password_save(Request $request)
                {
                    
                  //Validate input
                    $validator = Validator::make($request->all(), [
                   
                    'password' => 'required|confirmed',
                    'token' => 'required' ]);
                    $token=$request->token;
                    //check if payload is valid before moving on
                    if ($validator->fails()) {

                        return \Redirect::back()->withErrors($validator);       
                              }

                    $password = $request->password;
                    // Validate the token
                    $tokenData = DB::table('password_resets')
                    ->where('token', $request->token)->first();
                    
                    // Redirect the user back to the password reset request form if the token is invalid
                    if (!$tokenData)  return redirect()->back()->withErrors(['password' => 'Token Is invaild']);

                    $customer = Customer::where('email', $tokenData->email)->first();
                    // Redirect the user back if the email is invalid
                    if (!$customer) return redirect()->back()->withErrors(['password' => 'Email not found']);


                
                    //Hash and update the new password
                    $customer->password = \Hash::make($password);
                    $customer->update(); //or $user->save();



                    //Delete the token
                    \DB::table('password_resets')->where('email', $customer->email)
                    ->delete();
                    return view('update');
   
            }
    
}

