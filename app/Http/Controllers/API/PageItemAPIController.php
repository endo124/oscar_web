<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePageItemAPIRequest;
use App\Http\Requests\API\UpdatePageItemAPIRequest;
use App\Models\PageItem;
use App\Repositories\PageItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PageItemController
 * @package App\Http\Controllers\API
 */

class PageItemAPIController extends AppBaseController
{
    /** @var  PageItemRepository */
    private $pageItemRepository;

    public function __construct(PageItemRepository $pageItemRepo)
    {
        $this->pageItemRepository = $pageItemRepo;
    }

    /**
     * Display a listing of the PageItem.
     * GET|HEAD /pageItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pageItemRepository->pushCriteria(new RequestCriteria($request));
        $this->pageItemRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pageItems = $this->pageItemRepository->all();

        return $this->sendResponse($pageItems->toArray(), 'Page Items retrieved successfully');
    }

    /**
     * Store a newly created PageItem in storage.
     * POST /pageItems
     *
     * @param CreatePageItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePageItemAPIRequest $request)
    {
        $input = $request->all();

        $pageItems = $this->pageItemRepository->create($input);

        return $this->sendResponse($pageItems->toArray(), 'Page Item saved successfully');
    }

    /**
     * Display the specified PageItem.
     * GET|HEAD /pageItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PageItem $pageItem */
        $pageItem = $this->pageItemRepository->findWithoutFail($id);

        if (empty($pageItem)) {
            return $this->sendError('Page Item not found');
        }

        return $this->sendResponse($pageItem->toArray(), 'Page Item retrieved successfully');
    }

    /**
     * Update the specified PageItem in storage.
     * PUT/PATCH /pageItems/{id}
     *
     * @param  int $id
     * @param UpdatePageItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePageItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var PageItem $pageItem */
        $pageItem = $this->pageItemRepository->findWithoutFail($id);

        if (empty($pageItem)) {
            return $this->sendError('Page Item not found');
        }

        $pageItem = $this->pageItemRepository->update($input, $id);

        return $this->sendResponse($pageItem->toArray(), 'PageItem updated successfully');
    }

    /**
     * Remove the specified PageItem from storage.
     * DELETE /pageItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PageItem $pageItem */
        $pageItem = $this->pageItemRepository->findWithoutFail($id);

        if (empty($pageItem)) {
            return $this->sendError('Page Item not found');
        }

        $pageItem->delete();

        return $this->sendResponse($id, 'Page Item deleted successfully');
    }
}
