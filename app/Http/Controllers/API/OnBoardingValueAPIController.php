<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateOnBoardingValueAPIRequest;
use App\Http\Requests\API\UpdateOnBoardingValueAPIRequest;
use App\Models\OnBoardingValue;
use App\Repositories\OnBoardingValueRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class OnBoardingValueController
 * @package App\Http\Controllers\API
 */

class OnBoardingValueAPIController extends AppBaseController
{
	/** @var  OnBoardingValueRepository */
	private $onBoardingValueRepository;

	public function __construct(OnBoardingValueRepository $onBoardingValueRepo)
	{
		$this->onBoardingValueRepository = $onBoardingValueRepo;
	}

	/**
	 * Display a listing of the OnBoardingValue.
	 * GET|HEAD /onBoardingValues
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->onBoardingValueRepository->pushCriteria(new RequestCriteria($request));
		$this->onBoardingValueRepository->pushCriteria(new LimitOffsetCriteria($request));
		$onBoardingValues = $this->onBoardingValueRepository->all();

		return $this->sendResponse($onBoardingValues->toArray(), 'On Boarding Values retrieved successfully');
	}

	/**
	 * Store a newly created OnBoardingValue in storage.
	 * POST /onBoardingValues
	 *
	 * @param CreateOnBoardingValueAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$input = [];
		
		foreach ($request->all() as $key => $value) {
			$data = $value;
			if(is_array($data)) {
				$data = implode(',', $value);
			}
			array_push($input, [
				'question_id' => $key,
				'value' => $data
			]);
		}

		$onBoardingValues = [];
		foreach ($input as $record) {
			array_push($onBoardingValues, $this->onBoardingValueRepository->create($record)->toArray());
		}

		return $this->sendResponse($onBoardingValues, 'On Boarding Value saved successfully');
	}

	/**
	 * Display the specified OnBoardingValue.
	 * GET|HEAD /onBoardingValues/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var OnBoardingValue $onBoardingValue */
		$onBoardingValue = $this->onBoardingValueRepository->findWithoutFail($id);

		if (empty($onBoardingValue)) {
			return $this->sendError('On Boarding Value not found');
		}

		return $this->sendResponse($onBoardingValue->toArray(), 'On Boarding Value retrieved successfully');
	}

	/**
	 * Update the specified OnBoardingValue in storage.
	 * PUT/PATCH /onBoardingValues/{id}
	 *
	 * @param  int $id
	 * @param UpdateOnBoardingValueAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateOnBoardingValueAPIRequest $request)
	{
		$input = $request->all();

		/** @var OnBoardingValue $onBoardingValue */
		$onBoardingValue = $this->onBoardingValueRepository->findWithoutFail($id);

		if (empty($onBoardingValue)) {
			return $this->sendError('On Boarding Value not found');
		}

		$onBoardingValue = $this->onBoardingValueRepository->update($input, $id);

		return $this->sendResponse($onBoardingValue->toArray(), 'OnBoardingValue updated successfully');
	}

	/**
	 * Remove the specified OnBoardingValue from storage.
	 * DELETE /onBoardingValues/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var OnBoardingValue $onBoardingValue */
		$onBoardingValue = $this->onBoardingValueRepository->findWithoutFail($id);

		if (empty($onBoardingValue)) {
			return $this->sendError('On Boarding Value not found');
		}

		$onBoardingValue->delete();

		return $this->sendResponse($id, 'On Boarding Value deleted successfully');
	}
}
