<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\AppBaseController;

use Illuminate\Http\Request;
use App\Models\Message;
use App\Models\Channel;
use App\Models\Customer;
use Auth;
use App\Events\MessageSent;
use App\Events\ChannelCreate;
use App\Events\AgentSent;
class ChatsController extends AppBaseController
{
    //



/**
 * Show chats
 *
 * @return \Illuminate\Http\Response
 */


/**
 * Fetch all messages
 *
 * @return Message
 */
public function fetchMessages(Request $request)
{
if(!is_null($request->channel_id))
{
  $channel=Channel::where('id',$request->channel_id)->first();

  if($channel->status=='closed')
  {
    return Message::with('customer')->where('channel_id',$request->channel_id)->get();

  }
  else
  {
    return Message::with('customer')->where('channel_id',$request->channel_id)->where('status','!=','old')->get();

  }
}
else{
  return 'false';
}
  

}

/**
 * Persist message to database
 *
 * @param  Request $request
 * @return Response
 */

public function sendMessage(Request $request)
{
  $customer = Auth::guard('customers')->user();
$agent=Customer::where('type','customer-agent')->where('status','available')->count();

$order=Channel::where('status','inactive')->count();
$channel=Channel::where('client_id',$customer->id)->first();

if(is_null($channel))
{
  $channel= Channel::create(['client_id'=> $customer->id,'channel_name'=>'chat.'. $customer->id,'status'=>'inactive']);
  $customer->update(['channel_id'=>$channel->id]);
}
else
{
if($channel->status=='active')
{
  Channel::where('client_id',$customer->id)->update(['status'=>'active']); 
}
else
{
  Channel::where('client_id',$customer->id)->update(['status'=>'inactive']);
}
  
}
broadcast(new ChannelCreate($customer,$channel))->toOthers();

if($agent==0 && is_null($channel->customer_agent_id))
{
  
  return response()->json([
    'success' => false,
    'message'=>'All our agents are busy at this moment. Please wait, while our representative will join you shortly.',   
     'order' =>$order+1,
    'channel_name'=>'chat.'. $customer->id,
    
  ], 200);
}

  

  $message = $customer->messages()->create([
    'message' => $request->input('message'),
    'channel_id'=>$channel->id]);
  
  broadcast(new MessageSent($customer, $message))->toOthers();

  return ['status' => true,
  'message'=>'All our agents are busy at this moment. Please wait, while our representative will join you shortly.',
  'channel_name'=>'chat.'.$customer->id,
  'order' =>1,
  'channel_id'=>$channel->id];
}
public function online_now(Request $request)
{

  \DB::table('online_now')->delete();
  \DB::table('online_now')->insert(['online_number'=>$request->input('counter')]);
  return ['status' => 'update'];
}

public function get_users()
{
 
 $channels= Channel::with('customer')->get()->groupBy('client_id');
foreach($channels as $channel)
{ 
  $customer=Customer::find($channel->first()->client_id);
  $channel->put('messages',$customer->messages->last());


}
return response()->json(['channels' => $channels->values()]);
  
}
public function get_channelid(Request $request)
{
 $channels= Channel::where('client_id',$request->input('client_id'))->where('status','inactive')->update(['status'=>'closed']);

  return   ['status' => 'update'];
}
public function messages_client(Request $request)
{  
  $customer = Auth::guard('customers')->user();
  
  $message = $customer->messages()->create([
    'message' => $request->message,
    'channel_id'=>$request->channel_id 
  ]);
  $channel=Channel::where('id',$request->channel_id)->first();
  $channel->update(['customer_agent_id'=>$customer->id,'status'=>'active']);
  $customer->update(['status'=>'unavailable']);
  $client=Customer::where('id',$channel->client_id)->first();
  

  broadcast(new AgentSent($customer, $message,$client->id))->toOthers();

  return ['status' => 'Message Sent!'];
}

public function end_chat(Request $request)
{
  $customer = Auth::guard('customers')->user();
  $customer->update(['status'=>'available']);
  $channel=Channel::where('id',$request->channel_id)->first();
  $messages=Message::where('channel_id',$request->channel_id)->update(['status'=>'old']);
  $channel->update(['customer_agent_id'=>null,'status'=>'closed']);

  return ['status' => 'update'];
}
public function get_online(Request $request)
{
  $customer = Auth::guard('customers')->user();

 $online= \DB::table('online_now')->select('online_number')->first();

 return response()->json([
  'success' => true,
  'message' => 'online agents',
  'online' =>$online->online_number,

  
], 200);
}
public function get_client_name(Request $request)
{
  $customer = Customer::find($request->client_id);
 

  return $customer->name;
}
public function get_client_info(Request $request)
{
  $customer = Customer::find($request->client_id);
 

  return $customer;
}
public function leaving_user(Request $request)
{
  $customer =Customer::find($request->user_id);
  $message = $customer->messages()->create([
    'message' => 'Left',
    'channel_id'=>$request->channel_id]);
  
  broadcast(new MessageSent($customer, $message))->toOthers();
  return ['status' => 'message sent'];
}
public function change_status(Request $request)
{
  $customer = Auth::guard('customers')->user();
  $channel=Channel::where('id',$request->id)->first();
  
  $channel->update(['customer_agent_id'=>$customer->id,'status'=>'active']);

  return ['status' => 'update'];
}
}
