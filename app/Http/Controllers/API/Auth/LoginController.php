<?php namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\API\LoginAPIRequest;

class LoginController extends AppBaseController {
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest')->except(['logout', 'user']);
		$this->middleware(['auth:api', 'verified'])->only(['logout', 'user']);
	}

	public function login(LoginAPIRequest $request) {
		$credentials = $request->only('email', 'password');
		if ($this->hasTooManyLoginAttempts($request)) {
			$this->fireLockoutEvent($request);
			// this can throw a ValidationException
			$this->sendLockoutResponse($request);
		}
		$response = [];
		$status = 200;
		if($response['success'] = \Auth::attempt($credentials)) {
			$this->clearLoginAttempts($request);
			$user = \Auth::user();
			$response['token'] = \JWTAuth::fromUser($user);
		} else {
			$response['message'] = 'Invalid credentials';
			$this->incrementLoginAttempts($request);
			$status = 401;
		}
		return response()->json($response, $status);
	}

	public function logout(Request $request) {
		$user = $request->user();
		$user->api_token = null;
		$response = [
			'success' => $user->update()
		];
		return \response()->json($response, 200);
	}

	public function user(Request $request) {
		$user = $request->user()->load('employee', 'organizations');
		$user->token = $user->api_token;
		$response = [
			'success' => true,
			'data' => $user
		];
		return response()->json($response, 200);
	}
}
