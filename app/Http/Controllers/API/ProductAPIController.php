<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Repositories\CategoryRepository;
use Carbon\Carbon;
use App\Models\Category;
use App\Models\Product;
use Cache;
class ProductAPIController extends AppBaseController
{

	/** @var  CategoryRepository */
	private $categoryRepository;

	public function __construct(CategoryRepository $categoryRepo)
	{
		$this->categoryRepository = $categoryRepo;
	}

	public function index(Request $request)
	{
		
		$res = [];
		 ini_set('max_execution_time', 5000);
		 ini_set('memory_limit', '1024M');
	////// check if cache  empty or not 
	$products_all=Product::count();

		// if( is_null(\Cache::get('products')) )
		if($products_all==0)
		{
			
		
				$ch = curl_init('http://41.33.238.100/RR_Oscar_Product/Integration_WB.asmx/GetProduct');
		
				// Request headers
				$headers = array();
				$headers[] = '';
		
				// Return the transfer as a string
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
				// $output contains the output string
				$output = curl_exec($ch);
		
				// Close curl resource to free up system resources
				curl_close($ch);
				$product_response= json_decode($output);
				
				// $products = \Cache::get('products', []);
				$array=collect($product_response)->chunk(500);
				
				foreach($array as $arr)
				{	
					foreach($arr as $pro)
					{
					
					if($pro->PriceUnit !="box") 
					{
			$cats = $this->categoryRepository->findByField('id',$pro->CatRecid)->toArray();
			$product_find= Product::where('barcode',$pro->Barcode)->first();
		if(strlen( $pro->NameEn)!= 0  && strlen($pro->NameAr)!=0 && $cats!=[] && is_null($product_find) )
		{

			
					
					    // $product['id'] =$pro->ITEMID.$pro->PriceUnit;
					    // $product['name'] =  $pro->NameEn;
						// $product['description'] ='';
						// $product['name_ar'] = $pro->NameAr;
						// $product['description_ar'] = '';
						// $product['sale_price'] = $pro->discountprice;
						// $product['on_sale'] =($pro->DiscountFlag == 1) ? true : false;;
						// $product['regular_price'] = $pro->Price;
						// $product['hot_price'] = false;
						// $product['discountFrom'] = $pro->discountFrom;
						// $product['discountto'] = $pro->discountto;
						// $product['discountprice'] = $pro->discountprice;
						// $product['barcode']=$pro->Barcode;
						// $product['pro_number']=$pro->ITEMID;
						// $product['PriceActivefrom']=$pro->PriceActivefrom;
						// $product['PriceActiveto']=$pro->PriceActiveto;
						// $product['PriceUnit']=$pro->PriceUnit;
						// $product['VendorId']=$pro->VendorId;
						// $product['in_stock'] = true;
						
						    $selectedCategories = array();
							$cats = $this->categoryRepository->findByField('id',$pro->CatRecid)->toArray();

							$category=Category::where('id',$pro->CatRecid)->first();
							$category->update(array('count'=>$category->count+1));
							
							
							array_push($selectedCategories, $cats[0]);
							if(!is_null($cats[0]['parent']))
							{
								
							$category_parnet=Category::where('id',$cats[0]['parent'])->first();
							
							$category_parnet->update(array('count'=>$category_parnet->count+1));
							array_push($selectedCategories, $category_parnet);
						
								if(!is_null($category_parnet->parent) && $category_parnet->parent !=0 )
								{

								$category_parnet_1=Category::where('id',$category_parnet->parent)->first();
								$category_parnet_1->update(array('count'=>$category_parnet_1->count+1));
								array_push($selectedCategories, $category_parnet_1);
								if(!is_null($category_parnet_1->parent) && $category_parnet_1->parent !=0)
								{
								$category_parnet_2=Category::where('id',$category_parnet_1->parent)->first();
								$category_parnet_2->update(array('count'=>$category_parnet_2->count+1));
								if(!is_null($category_parnet_2->parent) && $category_parnet_2->parent !=0)
								{
								$category_parnet_3=Category::where('id',$category_parnet_2->parent)->first();
								$category_parnet_3->update(array('count'=>$category_parnet_3->count+1));
								}
								}
								}
								

							}

								
										
	
						    // $product['categories'] = $selectedCategories;
					
							$images = preg_split ("/\;/", $pro->Productimage);
							if(strlen( $pro->Productimage)!= 0)
							{
							$images = preg_split ("/\;/", $pro->Productimage);
							// for($i=0;$i< count($images);$i++)
							// {
							$img[0]['src'] ='https://oscar.momentum-sol.com/product/'.$pro->Barcode.'.jpg';
							// }
							}
							else{
							$img[0]['src']=url('/images/image.png');
							}
						
						$product['images'] = $img;
						$branch=json_decode(json_encode($pro), true);
						if($branch["01"]==1)
						{
							$product['01']='true';
						}
						else{
							$product['01']='false';	

						}
						if($branch["02"]==1)
						{
							$product['02']='true';
						}
						else{
							$product['02']='false';	

						}
						if($branch["04"]==1)
						{
							$product['04']='true';
						}
						else{
							$product['04']='false';	

						}

	$product=Product::create(['id'=>$pro->ITEMID.$pro->PriceUnit,'name'=>$pro->NameEn,'description'=>$pro->DescriptionEn,
	'name_ar'=> $pro->NameAr,'description_ar'=>$pro->DescriptionAr,'sale_price'=> $pro->discountprice,'on_sale'=>($pro->DiscountFlag == 1) ? true : false,
	'regular_price'=> $pro->Price,'hot_price'=>false,'discountFrom'=>$pro->discountFrom,'discountto'=>$pro->discountto,'discountprice'=>$pro->discountprice,
	'barcode'=>$pro->Barcode,'PriceUnit'=>$pro->PriceUnit,'in_stock' => true,'category_id'=>$pro->CatRecid,'images'=>json_encode($img),
	'01'=>$product['01'],'02'=>$product['02'],'04'=>$product['04'],'categories'=>json_encode($selectedCategories)]);
	
						//array_push($products, $product);
				
				}
			}
				
			}
			$expiresAt = Carbon::now()->addMinutes(24*60);
			//\Cache::forever('products', $products);
			\Cache::put('products_update', true,$expiresAt);
		// 	$data =\Cache::get('products');
		//    return $data;
				
		}
		}
		// else
		// {
			//dd(\Cache::get('products_update'));
		// 	\Cache::remember('products_update',24*60, function() {
		
		// 	$ch = curl_init('http://41.33.238.100/RR_Oscar_Product/Integration_WB.asmx/GetProductUpdate');
		
		// 	// Request headers
		// 	$headers = array();
		// 	$headers[] = '';
	
		// 	// Return the transfer as a string
		// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
		// 	// $output contains the output string
		// 	$output = curl_exec($ch);
	
		// 	// Close curl resource to free up system resources
		// 	curl_close($ch);
		// 	$update_product_response= json_decode($output);
			
		// 	$array=collect($update_product_response)->chunk(500);
			
		// 	foreach($array as $arr)
		// 	{	
		// 		foreach($arr as $pro)
		// 		{

				
		// 		if($pro->PriceUnit !="box") 
		// 		{
		// 	/// update product	
		// if(strtolower($pro->Action)=="update")
		//  {
	
		// $products = \Cache::get('products', []);
		// 	$product = [];
		
		// 	foreach ($products as $p) {
			
		// 		if ($p['id'] == $pro->ITEMID.$pro->PriceUnit) {
		// 			$product = $p;
		// 		}
		// 	}
		// 	$cats = $this->categoryRepository->findByField('id',$pro->CatRecid)->toArray();
			
		// 	if(strlen( $pro->NameEn)!= 0  && strlen($pro->NameAr)!=0 && $cats !=[] && array_key_exists('categories', $product) )
		// {
			
		// 	$product['name'] =  $pro->NameEn;
		// 	$product['description'] ='';
		// 	$product['name_ar'] = $pro->NameAr;
		// 	$product['description_ar'] = '';
		// 	$product['sale_price'] = $pro->discountprice;
		// 	$product['on_sale'] =($pro->DiscountFlag == 1) ? true : false;;
		// 	$product['regular_price'] = $pro->Price;
		// 	$product['hot_price'] = false;
		// 	$product['discountFrom'] = $pro->discountFrom;
		// 	$product['discountto'] = $pro->discountto;
		// 	$product['discountprice'] = $pro->discountprice;
		// 	$product['barcode']=$pro->Barcode;
		// 	$product['pro_number']=$pro->ITEMID;
		// 	$product['PriceActivefrom']=$pro->PriceActivefrom;
		// 	$product['PriceActiveto']=$pro->PriceActiveto;
		// 	$product['PriceUnit']=$pro->PriceUnit;
		// 	$product['VendorId']=$pro->VendorId;
		// 	$product['in_stock'] = true;
			
		// 	$selectedCategories = array();
		
		// 	$cats = $this->categoryRepository->findByField('id',$pro->CatRecid)->toArray();
		// 	array_push($selectedCategories, $cats[0]);
		// 	///ceheck if  it have same category on update or not
			
		// 		if($product['categories'][0]['id']==$cats[0]['id'])
		// 		{
                     
		// 		$product['categories'] = $selectedCategories;
		// 		 }
		// 		else
		// 		{        /// minus count of old category and update count of new category
		// 			        $category_old=Category::where('id',$product['categories'][0]['id'])->first();
		// 					$category_old->update(array('count'=>$category_old->count-1));

		// 					if(!is_null($product['categories'][0]['parent'])  && $product['categories'][0]['parent'] !=0 )
		// 					{
		// 					$category_old_parnet=Category::where('id',$product['categories'][0]['parent'])->first();
		// 					$category_old_parnet->update(array('count'=>$category_old_parnet->count-1));
		// 					if(!is_null($category_old_parnet->parent) && $category_old_parnet->parent !=0)
		// 					{
		// 					$category_old_parnet_1=Category::where('id',$category_old_parnet->parent)->first();
		// 					$category_old_parnet_1->update(array('count'=>$category_old_parnet_1->count-1));
		// 					if(!is_null($category_old_parnet_1->parent) && $category_old_parnet_1->parent !=0)
		// 					{
		// 					$category_old_parnet_2=Category::where('id',$category_old_parnet_1->parent)->first();
		// 					$category_old_parnet_2->update(array('count'=>$category_old_parnet_2->count-1));
		// 					if(!is_null($category_old_parnet_2->parent) && $category_old_parnet_2->parent !=0)
		// 					{
		// 					$category_old_parnet_3=Category::where('id',$category_old_parnet_2->parent)->first();
		// 					$category_old_parnet_3->update(array('count'=>$category_old_parnet_3->count-1));
		// 					}
		// 					}
		// 					}

		// 					}
		// 					$category=Category::where('id',$pro->CatRecid)->first();
		// 					$category->update(array('count'=>$category->count+1));

		// 					if(!is_null($cats[0]['parent']))
		// 					{
		// 					$category_parnet=Category::where('id',$cats[0]['parent'])->first();
		// 					$category_parnet->update(array('count'=>$category_parnet->count+1));

		// 						if(!is_null($category_parnet->parent) && $category_parnet->parent!=0)
		// 						{
		// 						$category_parnet_1=Category::where('id',$category_parnet->parent)->first();
		// 						$category_parnet_1->update(array('count'=>$category_parnet_1->count+1));
		// 						if(!is_null($category_parnet_1->parent)  && $category_parnet_1->parent!=0)
		// 						{
		// 						$category_parnet_2=Category::where('id',$category_parnet_1->parent)->first();
		// 						$category_parnet_2->update(array('count'=>$category_parnet_2->count+1));
		// 						if(!is_null($category_parnet_2->parent) && $category_parnet_2->parent!=0)
		// 						{
		// 						$category_parnet_3=Category::where('id',$category_parnet_2->parent)->first();
		// 						$category_parnet_3->update(array('count'=>$category_parnet_3->count+1));
		// 						}
		// 						}
		// 						}
								

		// 					}
		// 					$product['categories'] = $selectedCategories;

		// 		}
			
			
		
		// 	if(strlen( $pro->Productimage)!= 0)
		// 	{
		// 	   $images = preg_split ("/\;/", $pro->Productimage);
		// 	   for($i=0;$i< count($images);$i++)
		// 	   {
		// 		   $img[$i]['src'] =$images[$i];
		// 	   }
		// 	}
		// 	else{
		// 		$img[0]['src']=url('/images/image.png');
		// 	}
	   
	
		// 		$product['images'] = $img;
				
	
		// 		foreach ($products as $key => $p) {
		// 			if ($p['id'] == $pro->ITEMID.$pro->PriceUnit) {
		// 				$products[$key] = $product;
		// 			}
		// 			}
	
		//   \Cache::forever('products', $products);
		// 		}
		// 		}
		// 		//insert
		// 		elseif(strtolower($pro->Action)=="insert")
		// 		{
		// 			$cats = $this->categoryRepository->findByField('id',$pro->CatRecid)->toArray();

		// 	if(strlen( $pro->NameEn)!= 0  && strlen($pro->NameAr)!=0 && $cats !=[])
					
		// {
		// 			$product['id'] =$pro->ITEMID.$pro->PriceUnit;
		// 			$product['name'] =  $pro->NameEn;
		// 			 $product['description'] ='';
		// 			 $product['name_ar'] = $pro->NameAr;
		// 			 $product['description_ar'] = '';
		// 			 $product['sale_price'] = $pro->discountprice;
		// 			 $product['on_sale'] =($pro->DiscountFlag == 1) ? true : false;;
		// 			 $product['regular_price'] = $pro->Price;
		// 			 $product['hot_price'] = false;
		// 			 $product['discountFrom'] = $pro->discountFrom;
		// 			 $product['discountto'] = $pro->discountto;
		// 			 $product['discountprice'] = $pro->discountprice;
		// 			 $product['barcode']=$pro->Barcode;
		// 			 $product['pro_number']=$pro->ITEMID;
		// 			 $product['PriceActivefrom']=$pro->PriceActivefrom;
		// 			 $product['PriceActiveto']=$pro->PriceActiveto;
		// 			 $product['PriceUnit']=$pro->PriceUnit;
		// 			 $product['VendorId']=$pro->VendorId;
		// 			 $product['in_stock'] = true;
					 
		// 			 $selectedCategories = array();
		// 				 $cats = $this->categoryRepository->findByField('id',$pro->CatRecid)->toArray();
		// 				 $cats = $this->categoryRepository->findByField('id',$pro->CatRecid)->toArray();

		// 				 $category=Category::where('id',$pro->CatRecid)->first();
		// 				 $category->update(array('count'=>$category->count+1));

		// 				 if(!is_null($cats[0]['parent']) && $cats[0]['parent'] !=0 )
		// 				 {
		// 				 $category_parnet=Category::where('id',$cats[0]['parent'])->first();
		// 				 $category_parnet->update(array('count'=>$category_parnet->count+1));

		// 					 if(!is_null($category_parnet->parent) && $category_parnet->parent !=0)
		// 					 {
		// 					 $category_parnet_1=Category::where('id',$category_parnet->parent)->first();
		// 					 $category_parnet_1->update(array('count'=>$category_parnet_1->count+1));
		// 					 if(!is_null($category_parnet_1->parent)  && $category_parnet_1->parent !=0)
		// 					 {
		// 					 $category_parnet_2=Category::where('id',$category_parnet_1->parent)->first();
		// 					 $category_parnet_2->update(array('count'=>$category_parnet_2->count+1));
		// 					 if(!is_null($category_parnet_2->parent)  && $category_parnet_2->parent !=0)
		// 					 {
		// 					 $category_parnet_3=Category::where('id',$category_parnet_2->parent)->first();
		// 					 $category_parnet_3->update(array('count'=>$category_parnet_3->count+1));
		// 					 }
		// 					 }
		// 					 }
							 

		// 				 }
						 
		// 					 array_push($selectedCategories, $cats[0]);
					 
	 
		// 			 $product['categories'] = $selectedCategories;
		// 			 if(strlen( $pro->Productimage)!= 0)
		// 			 {
		// 				$images = preg_split ("/\;/", $pro->Productimage);
		// 				for($i=0;$i< count($images);$i++)
		// 				{
		// 					$img[$i]['src'] =$images[$i];
		// 				}
		// 			 }
		// 			 else{
		// 				$img[0]['src']=url('/images/image.png');
		// 			 }
				
					 
		// 			 $product['images'] = $img;
					
		// 			 array_push($products, $product);
		// 			 \Cache::forever('products', $products);
		// 		}
		// 		}
		// 		elseif(strtolower($pro->Action)=="delete")
		// 		{
		// 			if(strlen( $pro->NameEn)!= 0  && strlen($pro->NameAr)!=0)
		//        {
		// 			$products = \Cache::get('products', []);
		// 			$product = [];
		// 			$index = -1;
		// 			foreach ($products as $key => $p) {
		// 				if ($p['id'] == $pro->ITEMID.$pro->PriceUnit) {
		// 					$index = $key;
		// 				}
		// 			}
		// 			        $category=Category::where('id',$pro->CatRecid)->first();
		// 					$category->update(array('count'=>$category->count-1));

		// 					if(!is_null($product['categories'][0]['parent']) && $product['categories'][0]['parent'] !=0)
		// 					{
		// 					$category_old_parnet=Category::where('id',$product['categories'][0]['parent'])->first();
		// 					$category_old_parnet->update(array('count'=>$category_old_parnet->count-1));
		// 					if(!is_null($category_old_parnet->parent) && $category_old_parnet->parent !=0)
		// 					{
		// 					$category_old_parnet_1=Category::where('id',$category_old_parnet->parent)->first();
		// 					$category_old_parnet_1->update(array('count'=>$category_old_parnet_1->count-1));
		// 					if(!is_null($category_old_parnet_1->parent)  && $category_old_parnet_1->parent !=0)
		// 					{
		// 					$category_old_parnet_2=Category::where('id',$category_old_parnet_1->parent)->first();
		// 					$category_old_parnet_2->update(array('count'=>$category_old_parnet_2->count-1));
		// 					if(!is_null($category_old_parnet_2->parent)  && $category_old_parnet_2->parent !=0)
		// 					{
		// 					$category_old_parnet_3=Category::where('id',$category_old_parnet_2->parent)->first();
		// 					$category_old_parnet_3->update(array('count'=>$category_old_parnet_3->count-1));
		// 					}
		// 					}
		// 					}

		// 					}
		// 			unset($products[$index]);

		// 		  // \Cache::forget('products');
		// 	   \Cache::forever('products', $products);
		// 		}
	
		// 	}
		// }
		
		//    }
	
		// 		}

		// 		$expiresAt = Carbon::now()->addMinutes(60);
		
		// 		 \Cache::put('products_update', $products,$expiresAt);
		// 		 $data =\Cache::get('products');
		// 		return $data;
		// 	});
				
		// }
	


		

		
// $products_data=Product::all();

// 		if ($data = $products_data) {
		
// 			$res =  $products_data->values();
// 		}
	

// 		$search = $request->get('name', false);
// 		if ($search) {
// 			$res = collect($res)->filter(function ($p) use ($search) {
// 				if (
// 					stripos($p['name'], $search) !== false ||
// 					stripos($p['description'], $search) !== false ||
// 					(isset($p['name_ar']) && stripos($p['name_ar'], $search) !== false) ||
// 					(isset($p['description_ar']) && stripos($p['description_ar'], $search) !== false)
// 				) {
// 					return $p;
// 				}
// 			})->values();
// 		}

	
// 		if ($request->get('filter')) {
// 			$filter = $request->get('filter');
	
// 			$filter = explode(',', $filter);
// 			$result=collect();
// 			$array=collect($res)->chunk(1000);
// 			foreach($array as $pros)
// 			{
// 				foreach($pros as $pro)
// 				{
				
// 				if(isset($pro->category_id) && in_array($pro->category_id, $filter))
// 				{
					
// 					$result->push($pro);
// 				}
// 				if(isset($pro['categories'][0]->parent) && in_array($pro['categories'][0]->parent, $filter))
// 				{
// 					$result->push($pro);
// 				}
				
// 				$category_parnet=\DB::table('categories')->where('id',$pro['categories'][0]->parent)->first();
		
// 				if(isset($category_parnet->parent) && in_array($category_parnet->parent, $filter))
// 				{
					
// 					$result->push($pro);
// 				}
// 				if(!is_null($category_parnet->parent))
// 				{
// 					$category_parnet_1=\DB::table('categories')->where('id',$category_parnet->parent)->first();
		
// 					if(isset($category_parnet_1->parent) && in_array($category_parnet_1->parent, $filter))
// 					{
// 						$result->push($pro);
// 					}
// 				}
// 			}

// 			}
// 			$res=$result;

			
		
		
// 		}
// 			if ($request->get('barcode')) 
// 			{
// 			$barcode = $request->get('barcode');
	
			
// 			if ($barcode) {
// 				$res = collect($res)->filter(function ($p) use ($barcode) {
// 					if ($p['barcode'] ==  $barcode) {
// 						return $p;
// 					}
// 				})->values();
// 			}
		
// 		}

// 		if ($request->get('store_id')) 
// 		{
// 		$store_id = $request->get('store_id');

		
// 		if ($store_id) {
// 			$res = collect($res)->filter(function ($p) use ($store_id) {
				
// 				if ($p[$store_id] ==  true) {
// 					return $p;
// 				}
// 			})->values();
// 		}
	
// 	}
// 	if ($request->get('lang', null) === 'ar') {
// 		$res = collect($res)->map(function ($p) {
// 			if (isset($p['name_ar']))
// 				$p['name'] = $p['name_ar'];
// 			if (isset($p['description_ar']))
// 				$p['description'] = $p['description_ar'];
// 			return $p;
// 		});
// 	}
		
//enchament code andrew
$resCache = Cache::get("res", null);


if ($request->get('lang', null) === 'ar') {
	$products_data = Product::where('category_id',5637168576)->select(['*','name_ar as name','description_ar as description'])->get();



	if ($data = $products_data) {
	
		$res =  $products_data;
	}

$search = $request->get('name', false);
if ($search) {

	$res=Product::where('category_id','=','5637168576')->select(['*','name_ar as name','description_ar as description'])->where(function ($q) use ($search){

		$q->where("name", "LIKE","%".$search."%")
		->orWhere("description","LIKE","%".$search."%")
		->orWhere("name_ar", "LIKE","%".$search."%")
		->orWhere("description_ar", "LIKE","%".$search."%")
		->orWhere("barcode",$search);


	})->get();

}


	
if ($request->get('filter')) {
	$filter = $request->get('filter');
	$filter = explode(',', $filter);
	$result=collect();
	$array=collect($res)->chunk(1000);
	foreach($array as $pros)
	{
		foreach($pros as $pro)
		{
			$cats=$pro->categories;
			foreach($cats as $cat ){
				if(in_array($cat->id,$filter)){
					// $result->push($pro);
					$category=Category::find($cat->id);
					if($category->children){
						$cat_children=$category->children;
						// dd($cat_children);
						foreach($cat_children as $cat_child){
							if(in_array($cat_child->id ,$filter)){
							$result->push($pro);
							}
						}
					}
				}
				if(in_array($cat->parent,$filter)){
					$result->push($pro);
				}
			}
		
	}
	}
	$res= $result;
	// dd(count($res));
}
	if ($request->get('barcode')){
	$barcode = $request->get('barcode');

	
	if ($barcode) {
		$res=Product::where('category_id','=','5637168576')->select(['*','name_ar as name','description_ar as description'])->where('barcode',$barcode)->get();

	
	}

}

if ($request->get('store_id')){
	$store_id = $request->get('store_id');


	if ($store_id) {

			$res=Product::where('category_id','=','5637168576')->select(['*','name_ar as name','description_ar as description'])->where($store_id , 'true')->get();

	
		
	
	}

}

}
	


else{



	$products_data=Product::where('category_id','=','5637168576')->get();

	if ($data = $products_data) {
	
		$res =  $products_data;
	}
$search = $request->get('name', false);
if ($search) {

	$res=Product::where('category_id','=','5637168576')->where(function ($q) use ($search){

		$q->where("name", "LIKE","%".$search."%")
		
		->orWhere("description","LIKE","%".$search."%")
		->orWhere("name_ar", "LIKE","%".$search."%")
		->orWhere("description_ar", "LIKE","%".$search."%")
		->orWhere("barcode",$search);
		

	})->get();

}



	
if ($request->get('filter')) {
	$filter = $request->get('filter');
	$filter = explode(',', $filter);
	$result=collect();
	$array=collect($res)->chunk(1000);
	foreach($array as $pros)
	{
		foreach($pros as $pro)
		{
			$cats=$pro->categories;
			foreach($cats as $cat ){
				if(in_array($cat->id,$filter)){
					// $result->push($pro);
					$category=Category::find($cat->id);
					if($category->children){
						$cat_children=$category->children;
						// dd($cat_children);
						foreach($cat_children as $cat_child){
							if(in_array($cat_child->id ,$filter)){
							$result->push($pro);
							}
						}
					}
				}
				if(in_array($cat->parent,$filter)){
					$result->push($pro);
				}
			}
		
	}
	}
	$res= $result;
	// dd(count($res));
}
	if ($request->get('barcode')) 
	{
	$barcode = $request->get('barcode');

	
	if ($barcode) {
		$res=Product::where('barcode',$barcode)->where('category_id','=','5637168576')->get();

	
	}

}

if ($request->get('store_id')){
$store_id = $request->get('store_id');

if ($store_id) {
		$res=Product::where($store_id , 'true')->where('category_id','=','5637168576')->get();


}
}
}
		// $paginate = 10;
		// $page = $request->get('page', 1);

		// $offSet = ($page * $paginate) - $paginate;

		// $itemsForCurrentPage = array_slice($res->ToArray(), $offSet, $paginate, true);

		// $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($res), $paginate, $page);

if($res!=[])
{
	$res = $res->values()->toArray();
	
}
if(count($res)==0)
{
return response()->json([
		'success' => false,
		'message' => 'No Products Found',
	], 200);
}

return $this->sendResponse($res, 'Products retrieved successfully');	
	}

	public function getProductsByCategory($categoryId, Request $request)
	{
		ini_set('max_execution_time', 5000);
		ini_set('memory_limit', '1024M');
		$res = [];
		if (strtolower($categoryId) == 'on_sale') {
		
		$products_data=Product::where('category_id','=','5637168576')->get();
		}
		else
		{
			$products_data=Product::all();
		}
		if ($data = $products_data) {
			
			$products = array_values($data->ToArray());
			
		}

		$category = $this->categoryRepository->findWithoutFail($categoryId);
		 if (strtolower($categoryId) == 'signature') {
					
			$category = \App\Models\Category::where('slug', 'signature')->first();
			
			$res=Product::where('category_id',$category->id)->get();
			 //if ($category) $categoryId = $category->id;
		}
		else if (strtolower($categoryId) != 'banner') {
			$res = collect($products)->filter(function ($p) use ($category, $categoryId) {
	
				if (strtolower($categoryId) == 'on_sale') {
		
			
				return $p['on_sale'];
			
					
				} 
				else if (strtolower($categoryId) == 'signature') {
					
					$category = \App\Models\Category::where('slug', 'signature')->first();
				
					 if ($category) $categoryId = $category->id;
				}
				
				$categories = collect($p['categories']);
				// if ($category && count($category->children)) {
					$subCategories = $category->children->pluck('id')->toArray();
					
				
					// if(isset($categories[0]->{'parent'}))
					// {
					// 	$category_parnet_1=\DB::table('categories')->where('id',$categories[0]->{'parent'})->first();
					// }
					
		    
					
					// 	if(!is_null($category_parnet_1))
					// 	{

					// 		if(isset($category_parnet_1->parent) && in_array($category_parnet_1->parent, $subCategories))return true;
					// 	}
					foreach ($subCategories as $subCat) {
				
						if ($categories->contains('id', $subCat)) return true;
						if($categories->contains('parent', $subCat))return true;
						
						
						
						
					}
				//}
				

				



	return $categories->contains('id', $categoryId);


    
	


			
		
				
			})->values();
		}

	

if ($request->get('lang', null) === 'ar') {
	$res = collect($res)->map(function ($p) {
		if (isset($p['name_ar']))
			$p['name'] = $p['name_ar'];
		if (isset($p['description_ar']))
			$p['description'] = $p['description_ar'];
		return $p;
	});
}

		// $paginate = 10;
		// $page = $request->get('page', 1);

		// $offSet = ($page * $paginate) - $paginate;
		// $itemsForCurrentPage = array_slice($res->toArray(), $offSet, $paginate, true);
		// $res = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($res), $paginate, $page);
		// $result = $res->toArray();
		$result = $res->values()->toArray();

		return $this->sendResponse($result, 'Products retrieved successfully');
	}
}