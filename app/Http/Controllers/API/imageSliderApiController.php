<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeviceAPIRequest;
use App\Http\Requests\API\UpdateDeviceAPIRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ImageSliderRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Importer;
use App\Models\Device;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;

/**
 * Class DeviceController
 * @package App\Http\Controllers\API
 */

class imageSliderApiController extends AppBaseController
{
    /** @var  DeviceRepository */
    private $imageSliderRepository;

	public function __construct(ImageSliderRepository $imageSliderRepo)
	{
		// $this->productRepository = $productRepo;
		$this->imageSliderRepository = $imageSliderRepo;
	}

    /**
     * Display a listing of the Device.
     * GET|HEAD /devices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->imageSliderRepository->pushCriteria(new RequestCriteria($request));
        $this->imageSliderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $images = $this->imageSliderRepository->orderBy('order')->all();

        return $this->sendResponse($images->toArray(), 'images slider retrieved successfully');
    }

    /**
     * Store a newly created Device in storage.
     * POST /devices
     *
     * @param CreateDeviceAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
		
    }

    /**
     * Display the specified Device.
     * GET|HEAD /devices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
       
    }

    /**
     * Update the specified Device in storage.
     * PUT/PATCH /devices/{id}
     *
     * @param  int $id
     * @param UpdateDeviceAPIRequest $request
     *
     * @return Response
     */
    public function update($id)
    {
    }

    /**
     * Remove the specified Device from storage.
     * DELETE /devices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
      
    }
}
