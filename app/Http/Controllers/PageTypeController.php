<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatePageTypeRequest;
use App\Http\Requests\UpdatePageTypeRequest;
use App\Repositories\PageTypeRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PageTypeController extends AppBaseController
{
  /** @var  PageTypeRepository */
  private $pageTypeRepository;

  public function __construct(PageTypeRepository $pageTypeRepo)
  {
		$this->middleware('auth');
    $this->pageTypeRepository = $pageTypeRepo;
  }

  /**
   * Display a listing of the PageType.
   *
   * @param Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    $this->pageTypeRepository->pushCriteria(new RequestCriteria($request));
    $pageTypes = $this->pageTypeRepository->all();

    return view('page_types.index')
      ->with('pageTypes', $pageTypes);
  }

  /**
   * Show the form for creating a new PageType.
   *
   * @return Response
   */
  public function create()
  {
    return view('page_types.create');
  }

  /**
   * Store a newly created PageType in storage.
   *
   * @param CreatePageTypeRequest $request
   *
   * @return Response
   */
  public function store(CreatePageTypeRequest $request)
  {
    $input = $request->all();

    $pageType = $this->pageTypeRepository->create($input);

    Flash::success('Page Type saved successfully.');

    return redirect(route('pageTypes.index'));
  }

  /**
   * Display the specified PageType.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
    $pageType = $this->pageTypeRepository->findWithoutFail($id);

    if (empty($pageType)) {
      Flash::error('Page Type not found');

      return redirect(route('pageTypes.index'));
    }

    return view('page_types.show')->with('pageType', $pageType);
  }

  /**
   * Show the form for editing the specified PageType.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    $pageType = $this->pageTypeRepository->findWithoutFail($id);

    if (empty($pageType)) {
      Flash::error('Page Type not found');

      return redirect(route('pageTypes.index'));
    }

    return view('page_types.edit')->with('pageType', $pageType);
  }

  /**
   * Update the specified PageType in storage.
   *
   * @param  int              $id
   * @param UpdatePageTypeRequest $request
   *
   * @return Response
   */
  public function update($id, UpdatePageTypeRequest $request)
  {
    $pageType = $this->pageTypeRepository->findWithoutFail($id);

    if (empty($pageType)) {
      Flash::error('Page Type not found');

      return redirect(route('pageTypes.index'));
    }

    $pageType = $this->pageTypeRepository->update($request->all(), $id);

    Flash::success('Page Type updated successfully.');

    return redirect(route('pageTypes.index'));
  }

  /**
   * Remove the specified PageType from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $pageType = $this->pageTypeRepository->findWithoutFail($id);

    if (empty($pageType)) {
      Flash::error('Page Type not found');

      return redirect(route('pageTypes.index'));
    }

    $this->pageTypeRepository->delete($id);

    Flash::success('Page Type deleted successfully.');

    return redirect(route('pageTypes.index'));
  }
}
