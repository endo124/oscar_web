<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Customer;
class LoginChatController extends AppBaseController
{
    //
    public function index()
{
  return view('auth.login_chat');
}

public function login(Request $request)
{
    $validator = Validator::make($request->all(), [
        'email'=>'required',         
        'password' => 'required',
         ]);
       
        //check if payload is valid before moving on
        if ($validator->fails()) {

            return \Redirect::back()->withErrors($validator);       
                  }
    if (Auth::guard('customers')->attempt(['email' => $request->email, 'password' => $request->password])) {
        $customer = Auth::guard('customers')->user();
        if($customer->type !='customer-agent')
        {
            return   \Redirect::back()->withErrors(['password' => 'You are not allowed to access this page']);
 
        }
       
        $customer->update(['status'=>'available']);
        $token=auth('customers')->login($customer);
        $customer->where('id',$customer->id)->update([
            'token' => $token
           ]);
        return view('chat.users_chat');
    } else {
        return   \Redirect::back()->withErrors(['password' => 'There is something wrong in email or password']);
    }
}

public function logout(Request $request,$id)
	{
        $customer=Customer::find($id);
        auth('customers')->login($customer);
        \Auth::guard('customers')->logout($customer);
        $customer->update(['status'=>'unavailable']);
    $online=\DB::table('online_now')->select('online_number')->first();
 
    if($online->online_number==1)
    {
        \DB::table('online_now')->delete();
        \DB::table('online_now')->insert(['online_number'=>0]);
    }
		

   

		return redirect('/login_chat');
	}

}
