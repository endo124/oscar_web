<?php namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatePageContentRequest;
use App\Http\Requests\UpdatePageContentRequest;
use App\Repositories\PageContentRepository;
use App\Repositories\PageRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PageContentController extends AppBaseController
{
	/** @var  PageContentRepository */
	private $pageContentRepository;

	public function __construct(PageContentRepository $pageContentRepo, PageRepository $pageRepo)
	{
		$this->middleware('auth');
		$this->pageRepository = $pageRepo;
		$this->pageContentRepository = $pageContentRepo;
	}

	/**
	 * Display a listing of the PageContent.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->pageContentRepository->pushCriteria(new RequestCriteria($request));
		$pageContents = $this->pageContentRepository->all();

		return view('page_contents.index')
			->with('pageContents', $pageContents);
	}

	/**
	 * Show the form for creating a new PageContent.
	 *
	 * @return Response
	 */
	public function create()
	{
		$pages = $this->pageRepository->pluck('title', 'id');
		return view('page_contents.create')->with('pages', $pages);
	}

	/**
	 * Store a newly created PageContent in storage.
	 *
	 * @param CreatePageContentRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePageContentRequest $request)
	{
		$input = $request->all();

		$pageContent = $this->pageContentRepository->create($input);

		Flash::success('Page Content saved successfully.');

		return redirect(route('pageContents.index'));
	}

	/**
	 * Display the specified PageContent.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$pageContent = $this->pageContentRepository->findWithoutFail($id);

		if (empty($pageContent)) {
			Flash::error('Page Content not found');

			return redirect(route('pageContents.index'));
		}

		return view('page_contents.show')->with('pageContent', $pageContent);
	}

	/**
	 * Show the form for editing the specified PageContent.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$pages = $this->pageRepository->pluck('title', 'id');
		$pageContent = $this->pageContentRepository->findWithoutFail($id);

		if (empty($pageContent)) {
			Flash::error('Page Content not found');

			return redirect(route('pageContents.index'));
		}

		return view('page_contents.edit')->with('pageContent', $pageContent)->with('pages', $pages);
	}

	/**
	 * Update the specified PageContent in storage.
	 *
	 * @param  int              $id
	 * @param UpdatePageContentRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePageContentRequest $request)
	{
		$pageContent = $this->pageContentRepository->findWithoutFail($id);

		if (empty($pageContent)) {
			Flash::error('Page Content not found');

			return redirect(route('pageContents.index'));
		}

		$pageContent = $this->pageContentRepository->update($request->all(), $id);

		Flash::success('Page Content updated successfully.');

		return redirect(route('pageContents.index'));
	}

	/**
	 * Remove the specified PageContent from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$pageContent = $this->pageContentRepository->findWithoutFail($id);

		if (empty($pageContent)) {
			Flash::error('Page Content not found');

			return redirect(route('pageContents.index'));
		}

		$this->pageContentRepository->delete($id);

		Flash::success('Page Content deleted successfully.');

		return redirect(route('pageContents.index'));
	}
}
