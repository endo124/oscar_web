<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UserController extends AppBaseController
{
  /** @var  UserRepository */
  private $userRepository;

  public function __construct(UserRepository $userRepo, RoleRepository $roleRepo)
  {
		$this->middleware(['auth']);
    $this->userRepository = $userRepo;
    $this->roleRepository = $roleRepo;
  }

  /**
   * Display a listing of the User.
   *
   * @param Request $request
   * @return Response
   */
  public function index(Request $request)
  {
		$this->authorize('users.index');
    $this->userRepository->pushCriteria(new RequestCriteria($request));
    $users = $this->userRepository->all();

    return view('users.index')
      ->with('users', $users);
  }

  /**
   * Show the form for creating a new User.
   *
   * @return Response
   */
  public function create()
  {
		$this->authorize('users.create');
		$roles = $this->roleRepository->pluck('name', 'id');
    return view('users.create')->with('roles', $roles);
  }

  /**
   * Store a newly created User in storage.
   *
   * @param CreateUserRequest $request
   *
   * @return Response
   */
  public function store(CreateUserRequest $request)
  {
		$this->authorize('users.create');
    $input = $request->all();
    $user = $this->userRepository->create($input);
		if ($input['roles']) {
			$user->syncRoles($input['roles']); //Assigning role to user
			unset($input['roles']);
		}

    Flash::success('User saved successfully.');

    return redirect(route('users.index'));
  }

  /**
   * Display the specified User.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
		$this->authorize('users.show');
    $user = $this->userRepository->findWithoutFail($id);

    if (empty($user)) {
      Flash::error('User not found');

      return redirect(route('users.index'));
    }

    return view('users.show')->with('user', $user);
  }

  /**
   * Show the form for editing the specified User.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
		$this->authorize('users.edit');
    $user = $this->userRepository->findWithoutFail($id);
		$roles = $this->roleRepository->pluck('name', 'id');
    if (empty($user)) {
      Flash::error('User not found');

      return redirect(route('users.index'));
    }

    return view('users.edit')->with('user', $user)->with('roles', $roles);
  }

  /**
   * Update the specified User in storage.
   *
   * @param  int              $id
   * @param UpdateUserRequest $request
   *
   * @return Response
   */
  public function update($id, UpdateUserRequest $request)
  {
		$this->authorize('users.edit');
    $user = $this->userRepository->findWithoutFail($id);

    if (empty($user)) {
      Flash::error('User not found');

      return redirect(route('users.index'));
    }

    $fields = $request->all();
    if ($fields['roles']) {
			$user->syncRoles($fields['roles']); //Assigning role to user
      unset($fields['roles']);
		}
    if (!$fields['password']) {
      unset($fields['password']);
		}

    $user = $this->userRepository->update($fields, $id);

    Flash::success('User updated successfully.');

    return redirect(route('users.index'));
  }

  /**
   * Remove the specified User from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
		$this->authorize('users.delete');
    $user = $this->userRepository->findWithoutFail($id);

    if (empty($user)) {
      Flash::error('User not found');

      return redirect(route('users.index'));
    }

    $this->userRepository->delete($id);

    Flash::success('User deleted successfully.');

    return redirect(route('users.index'));
  }
}
