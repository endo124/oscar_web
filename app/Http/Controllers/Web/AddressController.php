<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Customer;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $id = \Cookie::get('Branch_id');

        $all=collect();
    
    
            // dd(isset(($_COOKIE['lat'])),$_COOKIE);
            if(isset($_COOKIE['lat'])){
            $lat1= $_COOKIE['lat'];
                
            }else{
                $lat1= 31.3655877;
            }
            if(isset($_COOKIE['long'])){
            $lon1=$_COOKIE['long'];
    
            }else{
                $lon1= 31.3655877;
    
            }
            $branches=Branch::where('status','open')->get();
    
            foreach($branches as $branch)
            {
                $lat2=$branch['latitudes'];
                $lon2=$branch['longitudes'];
                $theta = $lon1 - $lon2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $km = $miles * 1.609344;
                $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
            }
            $all->sortBy('km')->values()->toArray();
        
            $nearst=$all[0];
    
            if($id){
                $nearst['store_id']=$id;
            }
            $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
            $store_id=$nearst['store_id'];
            if(is_null($store_id))
            {
                $store_id=01;
            }
            $countId='count_';
            switch($store_id)
            {
            case 01:$countId.=1;
            break;
            case 02:$countId.=2;
            break;
            case 04:$countId.=4;
    
            }
            $cat=[];
            if($store_id !='04'){
                $categories=Category::where($countId , '!=' , 0 )->get();
    
            }else{
                $cat=Category::where('id','5637168576')->first();
            }
        return view('website.create_address',compact('categories','branches','branch','cat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$lang)
    {
        if($request->coordinates == null){
            // \Session::flash('message', 'You Must Enable Location Services for Your Browser'); 
            // return back();
            $request->coordinates='30.033333,31.233334';
        }
        $validate=$request->validate([
            'phone'=>'required',
            'name'=>'required',
            'address'=>'required',
            'area'=>'required',
            'city'=>'required',
            'coordinates'=>'required',
        ]);

        $address=Address::Create([
            'name'=>$request->name,
            'address'=>$request->address,
            'area'=>$request->area,
            'city'=>$request->city,
            'customer_id'=>auth()->guard('customerForWeb')->user()->id,
            'phone'=>$request->phone,
            'coordinates'=>$request->coordinates
        ]);
      
        return redirect($lang.'/profile');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang,$id)
    {
        $address=Address::find($id);
        $coordinates=json_encode($address->coordinates);
        $id = \Cookie::get('Branch_id');

        $all=collect();
    
    
            // dd(isset(($_COOKIE['lat'])),$_COOKIE);
            if(isset($_COOKIE['lat'])){
            $lat1= $_COOKIE['lat'];
                
            }else{
                $lat1= 31.3655877;
            }
            if(isset($_COOKIE['long'])){
            $lon1=$_COOKIE['long'];
    
            }else{
                $lon1= 31.3655877;
    
            }
            $branches=Branch::where('status','open')->get();
    
            foreach($branches as $branch)
            {
                $lat2=$branch['latitudes'];
                $lon2=$branch['longitudes'];
                $theta = $lon1 - $lon2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $km = $miles * 1.609344;
                $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
            }
            $all->sortBy('km')->values()->toArray();
        
            $nearst=$all[0];
    
            if($id){
                $nearst['store_id']=$id;
            }
            $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
            $store_id=$nearst['store_id'];
            if(is_null($store_id))
            {
                $store_id=01;
            }
            $countId='count_';
            switch($store_id)
            {
            case 01:$countId.=1;
            break;
            case 02:$countId.=2;
            break;
            case 04:$countId.=4;
    
            }
    
            if($store_id !='04'){
                $categories=Category::where($countId , '!=' , 0 )->get();
    
            }else{
                $cat=Category::where('id','5637168576')->first();
            }
       
        // $cords=explode(",", str_replace('"', '', $coordinates));
        // $cords=json_encode($cords);
        return view('website.update_address', compact('address','branch','categories','branches','coordinates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$lang, $id)
    {
        $address=Address::find($id);
        $validate=$request->validate([
            'phone'=>'required|unique:address,id,'.$address->id,
            'name'=>'required',
            'address'=>'required',
            'area'=>'required',
            'city'=>'required',
            'coordinates'=>'required',
        ]);
        $address=$address->Update([
            'name'=>$request->name,
            'address'=>$request->address,
            'area'=>$request->area,
            'city'=>$request->city,
            'customer_id'=>auth()->guard('customerForWeb')->user()->id,
            'phone'=>$request->phone,
            'coordinates'=>$request->coordinates
        ]);
      
        return redirect($lang.'/profile');

    }


    public function getAddress( Request $request){
        $address=Address::find($request->id);
        return response()->json($address);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang,$id)
    {
        $address=Address::find($id);
        $address->delete();
        return response()->json($address);
    }
}
