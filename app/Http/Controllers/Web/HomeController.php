<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Banner;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Customer;
use App\Models\ImageSlider;
use App\Models\Message;
use App\Models\OrderHistory;
use App\Models\Product;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use GuzzleHttp\Client;
use Carbon\Carbon;


class HomeController extends Controller

{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function redirect(){
      
        return $this->index(app()->getLocale(),'01');


     }

     public function show_product( $lang,$id_product )
     { 
        $id = \Cookie::get('Branch_id');

        $all=collect();
    
    
            // dd(isset(($_COOKIE['lat'])),$_COOKIE);
            if(isset($_COOKIE['lat'])){
            $lat1= $_COOKIE['lat'];
                
            }else{
                $lat1= 31.3655877;
            }
            if(isset($_COOKIE['long'])){
            $lon1=$_COOKIE['long'];
    
            }else{
                $lon1= 31.3655877;
    
            }
            $branches=Branch::where('status','open')->get();
    
            foreach($branches as $branch)
            {
                $lat2=$branch['latitudes'];
                $lon2=$branch['longitudes'];
                $theta = $lon1 - $lon2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $km = $miles * 1.609344;
                $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
            }
            $all->sortBy('km')->values()->toArray();
        
            $nearst=$all[0];
    
            if($id){
                $nearst['store_id']=$id;
            }
            $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
            $store_id=$nearst['store_id'];
            if(is_null($store_id))
            {
                $store_id=01;
            }
            $countId='count_';
            switch($store_id)
            {
            case 01:$countId.=1;
            break;
            case 02:$countId.=2;
            break;
            case 04:$countId.=4;
    
            }$cat=[];
    
            if($store_id !='04'){
                $categories=Category::where($countId , '!=' , 0 )->where('parent' , null)->get();
            }else{

                $cat=Category::where('id','5637168576')->first();
            }
       $customer=null;
       $pros=array();
        if(auth()->guard('customerForWeb')->user()){
            $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
           
            foreach($customer->products as $pro){

                array_push($pros,$pro->id);
            }
        }
        $product=Product::where('id' ,$id_product)->first();
        if($lang=='ar')
        {
            $product=Product::where('id' ,$id_product)->select(['*','name_ar as name','description_ar as description'])->first();
        }

        return view('website.show_product',compact('product','branches','branch','cat','categories','pros','customer'));


     }
    public function index( $lang,$id )
    {   
       
        setcookie("Branch_id", $id);
        \Cookie::queue('Branch_id', $id);
    
        $slider=ImageSlider::where('type','web')->first();
        $banners=Banner::where('type','web')->get();

        $all=collect();

    
        if(isset($_COOKIE['lat'])){
		$lat1= $_COOKIE['lat'];
            
        }else{
            $lat1= 31.3655877;
        }
        if(isset($_COOKIE['long'])){
        $lon1=$_COOKIE['long'];

        }else{
            $lon1= 31.3655877;

        }
        $branches=Branch::where('status','open')->get();

		foreach($branches as $branch)
		{
			$lat2=$branch['latitudes'];
			$lon2=$branch['longitudes'];
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$km = $miles * 1.609344;
            $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
    

        // $customer=Customer::where('id',38)->first();
        
        $customer=null;
        $pros=array();
        if(auth()->guard('customerForWeb')->user()){
            $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
           
            foreach($customer->products as $pro){

                array_push($pros,$pro->id);
            }

        }
        $nearst=$all[0];

        if($id){
            $nearst['store_id']=$id;
        }
        $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
        $store_id=$nearst['store_id'];
        if(is_null($store_id))
        {
            $store_id=01;
        }
        $countId='count_';
        
        switch($store_id)
        {
        case 01:$countId.=1;
        break;
        case 02:$countId.=2;
        break;
        case 04:$countId.=4;

        }
        $cat=[];
         $categories=[];
        if($store_id !='04'){
            $categories=Category::where($countId , '!=' , 0 )->get();

        }else{
            $cat=Category::where('id','5637168576')->first();
        }
       
        $offers=Product::where('on_sale' , '1')
        ->where($store_id , 'true')
        ->get();
        if($lang=='ar')
        {
            $offers=Product::where('on_sale' , '1')
            ->where($store_id , 'true')->select(['*','name_ar as name','description_ar as description'])->get();
        }
        $offers=$offers->unique('barcode');
 
        return view('website.index',compact('categories','cat','customer','branches','branch','slider','banners','offers','pros','nearst'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lang= app()->getLocale();
        if($request->coordinates == null){

            return response()->json(['error' => true,
             'message' => 'You Must Enable Location Services for Your Browser'],500);
            
        }
        // else
        // {
            // $request->coordinates='30.0653269,31.4707391';
            $validator = \Validator::make($request->all(), [
                'local_storage' => 'required',
                'total_price' => 'required',
                'name' => 'required',
                'email' => 'required',
                'number' => 'required|regex:/(01)[0-9]{9}/|numeric',
                'city' => 'required',
                'area' => 'required',
                // 'coordinates' => 'required',
            ]);
            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }
            if ($request->pay == null) {
                $cords=explode(",",  $request->coordinates);
                $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
                $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri('https://logista-282218.firebaseio.com/')
                ->create();
                $database = $firebase->getDatabase();
                $getUser = $database
                ->getReference('users')
                ->orderByChild('email')
                ->equalTo('hadeel.mostafa@momentum-sol.com')
                ->getValue();
                $user=array_keys($getUser);
                $getZones = $database
                ->getReference('/users/'.$user[0].'/geoFences')

                ->getValue();
                $zones=(array_values($getZones));
                $arr_check=[];
                foreach($zones as $zo)
                {
                    $arr_x=[];
                $arr_y=[];
                foreach($zo['cords'] as $zone)
                {
                    array_push($arr_x,$zone['lat']);
                    array_push($arr_y,$zone['lng']);
                    
                }
                $points_polygon = count($arr_x) - 1;  // number vertices - zero-based array
        
                $check=$this->is_in_polygon($points_polygon, $arr_x, $arr_y, $cords[0],$cords[1]);
                
                if ($check){
                    array_push($arr_check,$zo);
                }
                }
                // dd(count($arr_check) ,$arr_check ,$cords[0],$cords[1]);
                if($arr_check ==[])
                {
                    
                    return response()->json(['errors' =>['message'=>'Out Of Zones'] ]);

                }

                return response()->json([
                    'success' => true,
                    'message' => 'Get Orders successfully.',
                    'data' => array_values($arr_check[0]['flags']),
                ], 200);
                

            }
            else{
                $data = $request->all();

                $id = \Cookie::get('Branch_id');
                $proArray=[];
                $branch=Branch::where('store_id', $id)->first();
                $client = new Client();

                $products=json_decode($data['local_storage'],true);
                
                foreach($products as  $pro)
                {
                    $pro_name=array_keys($pro)[0];
                    // dd($pro[$pro_name]['qty']);
                    if(! empty($pro[$pro_name]['qty'])){
                        array_push($proArray,array(
                            "product_id" => $pro[$pro_name]['id'],
                            "quantity" => $pro[$pro_name]['qty'],
                    ));
                    }else{
                        array_push($proArray,array(
                            "product_id" => $pro[$pro_name]['id'],
                            "quantity" => 1,
                        ));
        
                    }
                    
                }
                if($request->has('delivery_date') && $request->delivery_date != null)
                {
                    $date=strtotime($request->delivery_date);
                    $date= date('d/m/Y h:m a', $date);
                    
                }else{
                    $date= date('d/m/Y h:m a');
                }
                
                $order_request = $client->request('POST', 'http://oscar.momentum-sol.com/api/orders', [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'json' => [
                        "token"=>\Auth::guard('customerForWeb')->user()->token,
                        "set_paid"=>false,
                        "address"=>[
                            "name"=>$request['name'],
                            "address"=>$request['address'],
                            "city"=>$request['city'],
                            "area"=>$request['area'],
                        ],
                        "coordinates_new"=>$request->coordinates,
                        "notes"=>$request->notes,
                        "phone"=>$request->number,
                        "payment_method"=>$request->pay,
                        "line_items"=>$proArray,
                        "cost"=>$request->cost,
                        "flag"=>$request->flag,
                        "delivery_date"=>$date,
                        "store_id"=>$id,
                    ],
                ]);

                $data=json_decode($order_request->getBody());
                if(true)
                { 
                     $url='/html/oscar_web/public/'.$lang;

                    return response()->json(['success' => true, 'message' => 'Order Sent Successfully','redirect' => $url.'/homeWeb/branch/'.$id]);
                }else{
                    
                    return response()->json(['error' => true, 'message' => 'Order Sent Failed']);
        
                }
            }

        // }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang,$slug)
    {

 

    $id = \Cookie::get('Branch_id');

    $all=collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if(isset($_COOKIE['lat'])){
		$lat1= $_COOKIE['lat'];
            
        }else{
            $lat1= 31.3655877;
        }
        if(isset($_COOKIE['long'])){
        $lon1=$_COOKIE['long'];

        }else{
            $lon1= 31.3655877;

        }
        $branches=Branch::where('status','open')->get();

		foreach($branches as $branch)
		{
			$lat2=$branch['latitudes'];
			$lon2=$branch['longitudes'];
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$km = $miles * 1.609344;
            $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
    
        $nearst=$all[0];

        if($id){
            $nearst['store_id']=$id;
        }
        $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
        $store_id=$nearst['store_id'];
        if(is_null($store_id))
        {
            $store_id=01;
        }
        $countId='count_';
        switch($store_id)
        {
        case 01:$countId.=1;
        break;
        case 02:$countId.=2;
        break;
        case 04:$countId.=4;

        }
$cat=[];
$customer=null;
$pros=array();
        if($store_id !='04'){
            $categories=Category::where($countId , '!=' , 0 )->get();

        }else{
            $cat=Category::where('id','5637168576')->first();
        }
        if(auth()->guard('customerForWeb')->user()){
            $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
           
            foreach($customer->products as $pro){
                array_push($pros,$pro->id);
            }
        }
        

        $categories=Category::all();//to show all categories in category list
        $category=Category::where('slug',$slug)->first();//to show selected category info

        $products=Product::where('category_id',$category->id)->paginate(36);//to show children products of these category


        return view('website.products',compact('category','cat','categories','products','customer','pros','branches','branch'));
    }




    public function showallproducts(Request $request, $lang,$id) //to show all children products of these category
    {   

  
    $cookie_id = \Cookie::get('Branch_id');

    $all=collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if(isset($_COOKIE['lat'])){
		$lat1= $_COOKIE['lat'];
            
        }else{
            $lat1= 31.3655877;
        }
        if(isset($_COOKIE['long'])){
        $lon1=$_COOKIE['long'];

        }else{
            $lon1= 31.3655877;

        }
        $branches=Branch::where('status','open')->get();

		foreach($branches as $branch)
		{
			$lat2=$branch['latitudes'];
			$lon2=$branch['longitudes'];
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$km = $miles * 1.609344;
            $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
    
        $nearst=$all[0];

        if($cookie_id){
            $nearst['store_id']=$cookie_id;
        }
        $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
        $store_id=$nearst['store_id'];
        if(is_null($store_id))
        {
            $store_id=01;
        }
        $countId='count_';
        switch($store_id)
        {
        case 01:$countId.=1;
        break;
        case 02:$countId.=2;
        break;
        case 04:$countId.=4;

        }
 $cat=[];
        if($store_id !='04'){
            $categories=Category::where($countId , '!=' , 0 )->get();

        }else{
            $cat=Category::where('id','5637168576')->first();
        }

        $products=Product::all();
       
        if($lang=='ar')
        {
            $products=Product::select(['*','name_ar as name','description_ar as description'])->get();
        }
        $category = Category::find($id);

        $res = collect($products)->filter(function ($p) use ($category, $id) {
    
        $categories = collect($p['categories']);

        $subCategories = $category->children->pluck('id')->toArray();
        
        foreach ($subCategories as $subCat) {
    
            if ($categories->contains('id', $subCat)) return true;
            if($categories->contains('parent', $subCat))return true;

        }
        
        return $categories->contains('id', $id);})->values();
        
        $pros=array();
        if(auth()->guard('customerForWeb')->user()){
            $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
           
            foreach($customer->products as $pro){

                array_push($pros,$pro->id);
            }

        }
        if(!isset($request->page)){
            $request->page=1;
            
        }
        $res=$this->paginate($res,$category->id,$request->page);

        return view('website.products',compact('category','cat','categories','res','branches','branch','pros'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function paginate($items,$cat,$page)
    {
        $perPage = 36; 
        $options=['path' =>'/html/oscar_web/public/homeWeb/all_products/'.$cat,'pageName'=>'page' ];
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }


    public function wishList(Request $request){

        $product=Product::where('id',$request->id)->first();
        $customer_id=auth()->guard('customerForWeb')->user()->id;
        $customer=Customer::where('id',$customer_id)->first();
        $customer->products()->attach($product);
        return response()->json($product);

    }

    public function unLike(Request $request){
        $product=Product::where('id',$request->id)->first();
        $customer_id=auth()->guard('customerForWeb')->user()->id;
        $customer=Customer::where('id',$customer_id)->first();
        $customer->products()->detach($product);

        return response()->json($product->id);

    }


 
    public function search(Request $request,$lang)
	{
     
 
        $cookie_id = \Cookie::get('Branch_id');

        $all=collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if(isset($_COOKIE['lat'])){
		$lat1= $_COOKIE['lat'];
            
        }else{
            $lat1= 31.3655877;
        }
        if(isset($_COOKIE['long'])){
        $lon1=$_COOKIE['long'];

        }else{
            $lon1= 31.3655877;

        }
        $branches=Branch::where('status','open')->get();

		foreach($branches as $branch)
		{
			$lat2=$branch['latitudes'];
			$lon2=$branch['longitudes'];
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$km = $miles * 1.609344;
            $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
    
        $nearst=$all[0];

        if($cookie_id){
            $nearst['store_id']=$cookie_id;
        }
        $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
        $store_id=$nearst['store_id'];
        if(is_null($store_id))
        {
            $store_id=01;
        }
        $countId='count_';
        switch($store_id)
        {
        case 01:$countId.=1;
        break;
        case 02:$countId.=2;
        break;
        case 04:$countId.=4;

        }
$cat=[];

        if($store_id !='04'){
            $categories=Category::where($countId , '!=' , 0 )->get();

        }else{
            $cat=Category::where('id','5637168576')->first();
        }

        
        
        $search=$request->get('search');

        if($lang=='ar')
        {
            $products=Product::where(function ($q) use ($search){

                $q->where("name", "LIKE","%".$search."%")
                ->orWhere("description","LIKE","%".$search."%")
                ->orWhere("name_ar", "LIKE","%".$search."%")
                ->orWhere("description_ar", "LIKE","%".$search."%");
            })->select(['*','name_ar as name','description_ar as description'])->get();
        }
        else
        {
            $products=Product::where(function ($q) use ($search){

                $q->where("name", "LIKE","%".$search."%")
                ->orWhere("description","LIKE","%".$search."%")
                ->orWhere("name_ar", "LIKE","%".$search."%")
                ->orWhere("description_ar", "LIKE","%".$search."%");
            })->get();
        }
     
        $products=$products->unique('barcode');
        $pros=array();
        if(auth()->guard('customerForWeb')->user()){
            $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
            
            foreach($customer->products as $pro){

                array_push($pros,$pro->id);
            }

        }



        return view('website.product_search',compact('categories','cat','branch','branches','products','pros'));
    }

  
   

    public function cart()
    { 

        if(auth()->guard('customerForWeb')->user()){
            $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
            $pros=array();
            foreach($customer->products as $pro){

                array_push($pros,$pro->id);
            }

        }
        $cookie_id = \Cookie::get('Branch_id');

        $all=collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if(isset($_COOKIE['lat'])){
		$lat1= $_COOKIE['lat'];
            
        }else{
            $lat1= 31.3655877;
        }
        if(isset($_COOKIE['long'])){
        $lon1=$_COOKIE['long'];

        }else{
            $lon1= 31.3655877;

        }
        $branches=Branch::where('status','open')->get();

		foreach($branches as $branch)
		{
			$lat2=$branch['latitudes'];
			$lon2=$branch['longitudes'];
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$km = $miles * 1.609344;
            $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
    
        $nearst=$all[0];

        if($cookie_id){
            $nearst['store_id']=$cookie_id;
        }
        $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
        $store_id=$nearst['store_id'];
        if(is_null($store_id))
        {
            $store_id=01;
        }
        $countId='count_';
        switch($store_id)
        {
        case 01:$countId.=1;
        break;
        case 02:$countId.=2;
        break;
        case 04:$countId.=4;

        }
        $cat=[];
        if($store_id !='04'){
            $categories=Category::where($countId , '!=' , 0 )->get();

        }else{
            $cat=Category::where('id','5637168576')->first();
        }

        $products=Product::all();
        return view('website.cart',compact('products','categories','cat','branch','branches','pros'));
    }
    
    public function addToCart(Request $request)
    {

        $product=Product::where('id',$request->id)->first();

        return response()->json($product);

    }


    public function checkout()
    {
        if(auth()->guard('customerForWeb')->user()){
            $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
            $pros=array();
            foreach($customer->products as $pro){

                array_push($pros,$pro->id);
            }

        }

        $id = \Cookie::get('Branch_id');

        $all=collect();
    
    
            // dd(isset(($_COOKIE['lat'])),$_COOKIE);
            if(isset($_COOKIE['lat'])){
            $lat1= $_COOKIE['lat'];
                
            }else{
                $lat1= 31.3655877;
            }
            if(isset($_COOKIE['long'])){
            $lon1=$_COOKIE['long'];
    
            }else{
                $lon1= 31.3655877;
    
            }
            $branches=Branch::where('status','open')->get();
    
            foreach($branches as $branch)
            {
                $lat2=$branch['latitudes'];
                $lon2=$branch['longitudes'];
                $theta = $lon1 - $lon2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $km = $miles * 1.609344;
                $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
            }
            $all->sortBy('km')->values()->toArray();
        
            $nearst=$all[0];
    
            if($id){
                $nearst['store_id']=$id;
            }
            $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
            $store_id=$nearst['store_id'];
            if(is_null($store_id))
            {
                $store_id=01;
            }
            $countId='count_';
            switch($store_id)
            {
            case 01:$countId.=1;
            break;
            case 02:$countId.=2;
            break;
            case 04:$countId.=4;
    
            }
            $cat=[];
            if($store_id !='04'){
                $categories=Category::where($countId , '!=' , 0 )->get();
    
            }else{
                $cat=Category::where('id','5637168576')->first();
            }
        $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();

        $products=Product::all();
        $addresses=Address::where('customer_id',$customer->id)->get();
        return view('website.checkout',compact('categories','cat','products','customer','branches','addresses','branch','pros'));

    }

    public function contact(){
    $id = \Cookie::get('Branch_id');

    $all=collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if(isset($_COOKIE['lat'])){
		$lat1= $_COOKIE['lat'];
            
        }else{
            $lat1= 31.3655877;
        }
        if(isset($_COOKIE['long'])){
        $lon1=$_COOKIE['long'];

        }else{
            $lon1= 31.3655877;

        }
        $branches=Branch::where('status','open')->get();

		foreach($branches as $branch)
		{
			$lat2=$branch['latitudes'];
			$lon2=$branch['longitudes'];
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$km = $miles * 1.609344;
            $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
    
        $nearst=$all[0];

        if($id){
            $nearst['store_id']=$id;
        }
        $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
        $store_id=$nearst['store_id'];
        if(is_null($store_id))
        {
            $store_id=01;
        }
        $countId='count_';
        switch($store_id)
        {
        case 01:$countId.=1;
        break;
        case 02:$countId.=2;
        break;
        case 04:$countId.=4;

        }
         $cat=[];
        if($store_id !='04'){
            $categories=Category::where($countId , '!=' , 0 )->get();

        }else{
            $cat=Category::where('id','5637168576')->first();
        }
        $pros=array();
        if(auth()->guard('customerForWeb')->user()){
            $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
          
            foreach($customer->products as $pro){

                array_push($pros,$pro->id);
            }

        }
       
        return view('website.contact',compact('categories','cat','branch','branches','pros'));
    }
    public function aboutus(){
        $id = \Cookie::get('Branch_id');

        $all=collect();
    
    
            // dd(isset(($_COOKIE['lat'])),$_COOKIE);
            if(isset($_COOKIE['lat'])){
            $lat1= $_COOKIE['lat'];
                
            }else{
                $lat1= 31.3655877;
            }
            if(isset($_COOKIE['long'])){
            $lon1=$_COOKIE['long'];
    
            }else{
                $lon1= 31.3655877;
    
            }
            $branches=Branch::where('status','open')->get();
    
            foreach($branches as $branch)
            {
                $lat2=$branch['latitudes'];
                $lon2=$branch['longitudes'];
                $theta = $lon1 - $lon2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $km = $miles * 1.609344;
                $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
            }
            $all->sortBy('km')->values()->toArray();
        
            $nearst=$all[0];
    
            if($id){
                $nearst['store_id']=$id;
            }
            $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
            $store_id=$nearst['store_id'];
            if(is_null($store_id))
            {
                $store_id=01;
            }
            $countId='count_';
            switch($store_id)
            {
            case 01:$countId.=1;
            break;
            case 02:$countId.=2;
            break;
            case 04:$countId.=4;
    
            }
    $cat=[];
            if($store_id !='04'){
                $categories=Category::where($countId , '!=' , 0 )->get();
    
            }else{
                $cat=Category::where('id','5637168576')->first();
            }
            $pros=array();
            if(auth()->guard('customerForWeb')->user()){
                $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
               
                foreach($customer->products as $pro){
    
                    array_push($pros,$pro->id);
                }
    
            }
         

        return view('website.about-us',compact('categories','cat','branch','branches','pros'));
    }





    public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
	{
	  $i = $j = $c = 0;
	  for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
		if ( (($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
		 ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
		   $c = !$c;
	  }
	  return $c;
    }
    


}
