<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatePageItemRequest;
use App\Http\Requests\UpdatePageItemRequest;
use App\Repositories\CategoryRepository;
use App\Repositories\PageItemRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PageItemController extends AppBaseController
{
  /** @var  PageItemRepository */
  private $pageItemRepository;

  public function __construct(PageItemRepository $pageItemRepo, CategoryRepository $categoryRepo)
  {
		$this->middleware('auth');
    $this->pageItemRepository = $pageItemRepo;
    $this->categoryRepository = $categoryRepo;
  }

  /**
   * Display a listing of the PageItem.
   *
   * @param Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    $this->pageItemRepository->pushCriteria(new RequestCriteria($request));
    $pageItems = $this->pageItemRepository->all();

    return view('page_items.index')
      ->with('pageItems', $pageItems);
  }

  /**
   * Show the form for creating a new PageItem.
   *
   * @return Response
   */
  public function create()
  {
    $categories = $this->categoryRepository->pluck('title', 'id');
    return view('page_items.create')->with('categories', $categories);
  }

  /**
   * Store a newly created PageItem in storage.
   *
   * @param CreatePageItemRequest $request
   *
   * @return Response
   */
  public function store(CreatePageItemRequest $request)
  {
    $input = $request->all();

    $pageItem = $this->pageItemRepository->create($input);

    Flash::success('Page Item saved successfully.');

    return redirect(route('pageItems.index'));
  }

  /**
   * Display the specified PageItem.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
    $pageItem = $this->pageItemRepository->findWithoutFail($id);

    if (empty($pageItem)) {
      Flash::error('Page Item not found');

      return redirect(route('pageItems.index'));
    }

    return view('page_items.show')->with('pageItem', $pageItem);
  }

  /**
   * Show the form for editing the specified PageItem.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    $pageItem = $this->pageItemRepository->findWithoutFail($id);
    $categories = $this->categoryRepository->pluck('title', 'id');

    if (empty($pageItem)) {
      Flash::error('Page Item not found');

      return redirect(route('pageItems.index'));
    }

    return view('page_items.edit')->with('pageItem', $pageItem)->with('categories', $categories);
  }

  /**
   * Update the specified PageItem in storage.
   *
   * @param  int              $id
   * @param UpdatePageItemRequest $request
   *
   * @return Response
   */
  public function update($id, UpdatePageItemRequest $request)
  {
    $pageItem = $this->pageItemRepository->findWithoutFail($id);

    if (empty($pageItem)) {
      Flash::error('Page Item not found');

      return redirect(route('pageItems.index'));
    }

    $pageItem = $this->pageItemRepository->update($request->all(), $id);

    Flash::success('Page Item updated successfully.');

    return redirect(route('pageItems.index'));
  }

  /**
   * Remove the specified PageItem from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $pageItem = $this->pageItemRepository->findWithoutFail($id);

    if (empty($pageItem)) {
      Flash::error('Page Item not found');

      return redirect(route('pageItems.index'));
    }

    $this->pageItemRepository->delete($id);

    Flash::success('Page Item deleted successfully.');

    return redirect(route('pageItems.index'));
  }
}
