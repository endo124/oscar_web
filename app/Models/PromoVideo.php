<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PromoVideo
 * @package App\Models
 * @version October 7, 2019, 2:52 pm UTC
 *
 * @property string title
 * @property string url
 * @property boolean is_active
 * @property integer views
 */
class PromoVideo extends Model
{
    use SoftDeletes;

    public $table = 'promo_videos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'url',
        'is_active',
        'views'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'url' => 'string',
        'is_active' => 'boolean',
        'views' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|unique:promo_videos,title',
        'url' => 'url'
    ];

    
}
