<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Promotion
 * @package App\Models
 * @version March 27, 2018, 5:31 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection categoryPromotion
 * @property string title
 * @property string type
 * @property string description
 * @property string image
 * @property string url
 * @property string price
 * @property string sale
 * @property dateTime start_date
 * @property dateTime end_date
 */
class Product extends Model
{
	




	public $table = 'products';



	public $fillable = [
		'id',
		'name',
		'description',
		'name_ar',
		'description_ar',
		'on_sale',
		'regular_price',
		'hot_price',
        'discountFrom',
        'discountto',
        'discountprice',
        'barcode',
        'PriceUnit',
        'in_stock',
        'category_id',
        'images',
        '01',
        '02',
		'04',
		'categories',
		'limit'
        
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */


	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'title' => 'required',
		'type' => 'required',
		'image' => 'file|image',
	];
	public function getCategoriesAttribute($json)
	{
	
		return  json_decode($json);
	}
	public function getIdAttribute($id)
	{
		return  $id;
	}
	public function getImagesAttribute($json)
	{
		
	
		if ($json) {
			$url = json_decode($json);
			
			if(count( $url)!= 0)
			{
			for($i=0;$i< count($url);$i++)
				{
					
				$img[$i] =$url[$i];
				}
							}
			else{
			$img[0]['src']=url('/images/image.png');
			}
					

			return $img;
		}
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 **/
	public function categories_all()
	{
		return $this->belongsTo(\App\Models\Category::class, 'category_id');
	}

	public function customers(){
		return $this->belongsToMany(\App\Models\Customer::class,'customer_product','product_id','customer_id');
	}
}
