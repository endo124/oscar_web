<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Restaurant
 * @package App\Models
 * @version March 26, 2018, 3:40 pm UTC
 *
 * @property string title
 * @property string image
 * @property string description
 * @property string emails
 * @property string menu
 */
class Restaurant extends Model
{
	use SoftDeletes;
	
	use \igaster\TranslateEloquent\TranslationTrait;

	// use \igaster\TranslateEloquent\TranslationTrait {
	// 	boot as bootTranslations;
	// }

	// protected static function boot() {
	// 	parent::boot();
	// 	self::bootTranslations();
	// }

	protected static $translatable = [
		'm_title',
		'm_description',
		'm_image',
		'm_menu'
	];

	public $table = 'restaurants';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'title',
		'image',
		'description',
		'emails',
		'menu',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'title' => 'string',
		'image' => 'string',
		'description' => 'string',
		'emails' => 'string',
		'menu' => 'string',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'title' => '',
		'image' => 'file|image',
		'menu' => 'file|mimes:pdf',
	];

	public function setImageAttribute($file)
	{
		if (is_file($file)) {
			$file = \Storage::put('public/restauratns', $file, 'public');
		}
		$this->m_image = $file;
		$this->attributes['image'] = $file;
	}

	public function getImageAttribute($file)
	{
		$url = $file;
		if (\Storage::exists($this->m_image)) {
			$url = url(\Storage::url($this->m_image));
		} else if (\Storage::exists($this->attributes['image'])) {
			$url = url(\Storage::url($this->attributes['image']));
		}
		return $url;
	}

	public function setMenuAttribute($file)
	{
		if (is_file($file)) {
			$file = \Storage::put('public/restaurants/menus', $file, 'public');
		}
		$this->m_menu = $file;
		$this->attributes['menu'] = $file;
	}

	public function getMenuAttribute($file)
	{
		$url = $file;
		if (\Storage::exists($this->m_menu)) {
			$url = url(\Storage::url($this->m_menu));
		}
		else if (\Storage::exists($file)) {
			$url = url(\Storage::url($file));
		}
		return $url;
	}

	public function setTitleAttribute($data) {
		$this->attributes['title'] = json_encode($data);
	}

	public function getTitleAttribute($data) {
		$title = json_decode($data);
		if(!$title) {
			$title = (object) [
				'en' => $data,
				'ar' => $data
			];
		}
		return $title;
	}

	public function setDescriptionAttribute($data) {
		$this->m_description = $data;
		$this->attributes['description'] = $data;
	}

	public function getDescriptionAttribute($data) {
		return empty($this->m_description)? $this->attributes['description'] : $this->m_description;
	}

	public function gettitle()
	{
	foreach($this->title as $app) {
	
		$en=$app;
		return $en;
	}
	}

}
