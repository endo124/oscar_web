<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Promotion
 * @package App\Models
 * @version March 27, 2018, 5:31 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection categoryPromotion
 * @property string title
 * @property string type
 * @property string description
 * @property string image
 * @property string url
 * @property string price
 * @property string sale
 * @property dateTime start_date
 * @property dateTime end_date
 */
class Promotion extends Model
{
	use SoftDeletes;
	use \igaster\TranslateEloquent\TranslationTrait;

	// use \igaster\TranslateEloquent\TranslationTrait {
	// 	boot as bootTranslations;
	// }

	// protected static function boot() {
	// 	parent::boot();
	// 	self::bootTranslations();
	// }

	protected static $translatable = [
		'm_title',
		'm_description',
		'm_image',
		'm_url'
	];

	public $table = 'promotions';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'title',
		'type',
		'description',
		'image',
		'url',
		'price',
		'sale',
		'start_date',
		'end_date',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'title' => 'string',
		'type' => 'string',
		'description' => 'string',
		'image' => 'string',
		'url' => 'string',
		'price' => 'string',
		'sale' => 'string',
		'start_date' => 'datetime',
		'end_date' => 'datetime',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'title' => 'required',
		'type' => 'required',
		'image' => 'file|image',
	];

	public function setImageAttribute($file) {
		if (is_file($file)) {
			$file = \Storage::put('public/promotions', $file, 'public');
		}
		$this->m_image = $file;
		$this->attributes['image'] = $file;
	}

	public function getImageAttribute($file) {
		$url = $this->m_image;
		if (\Storage::exists($this->m_image)) {
			$url = url(\Storage::url($this->m_image));
		} else if (\Storage::exists($this->attributes['image'])) {
			$url = url(\Storage::url($this->attributes['image']));
		}
		return $url;
	}

	public function setTitleAttribute($data) {
		$this->m_title = $data;
		$this->attributes['title'] = $data;
	}

	public function getTitleAttribute($data) {
		return empty($this->m_title)? $this->attributes['title'] : $this->m_title;
	}
	
	public function setDescriptionAttribute($data) {
		$this->m_description = $data;
		$this->attributes['description'] = $data;
	}

	public function getDescriptionAttribute($data) {
		return empty($this->m_description)? $this->attributes['description'] : $this->m_description;
	}
	
	public function setUrlAttribute($data) {
		$this->m_url = $data;
		$this->attributes['url'] = $data;
	}

	public function getUrlAttribute($data) {
		return empty($this->m_url)? $this->attributes['url'] : $this->m_url;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 **/
	public function categories()
	{
		return $this->belongsToMany(\App\Models\Category::class, 'category_promotion', 'promotion_id', 'category_id');
	}
}
