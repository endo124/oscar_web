<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PageContent
 * @package App\Models
 * @version March 6, 2018, 10:12 am UTC
 *
 * @property \App\Models\Page page
 * @property integer order
 * @property string type
 * @property string value
 * @property integer page_id
 */
class PageContent extends Model
{
	use SoftDeletes;
	use \igaster\TranslateEloquent\TranslationTrait;

	// use \igaster\TranslateEloquent\TranslationTrait {
	// 	boot as bootTranslations;
	// }

	// protected static function boot() {
	// 	parent::boot();
	// 	self::bootTranslations();
	// }
	
	protected static $translatable = ['m_value'];
	
	public $table = 'page_contents';

	protected $appends = ['value_decoded'];
	protected $dates = ['deleted_at'];

	public $fillable = [
		'order',
		'type',
		'value',
		'page_id',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'order' => 'integer',
		'type' => 'string',
		'value' => 'string',
		'page_id' => 'integer',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'order' => 'integer',
		'type' => 'required',
		'value' => 'required',
	];

	public function getValueAttribute() {
		return empty($this->m_value)? $this->attributes['value'] : $this->m_value;
	}

	public function setValueAttribute($value) {
		$this->m_value = $value;
		$this->attributes['value'] = $value;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function page()
	{
		return $this->belongsTo(\App\Models\Page::class, 'page_id', 'id');
	}

	public function getValueDecodedAttribute()
	{
		$value = empty($this->m_value)? $this->attributes['value'] : $this->m_value;
		$data = json_decode($value);
		return is_null($data) ? $value : $data;
	}
}
