<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Models\Permission as Model;

/**
 * Class Permission
 * @package App\Models
 * @version March 4, 2018, 2:16 pm UTC
 *
 * @property \App\Models\ModelHasPermission modelHasPermission
 * @property \Illuminate\Database\Eloquent\Collection roleHasPermissions
 * @property string name
 * @property string guard_name
 */
class Permission extends Model
{
  use SoftDeletes;

  public $table = 'permissions';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  protected $dates = ['deleted_at'];

  public $fillable = [
    'name',
    'guard_name',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'name' => 'string',
    'guard_name' => 'string',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];

}
