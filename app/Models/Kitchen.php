<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Kitchen
 * @package App\Models
 * @version September 25, 2019, 12:55 pm UTC
 *
 * @property string title
 * @property string slug
 * @property string contect
 * @property longtext better_featured_image
 */
class Kitchen extends Model
{
	use SoftDeletes;

	public $table = 'kitchens';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'title',
		'slug',
		'content',
		'better_featured_image'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'title' => 'string',
		'slug' => 'string',
		'content' => 'string',
		'better_featured_image' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'title' => 'required',
		'slug' => 'required',

	];

	public function setBetterFeaturedImageAttribute($file)
	{
		if ($file) {
			if (is_file($file)) {
				$file = \Storage::put('public/kitchen', $file, 'public');
			}
			$jsonstring = array();
			$jsonstring['source_url'] = url(\Storage::url($file));
			$this->attributes['better_featured_image'] = json_encode($jsonstring);
		}
	}

	public function getBetterFeaturedImageAttribute($json)
	{
		if ($json) {
			$url = json_decode($json);
			if (!\Request::isJson()) {
				$img = $url->source_url;
				return $img;
			}
			return $url;
		}
	}

	public function setTitleAttribute($title)
	{
		$this->attributes['title'] = json_encode($title);
	}

	public function getTitleAttribute($data)
	{
		$title = json_decode($data);
		if (!$title) {
			$title = (object) [
				'rendered' => (object) [
					'en' => $data,
					'ar' => $data
				]
			];
		} else if (!is_object($title->rendered)) {
			$title->rendered = (object) [
				'en' => $title->rendered,
				'ar' => $title->rendered,
			];
		}
		return $title;
	}

	public function setContentAttribute($content)
	{
		$this->attributes['content'] = json_encode($content);
	}

	public function getContentAttribute($content)
	{
		$content = json_decode($content);
		if (!$content) {
			$content = (object) [
				'rendered' => (object) [
					'en' => $content,
					'ar' => $content
				]
			];
		} else if (!is_object($content->rendered)) {
			$content->rendered = (object) [
				'en' => $content->rendered,
				'ar' => $content->rendered,
			];
		}
		return $content;
	}

	public function toArray()
	{
		$array = parent::toArray();
		$data = [
			"date" => $this->attributes['created_at'],
			"date_gmt" => $this->attributes['created_at'],
			"guid" => [],
			"modified" => $this->attributes['updated_at'],
			"modified_gmt" => $this->attributes['updated_at'],
			"status" => "publish",
			"type" => "post",
			"link" => "",

			"excerpt" => [
				"protected" => false
			],
			"author" => 1,
			"featured_media" => 8693,
			"comment_status" => "open",
			"ping_status" => "open",
			"sticky" => false,
			"template" => "",
			"format" => "standard",
			"meta" => [],
			"categories" => [],
			"tags" => [],
			"_links" => [],
		];
		$array = array_merge($data, $array);
		$array["title"] = json_decode($this->attributes['title']);
		$array["content"] = json_decode($this->attributes['content']);
		$array["better_featured_image"] = json_decode($this->attributes['better_featured_image']);
		return $array;
	}


	public function gettitle()
	{
	foreach($this->title as $app) {
		$en=$app->en;
		return $en;
	}
	}
}
