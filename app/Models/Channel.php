<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version March 5, 2018, 12:02 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection categoryPageItem
 * @property string title
 * @property string image
 */
class Channel extends Model
{



	

	public $table = 'channel';

    public $fillable = [
		'client_id',
        'channel_name',
        'customer_agent_id',
        'status'
		
	];

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'client_id');
    }



	


}
