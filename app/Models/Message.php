<?php

namespace App\Models;

use Eloquent as Model;


class Message extends Model
{
    //

    protected $fillable = ['message','channel_id'];

     protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }
}
