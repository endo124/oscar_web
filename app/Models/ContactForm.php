<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ContactForm
 * @package App\Models
 * @version April 2, 2018, 6:30 am UTC
 *
 * @property string type
 * @property string values
 * @property string language
 */
class ContactForm extends Model
{
    use SoftDeletes;

    public $table = 'contact_forms';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'type',
        'values',
        'language'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'string',
        'values' => 'string',
        'language' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type' => 'required',
        'values' => 'required',
        'language' => 'required'
    ];

    
}
