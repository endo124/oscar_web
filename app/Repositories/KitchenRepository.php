<?php

namespace App\Repositories;

use App\Models\Kitchen;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class KitchenRepository
 * @package App\Repositories
 * @version September 25, 2019, 12:55 pm UTC
 *
 * @method Kitchen findWithoutFail($id, $columns = ['*'])
 * @method Kitchen find($id, $columns = ['*'])
 * @method Kitchen first($columns = ['*'])
*/
class KitchenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'slug',
        'better_featured_image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Kitchen::class;
    }
}
