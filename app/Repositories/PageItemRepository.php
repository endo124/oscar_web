<?php

namespace App\Repositories;

use App\Models\PageItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PageItemRepository
 * @package App\Repositories
 * @version March 5, 2018, 12:02 pm UTC
 *
 * @method PageItem findWithoutFail($id, $columns = ['*'])
 * @method PageItem find($id, $columns = ['*'])
 * @method PageItem first($columns = ['*'])
*/
class PageItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PageItem::class;
    }
}
