<?php

namespace App\Repositories;

use App\Models\PageContent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PageContentRepository
 * @package App\Repositories
 * @version March 6, 2018, 10:12 am UTC
 *
 * @method PageContent findWithoutFail($id, $columns = ['*'])
 * @method PageContent find($id, $columns = ['*'])
 * @method PageContent first($columns = ['*'])
*/
class PageContentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PageContent::class;
    }
}
