<?php

namespace App\Repositories;

use App\Models\OnBoarding;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OnBoardingRepository
 * @package App\Repositories
 * @version March 28, 2018, 11:04 am UTC
 *
 * @method OnBoarding findWithoutFail($id, $columns = ['*'])
 * @method OnBoarding find($id, $columns = ['*'])
 * @method OnBoarding first($columns = ['*'])
*/
class OnBoardingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'question',
        'answers'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OnBoarding::class;
    }
}
