<?php

namespace App\Repositories;

use App\Models\ContactForm;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactFormRepository
 * @package App\Repositories
 * @version April 2, 2018, 6:30 am UTC
 *
 * @method ContactForm findWithoutFail($id, $columns = ['*'])
 * @method ContactForm find($id, $columns = ['*'])
 * @method ContactForm first($columns = ['*'])
*/
class ContactFormRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'values',
        'language'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactForm::class;
    }
}
