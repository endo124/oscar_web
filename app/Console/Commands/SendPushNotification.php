<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Notification;

class SendPushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
     
          
        $notifications = Notification::whereNotNull('published_at')->get();
        foreach ( $notifications as  $notification) {
           if(date("Y-m-d H:i", strtotime($notification->published_at)) == date("Y-m-d H:i"))
           {
            
            $devices=\DB::table('devices')->where('os',$notification->os)->where('language',$notification->language)->pluck('device_token');
	
		
            if($devices->ToArray() !=[])
            {
    
              
                foreach($devices->ToArray() as $device)	
                {
                    
    
    #prep the bundle
    if(!is_null($notification->image))
    {
        $msg = array
        (
        "body" => $notification->title,
        "title" => $notification->message,
        'sound' => 'notify.mp3',
        'image'=>$notification->image
        
        );
    }
    else
    {
        $msg = array
        (
        "body" => $notification->title,
        "title" => $notification->message,
        'sound' => 'notify.mp3',
        
        );
    }
    
    $fields = array
    (
    'to' => $device,
    'notification' => $msg,
    'priority' => 'high',
    );
    $headers = array
    (
    'Authorization: key=' . 'AAAAfNRNLzw:APA91bEBA9-Wbm89gVPYRsZ7T9g-mNpM9ctT2TUDyZrgRcsaL99V4KuehTtOKLimInp1_G7uvJKOWQjjqsZF5N-fMhRI-r-oHN1nTMgTZA0v6qnVnV1RDoVbfKOym7cCJQFL-BHoCEj8',
    'Content-Type: application/json'
    );
    #Send Reponse To FireBase Server    
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
    $cur_message = json_decode($result);
           }
        }
     
        $this->info('Notification sent to All Users');

    }
}
}
}
